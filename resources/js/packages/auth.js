export default function (Vue) {
    Vue.auth = {
        setToken(token, expires_in){

            window.localStorage.setItem('token', token)
            window.localStorage.setItem('expires_in', expires_in)

            
        },
        getToken(){
            let token = window.localStorage.getItem('token')
            let expires_in = window.localStorage.getItem('expires_in')

            if(!token || !expires_in)  return null

            let current_tim = Date.now().valueOf() / 1000;
            if(current_tim > parseInt(expires_in)){
                this.destroyToken()
                return  null
            }
            else {
                return token
            }
        },
        destroyToken(){
            window.localStorage.removeItem('user');
            window.localStorage.removeItem('jwt');
            window.localStorage.removeItem('acl');
            window.localStorage.removeItem('token');
            window.localStorage.removeItem('expires_in');
        },
        isLoggedin(){

            if(this.getToken()) return true;
            else return false
        }
    }
    Object.defineProperties(Vue.prototype,{
        $auth:{
            get:() => {
                return Vue.auth;
            }
        }
    })
}