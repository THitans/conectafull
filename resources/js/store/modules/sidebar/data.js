// Sidebar Routers
export const menus = {
    'message.general': [
        {
            action: 'zmdi-view-dashboard',
            title: 'message.deskfull.title',
            active: true,
            items: [
                // {title: 'Adicionar Conta', path: '/deskfull-conta', items: null},
                {title: 'message.reportsADS', path: '/deskfull-reports-clients',shield:'reports.overview'},
                {title: 'message.accounts', path: '/deskfull-accounts', shield: 'deskfullUserTeam.listUserTeam'},
                {title: 'message.manageCustomers', path: '/manage-customers', shield: 'accounts.manage'},
                {title: 'message.teams', path: '/team', shield: 'deskfullUserTeam.listUserTeam'},
                {title: 'message.deskfullClients', path: '/deskfull-client', shield: 'deskfullclients.list'},
            ],

        },
        {
            action: 'zmdi-globe',
            title: 'message.config',
            active: false,
            items: [
                {title: 'message.users', path: '/users/list', shield: 'user.index'},
                {title: 'message.acl.roles', path: '/acl/roles/list', shield: 'role.index'},
                {title: 'message.acl.permissions', path: '/acl/permissions/list', is: 'Desenvolvedor'},
            ]
        }
    ]
}
