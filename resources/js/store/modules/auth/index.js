/**
 * Auth Module
 */
import Vue from 'vue'
import Nprogress from 'nprogress';
import router from '../../../router';

const state = {
  user: localStorage.getItem('user'),
  jwt: localStorage.getItem('jwt'),
  acl: localStorage.getItem('acl'),
  urlImageProfile: localStorage.getItem('urlImageProfile'),
};

// getters
const getters = {
  getUrlImageProfile: state => {
    if (state.urlImageProfile == 'null' || state.urlImageProfile == null) {
      return '/static/people-cover/face.png'
    }

    return state.urlImageProfile
  },
};

// actions
const actions = {
  signinUser(context, payload) {
    const {user} = payload;
    // context.commit('loginUser');
    axios.post('/api/login', {email: user.email, password: user.password})
      .then(response => {
        context.commit('loginUserSuccess', response.data.data);
      })
      .catch(error => {
        context.commit('loginUserFailure', error);
      });
  },

  logoutUser(context) {
    Nprogress.start();

    axios.post('/api/logout', {})
      .then(user => {
        Nprogress.done();
        context.commit('logoutUser');
      })
      .catch(error => {
        Nprogress.done();
        context.commit('loginUserFailure', error);
      });

  },

  setUrlProfileImage(context, payload) {
    const urlImage = payload.urlImage;
    context.commit('setUrlProfileImage', urlImage);
  }
};

// mutations
const mutations = {
  setUrlProfileImage(state, data) {
    const urlImage = '/storage/' + data
    state.urlImageProfile = urlImage
  },

  loginUser(state) {
    Nprogress.start();
  },

  loginUserSuccess(state, data) {
    state.user = data.user;
    state.jwt = data.jwt;
    state.acl = data.acl;
    state.urlImageProfile = data.user.imageUrl;

    localStorage.setItem('user', JSON.stringify(data.user));
    localStorage.setItem('jwt', JSON.stringify(data.jwt));
    localStorage.setItem('acl', JSON.stringify(data.acl));
    localStorage.setItem('urlImageProfile', data.user.imageUrl);

    window.axios = require('axios');

    window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

    let token = document.head.querySelector('meta[name="csrf-token"]');

    if (token) {
      window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
      window.axios.defaults.headers.common['Authorization'] = 'Bearer ' +  data.jwt.access_token;
    } else {
      console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
    }

    router.push("/");

    Vue.notify({
      group: 'loggedIn',
      type: 'success',
      text: 'Acesso Autorizado!'
    });
  },

  loginUserFailure(state, error) {
    Nprogress.done();
    Vue.notify({
      group: 'loggedIn',
      type: 'error',
      text: error.response.data.message
    });
  },

  logoutUser(state) {
    state.user = null
    state.jwt = null
    state.acl = null
    state.urlImageProfile = null
    
    localStorage.removeItem('user');
    localStorage.removeItem('jwt');
    localStorage.removeItem('acl');
    localStorage.removeItem('urlImageProfile');
    router.push("/session/login");
  },

  signUpUserFailure(state, error) {
    Nprogress.done();
    Vue.notify({
      group: 'loggedIn',
      type: 'error',
      text: error.message
    });
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
