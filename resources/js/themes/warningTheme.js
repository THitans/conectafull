export default {
   primary: '#605f62',
   secondary: '#424242',
   accent: '#82B1FF',
   error: '#FF3739',
   info: '#00D0BD',
   success: '#f36a32',
   warning: '#6182f3'
}