import Full from 'Container/Full'

/**
 * Components Connecta
 */
const UserProfile = () => import('Views/users/UserProfile');
const UsersCreate = () => import('Views/users/UsersCreate');
const UsersEdit = () => import('Views/users/UsersEdit');
const UsersList = () => import('Views/users/UsersList');
const UsersPassword = () => import('Views/users/UsersPassword');
const RolesList = () => import('Views/acl/RolesList');
const RolesCreate = () => import('Views/acl/RolesCreate');
const RolesEdit = () => import('Views/acl/RolesEdit');
const PermissionsList = () => import('Views/acl/PermissionsList');
const PermissionsCreate = () => import('Views/acl/PermissionsCreate');

const TeamCreate = () => import('Views/team/TeamCreate');
const TeamAddmember = () => import('Views/team/TeamMemberCreate');
const ClienteDeskFull = () => import('Views/deskfull/client/DeskFullClient');
const ClienteDeskFullAdd = () => import('Views/deskfull/client/DeskFullClientAdd');

const ReportsClients = () => import('Views/deskfull/ReportsClients');
const ReportsClientsDate = () => import('Views/deskfull/ReportsClientsDate');
const Reports = () => import('Views/deskfull/Reports');
const Accounts = () => import('Views/deskfull/Accounts');
const ManageCustomers = () => import('Views/deskfull/ManageCustomers');

export default {
  path: '/',
  component: Full,
  redirect: '/default/deskfull-reports-clients',
  children: [
    {
      path: '/default/team',
      component: TeamCreate,
      meta: {
        requiresAuth: true,
        title: 'message.deskfull.team.titleTeam',
        breadcrumb: 'Dashboard / Minha Equipe',
        shield: 'deskfullUserTeam.listUserTeam'
      }
    },
    {
      path: '/default/team-add-user',
      component: TeamAddmember,
      meta: {
        requiresAuth: true,
        title: 'message.deskfull.team.add',
        breadcrumb: 'Dashboard / Adicionar Membro a Equipe',
        shield: 'deskfullUserTeam.store'
      }
    },
    {
      path: '/default/deskfull-client',
      component: ClienteDeskFull,
      meta: {
        requiresAuth: true,
        title: 'message.team.titleDeskFullClient',
        breadcrumb: 'Dashboard / Cadastrar de Cliente',
        shield: 'deskfullclients.list'
      }
    },
    {
      path: '/default/deskfull-client-add/:clientId?',
      component: ClienteDeskFullAdd,
      meta: {
        requiresAuth: true,
        title: 'message.titleDeskFullClient',
        breadcrumb: 'Dashboard / Adicionar Cliente',
        shield: 'deskfullclients.store'
      }
    },
    {
      component: UserProfile,
      path: '/default/users/user-profile',
      meta: {
        requiresAuth: true,
        title: 'message.userProfile',
        breadcrumb: 'Users / User Profile'
      }
    },
    {
      path: '/default/users/new',
      component: UsersCreate,
      meta: {
        requiresAuth: true,
        title: 'message.usersNewTitle',
        breadcrumb: 'Usuários / Novo',
        shield: 'user.create'
      }
    },
    {
      path: '/default/users/edit/:userId',
      component: UsersEdit,
      meta: {
        requiresAuth: true,
        title: 'message.usersEditTitle',
        breadcrumb: 'Usuários / Editar',
        shield: 'user.update'
      }
    },
    {
      path: '/default/users/list',
      component: UsersList,
      meta: {
        requiresAuth: true,
        title: 'message.usersList',
        breadcrumb: 'Usuários / Lista',
        shield: 'user.index'
      }
    },
    {
      path: '/default/users/password',
      component: UsersPassword,
      meta: {
        requiresAuth: true,
        title: 'message.usersPasswordTitle',
        breadcrumb: 'Usuários / Alterar Senha'
      }
    },
    {
      path: '/default/acl/roles/list',
      component: RolesList,
      meta: {
        requiresAuth: true,
        title: 'message.acl.rolesListTitle',
        breadcrumb: 'Perfil / Lista',
        shield: 'role.index'
      }
    },
    {
      path: '/default/acl/roles/new',
      component: RolesCreate,
      meta: {
        requiresAuth: true,
        title: 'message.acl.rolesNewTitle',
        breadcrumb: 'Perfil / Novo',
        shield: 'role.create'
      }
    },
    {
      path: '/default/acl/roles/edit/:roleId',
      component: RolesEdit,
      meta: {
        requiresAuth: true,
        title: 'message.acl.rolesEditTitle',
        breadcrumb: 'Perfil / Editar',
        shield: 'role.edit'
      }
    },
    {
      path: '/default/acl/permissions/list',
      component: PermissionsList,
      meta: {
        requiresAuth: true,
        title: 'message.acl.permissionsListTitle',
        breadcrumb: 'Permissão / Lista',
        is: 'Desenvolvedor'
      }
    },
    {
      path: '/default/acl/permissions/new',
      component: PermissionsCreate,
      meta: {
        requiresAuth: true,
        title: 'message.acl.permissionsNewTitle',
        breadcrumb: 'Permissão / Nova',
        is: 'Desenvolvedor'
      }
    },
    {
      path: '/default/deskfull-accounts',
      component: Accounts,
      meta: {
        requiresAuth: true,
        title: 'message.accounts',
        breadcrumb: 'Cadastro de Conta',
        type: 'cliente',
        shield: 'reports.overview'
      }
    },
    {
      path: '/default/manage-customers',
      component: ManageCustomers,
      meta: {
        requiresAuth: true,
        title: 'message.manageCustomers',
        breadcrumb: 'Gerenciamento de contas',
        type: 'cliente',
        shield: 'accounts.manage'
      }
    },
    {
      requiresAuth: true,
      path: '/default/deskfull-reports',
      component: Reports,
      type: 'cliente',
      shield: 'reports.overview'
    },
    {
      requiresAuth: true,
      path: '/default/deskfull-reports-clients',
      component: ReportsClients,
      type: 'cliente',
      shield: 'reports.overview'
    },
    {
      requiresAuth: true,
      path: '/default/deskfull-reports-clients-date',
      component: ReportsClientsDate,
      type: 'cliente'
    }
  ]
}
