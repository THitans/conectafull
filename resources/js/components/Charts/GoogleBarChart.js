
import { Bar } from 'vue-chartjs'

export default {
    extends: Bar,
    props: {
        chartdata: {
            type: Array | Object,
            default: null
        },
        options: null,
        labelFormat: '',
    },
    data: () => ({


    }),

    mounted () {
        let labelFormat = this.labelFormat || '';
        if(this.options == null) {
            this.options = {
                responsive: true,
                maintainAspectRatio: false
            };
        }
        this.options.scales = {
            yAxes: [{
                ticks: {
                    callback: function(value, index, values) {
                        return `${labelFormat} ${value}`;
                    }
                }
            }]
        };
        this.renderChart(this.chartdata, this.options)
    }
}
