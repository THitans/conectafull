import router from './router'

import Auth from './packages/auth'
import Nprogress from "nprogress";


window.Vue = require('vue');

window._ = require('lodash');
window.Popper = require('popper.js').default;

window.Vue.use(Auth)
/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');
} catch (e) {
}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}


window.axios.interceptors.response.use((resp) => {
    return resp
}, (err) => {

    if (err.response.status === 401) {

        Vue.auth.destroyToken();
        router.push('/session/login')
    }
    return Promise.reject(err)

})

router.beforeEach((to, from, next) => {
    Nprogress.start()
    //Authorization
    if (localStorage.getItem('jwt') !== null) {

        var jwt = JSON.parse(localStorage.getItem('jwt'));
        window.axios.defaults.headers.common['Authorization'] = 'Bearer ' + jwt.access_token;
    }

    if (to.matched.some(record => record.meta.requiresAuth)) {

        if (localStorage.getItem('user') == null || localStorage.getItem('jwt') == null) {

            next({
                path: '/session/login',
            });
        } else {
            next()
        }

        axios.post('/api/me')
            .then(response => {
                //console.log(response)
            })
            .catch((error) => {
                //console.log(error.response)
                if (error.response.status === 401) {
                    Vue.auth.destroyToken();
                    //srouter.push('/session/login');
                }
            })

        if (to.matched.some(record => record.meta.visitors)) {

            if (Vue.auth.isLoggedin()) {
                next({
                    path: '/default/deskfull-reports-clients',
                });
            } else {
                next()
            }
        } else if (to.matched.some(record => record.meta.users)) {
            //to.matched.some(record => record.meta.users)) {
            if (!Vue.auth.isLoggedin()) {

                if (to.matched.some(record => record.meta.requiresAuth)) {

                    next({
                        path: '/session/login',
                    });
                }
            } else {
                next()
            }
        } else {
            next()
        }
    } else if (to.matched.some(record => record.meta.requiresVisitor)) {
        next()
    } else {
        if (localStorage.getItem('user') == null || localStorage.getItem('jwt') == null) {

            next({
                path: '/session/login',
            });
        } else {
            next()
        }
    }

    /*        router.push("/");
        } else {
            next()
        }*/


})
/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo'

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     encrypted: true
// });
