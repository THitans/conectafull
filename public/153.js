webpackJsonp([153],{

/***/ 2329:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(2335)
/* template */
var __vue_template__ = __webpack_require__(2336)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/views/acl/PermissionsList.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-9af45024", Component.options)
  } else {
    hotAPI.reload("data-v-9af45024", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 2335:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_Helpers_helpers__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_nprogress__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_nprogress___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_nprogress__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      loader: true,
      headers: [{
        text: "#",
        sortable: false,
        value: "#"
      }, {
        text: "Permissões",
        sortable: false,
        value: "readable_name"
      }],
      permissionsList: [],
      connectUsersList: null,
      links: [{
        id: 1,
        title: 'Nova Permission',
        icon: 'ti-lock mr-3 primary--text',
        path: '/users/new'
      }, {
        id: 3,
        title: 'Listar Permissões',
        icon: 'ti-list mr-3 success--text',
        path: '/users/list'
      }]
    };
  },
  mounted: function mounted() {
    this.getPermissions();
  },

  methods: {
    getPermissions: function getPermissions() {
      var _this = this;

      __WEBPACK_IMPORTED_MODULE_1_nprogress___default.a.start();
      axios.get("/acl/permission/index").then(function (response) {
        __WEBPACK_IMPORTED_MODULE_1_nprogress___default.a.done();
        _this.loader = false;
        _this.permissionsList = response.data.data;
      }).catch(function (error) {
        __WEBPACK_IMPORTED_MODULE_1_nprogress___default.a.done();
        _this.loader = false;

        _this.$store.dispatch("verifyUserAuth", error.response);
        Vue.notify({
          group: 'loggedIn',
          type: 'error',
          text: 'Erro ao Listar!'
        });
        console.log(error);
      });
    },
    getMenuLink: function getMenuLink(path) {
      return '/' + Object(__WEBPACK_IMPORTED_MODULE_0_Helpers_helpers__["b" /* getCurrentAppLayout */])(this.$router) + path;
    }
  }
});

/***/ }),

/***/ 2336:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("page-title-bar"),
      _vm._v(" "),
      _c("app-section-loader", { attrs: { status: _vm.loader } }),
      _vm._v(" "),
      _c(
        "v-container",
        { attrs: { fluid: "", "grid-list-xl": "" } },
        [
          _c(
            "v-layout",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    colClasses: "xs12",
                    heading: "",
                    customClasses: "mb-30"
                  }
                },
                [
                  _c(
                    "div",
                    { staticClass: "table-responsive" },
                    [
                      _c("app-section-loader", {
                        attrs: { status: _vm.loader }
                      }),
                      _vm._v(" "),
                      _c("v-data-table", {
                        attrs: {
                          headers: _vm.headers,
                          items: _vm.permissionsList,
                          "hide-actions": ""
                        },
                        scopedSlots: _vm._u([
                          {
                            key: "headers",
                            fn: function(props) {
                              return [
                                _c(
                                  "tr",
                                  _vm._l(props.headers, function(header) {
                                    return _c(
                                      "th",
                                      {
                                        key: header.text,
                                        staticClass: "text-xs-left fw-bold"
                                      },
                                      [
                                        _vm._v(
                                          "\n                                    " +
                                            _vm._s(header.text) +
                                            "\n                                "
                                        )
                                      ]
                                    )
                                  }),
                                  0
                                )
                              ]
                            }
                          },
                          {
                            key: "items",
                            fn: function(props) {
                              return [
                                _c("td", [_vm._v(_vm._s(props.item.id))]),
                                _vm._v(" "),
                                _c(
                                  "td",
                                  [
                                    _vm._v(
                                      "\n                                " +
                                        _vm._s(
                                          _vm.$tAcl(
                                            "readable_name",
                                            props.item.name,
                                            true
                                          )
                                        ) +
                                        "\n                                "
                                    ),
                                    _c(
                                      "v-badge",
                                      { staticClass: "primary text-lowercase" },
                                      [
                                        _vm._v(
                                          "shield: " + _vm._s(props.item.name)
                                        )
                                      ]
                                    )
                                  ],
                                  1
                                )
                              ]
                            }
                          }
                        ])
                      })
                    ],
                    1
                  )
                ]
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-9af45024", module.exports)
  }
}

/***/ })

});