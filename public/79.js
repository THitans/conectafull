webpackJsonp([79],{

/***/ 1510:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(2270)
}
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(2272)
/* template */
var __vue_template__ = __webpack_require__(2273)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-f53ac9d2"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/views/deskfull/VincularConta.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-f53ac9d2", Component.options)
  } else {
    hotAPI.reload("data-v-f53ac9d2", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 2270:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(2271);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("b6005f8c", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-f53ac9d2\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./VincularConta.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-f53ac9d2\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./VincularConta.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 2271:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)(false);
// imports


// module
exports.push([module.i, "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", ""]);

// exports


/***/ }),

/***/ 2272:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vuex__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_Helpers_helpers__ = __webpack_require__(43);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            selectCountry: ["United Kingdom"],
            addNewCardDialog: false,
            headers: [{ text: "Product", sortable: false, align: "center" }, { text: "", sortable: false }, { text: "Quantity", sortable: false, align: "center" }, { text: "Total", sortable: false, align: "center" }]
        };
    },

    name: "VincularConta"
});

/***/ }),

/***/ 2273:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "cart-wrapper" },
    [
      _c("page-title-bar"),
      _vm._v(" "),
      _c(
        "v-container",
        { attrs: { fluid: "", "pt-0": "" } },
        [
          _c(
            "app-card",
            { attrs: { heading: _vm.Conta, customClasses: "mb-30" } },
            [
              _c(
                "v-form",
                [
                  _c(
                    "v-btn",
                    {
                      attrs: { color: "primary" },
                      on: {
                        click: function($event) {
                          _vm.addNewCardDialog = true
                        }
                      }
                    },
                    [
                      _vm._v(
                        "\n                    " +
                          _vm._s(_vm.$t("message.addNewConta")) +
                          "\n                "
                      )
                    ]
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "app-card",
            { attrs: { heading: _vm.Contas, customClasses: "mb-30" } },
            [
              _c(
                "v-layout",
                { attrs: { row: "", wrap: "" } },
                [
                  _c(
                    "v-layout",
                    { attrs: { row: "", wrap: "", "icon-box": "" } },
                    [
                      _c(
                        "v-flex",
                        { attrs: { xs12: "", sm6: "", md4: "", lg3: "" } },
                        [_c("span", [_c("i", { staticClass: "ti-link" })])]
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c("v-flex", { attrs: { xs6: "", sm4: "" } }, [
                    _c("span", { staticClass: "small pt-4 d-block" }, [
                      _vm._v("alexcomput@gmail.com")
                    ])
                  ]),
                  _vm._v(" "),
                  _c(
                    "v-layout",
                    { attrs: { row: "", wrap: "", "icon-box": "" } },
                    [
                      _c(
                        "v-flex",
                        { attrs: { xs12: "", sm3: "", md4: "", lg2: "" } },
                        [_c("span", [_c("i", { staticClass: "ti-reload" })])]
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-layout",
                    { attrs: { row: "", wrap: "", "icon-box": "" } },
                    [
                      _c(
                        "v-flex",
                        { attrs: { xs12: "", sm2: "", md4: "", lg2: "" } },
                        [_c("span", [_c("i", { staticClass: "ti-link" })])]
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-layout",
                    { attrs: { row: "", wrap: "", "icon-box": "" } },
                    [
                      _c(
                        "v-flex",
                        { attrs: { xs12: "", sm6: "", md4: "", lg2: "" } },
                        [_c("span", [_c("i", { staticClass: "ti-trash" })])]
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-dialog",
            {
              attrs: { "max-width": "500" },
              model: {
                value: _vm.addNewCardDialog,
                callback: function($$v) {
                  _vm.addNewCardDialog = $$v
                },
                expression: "addNewCardDialog"
              }
            },
            [
              _c(
                "v-card",
                [
                  _c("v-card-title", [
                    _c("span", { staticClass: "headline" }, [
                      _vm._v(_vm._s(_vm.$t("message.addNewConta")))
                    ])
                  ]),
                  _vm._v(" "),
                  _c("v-spacer"),
                  _vm._v(" "),
                  _c(
                    "v-card-media",
                    [
                      _c(
                        "v-layout",
                        { attrs: { row: "", wrap: "" } },
                        [
                          _c(
                            "v-layout",
                            { attrs: { row: "", wrap: "", "icon-box": "" } },
                            [
                              _c(
                                "v-flex",
                                {
                                  attrs: { xs12: "", sm6: "", md4: "", lg4: "" }
                                },
                                [
                                  _c("span", [
                                    _c("i", { staticClass: "ti-google" })
                                  ]),
                                  _vm._v(" "),
                                  _c("span", { staticClass: "icon-title" }, [
                                    _vm._v("Google Ads")
                                  ])
                                ]
                              )
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-layout",
                            { attrs: { row: "", wrap: "", "icon-box": "" } },
                            [
                              _c(
                                "v-flex",
                                {
                                  attrs: { xs12: "", sm6: "", md4: "", lg4: "" }
                                },
                                [
                                  _c("span", [
                                    _c("i", { staticClass: "ti-google" })
                                  ]),
                                  _vm._v(" "),
                                  _c("span", { staticClass: "icon-title" }, [
                                    _vm._v("Google Analytics")
                                  ])
                                ]
                              )
                            ],
                            1
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-layout",
                        { attrs: { row: "", wrap: "" } },
                        [
                          _c(
                            "v-layout",
                            { attrs: { row: "", wrap: "", "icon-box": "" } },
                            [
                              _c(
                                "v-flex",
                                {
                                  attrs: { xs12: "", sm6: "", md4: "", lg4: "" }
                                },
                                [
                                  _c("span", [
                                    _c("i", { staticClass: "ti-google" })
                                  ]),
                                  _vm._v(" "),
                                  _c("span", { staticClass: "icon-title" }, [
                                    _vm._v("Facebook ADS")
                                  ])
                                ]
                              )
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-layout",
                            { attrs: { row: "", wrap: "", "icon-box": "" } },
                            [
                              _c(
                                "v-flex",
                                {
                                  attrs: { xs12: "", sm6: "", md4: "", lg4: "" }
                                },
                                [
                                  _c("span", [
                                    _c("i", { staticClass: "ti-google" })
                                  ]),
                                  _vm._v(" "),
                                  _c("span", { staticClass: "icon-title" }, [
                                    _vm._v("Instagram")
                                  ])
                                ]
                              )
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c("v-card-actions")
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-f53ac9d2", module.exports)
  }
}

/***/ })

});