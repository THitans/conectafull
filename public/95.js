webpackJsonp([95],{

/***/ 1513:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(2287)
}
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(2291)
/* template */
var __vue_template__ = __webpack_require__(2293)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-4c50182f"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/views/deskfull/client/DeskFullClient.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-4c50182f", Component.options)
  } else {
    hotAPI.reload("data-v-4c50182f", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1823:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1824)
}
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(1826)
/* template */
var __vue_template__ = __webpack_require__(1827)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-75773638"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/components/AppCard/AccountCard.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-75773638", Component.options)
  } else {
    hotAPI.reload("data-v-75773638", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1824:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1825);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("567d49ea", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-75773638\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./AccountCard.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-75773638\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./AccountCard.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1825:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)(false);
// imports


// module
exports.push([module.i, "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", ""]);

// exports


/***/ }),

/***/ 1826:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__AppCardHeading__ = __webpack_require__(569);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__AppCardHeading___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__AppCardHeading__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
    name: "AccountCard",
    components: { AppCardHeading: __WEBPACK_IMPORTED_MODULE_0__AppCardHeading___default.a },
    props: ['account', 'id'],
    data: function data() {
        return {};
    }
});

/***/ }),

/***/ 1827:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "app-card",
    { attrs: { heading: this.account.name, customClasses: "mb-30" } },
    [
      _c(
        "v-chip",
        { attrs: { color: "error", "text-color": "white" } },
        [
          _c("v-avatar", [_c("v-icon", [_vm._v("ti-google")])], 1),
          _vm._v("\n        " + _vm._s(this.account.socialnetwork) + "\n    ")
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "v-layout",
        { attrs: { row: "", wrap: "" } },
        [
          _c("v-flex", { attrs: { xs6: "", sm4: "" } }, [
            _c("span", { staticClass: "small pt-4 d-block" }, [
              _vm._v(" " + _vm._s(this.account.email))
            ])
          ]),
          _vm._v(" "),
          _c("v-flex", { attrs: { xs6: "", sm4: "" } }, [
            _c("span", { staticClass: "small pt-4 d-block" }, [
              _vm._v(" " + _vm._s(this.account.refresh_token))
            ])
          ]),
          _vm._v(" "),
          _c(
            "v-layout",
            { attrs: { row: "", wrap: "", "icon-box": "" } },
            [
              _c(
                "v-flex",
                { attrs: { xs12: "", sm6: "", md4: "", lg2: "" } },
                [
                  _c(
                    "v-btn",
                    {
                      attrs: { color: "error", fab: "", small: "", dark: "" },
                      on: {
                        click: function($event) {
                          _vm.$emit("deleteAccount", _vm.id)
                        }
                      }
                    },
                    [_c("v-icon", [_vm._v("ti-trash")])],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-75773638", module.exports)
  }
}

/***/ }),

/***/ 2287:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(2288);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("2b99fc57", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-4c50182f\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./DeskFullClient.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-4c50182f\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./DeskFullClient.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 2288:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)(false);
// imports


// module
exports.push([module.i, "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", ""]);

// exports


/***/ }),

/***/ 2291:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_Helpers_helpers__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_Components_AppCard_AccountCard__ = __webpack_require__(1823);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_Components_AppCard_AccountCard___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_Components_AppCard_AccountCard__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_axios__ = __webpack_require__(568);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_axios___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_axios__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ __webpack_exports__["default"] = ({
  components: { AccountCard: __WEBPACK_IMPORTED_MODULE_1_Components_AppCard_AccountCard___default.a },
  data: function data() {
    return {
      form1: {},
      addNewAccountDialog: false,
      showAccounts: false,
      selectDeletedAccount: null,
      loader: true,
      headers: [{ text: "Nome", sortable: false }, { text: "E-mail", sortable: false }, { text: "Refresh Token", sortable: false }, { text: "Remover conta", sortable: false }],
      accounts: []
    };
  },

  methods: {
    mounted: function mounted() {
      this.getAccounts();
    },
    submit: function submit() {
      if (this.$refs.form.validate()) {
        console.log("form submit");
      }
    },
    googleOauth: function googleOauth() {
      window.location = "/oauth2";
    },
    getAccounts: function getAccounts() {
      var _this = this;

      __WEBPACK_IMPORTED_MODULE_2_axios___default.a.get('/api/accounts').then(function (response) {
        _this.accounts = response.data;
        _this.loader = false;
      }).catch(function (error) {
        _this.loader = false;
        console.log(error);
      });
    },
    deleteAccount: function deleteAccount(account) {
      this.$refs.deleteConfirmationDialog.openDialog();
      this.selectDeletedAccount = account;
    },

    // delete card
    onDeleteAccount: function onDeleteAccount() {
      var _this2 = this;

      this.$refs.deleteConfirmationDialog.close();
      this.loader = true;
      __WEBPACK_IMPORTED_MODULE_2_axios___default.a.delete('/api/accounts/' + this.selectDeletedAccount.id).then(function (response) {
        if (response.data.result == true) {
          _this2.$store.dispatch("onDeleteAccount", _this2.selectDeletedAccount);
          _this2.getAccounts();
        } else {
          alert('Erro ao excluir conta');
        }
      }).catch(function (error) {
        console.log(error);
      });
    }
  },

  name: "VincularConta"
});

/***/ }),

/***/ 2293:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-container",
    { attrs: { fluid: "", "grid-list-xl": "", "py-0": "" } },
    [
      _c("page-title-bar"),
      _vm._v(" "),
      _c(
        "app-card",
        { attrs: { heading: "", customClasses: "mb-30" } },
        [
          _c(
            "v-form",
            [
              _c(
                "v-btn",
                { attrs: { to: "team-add-client", color: "primary" } },
                [
                  _vm._v(
                    "\n                " +
                      _vm._s(_vm.$t("message.addNewDeskFullClient")) +
                      "\n            "
                  )
                ]
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c("v-spacer"),
      _vm._v(" "),
      _c(
        "app-card",
        {
          attrs: {
            colClasses: "xs12 md12",
            heading: _vm.$t("message.addNewDeskFullClientPanel")
          }
        },
        [
          _c(
            "v-layout",
            { attrs: { row: "", wrap: "" } },
            [
              _c(
                "app-card",
                { attrs: { colClasses: "xs12 md6" } },
                [
                  _c("v-img", {
                    attrs: { src: "/static/img/blog-6.jpg", height: "240px" }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                { attrs: { colClasses: "xs12 md6" } },
                [
                  _c(
                    "v-form",
                    { ref: "form", attrs: { "lazy-validation": "" } },
                    [
                      _c("v-text-field", {
                        attrs: {
                          label: "Nome",
                          id: "name",
                          counter: 10,
                          required: ""
                        }
                      }),
                      _vm._v(" "),
                      _c("v-text-field", {
                        attrs: { label: "E-mail", id: "email", required: "" }
                      }),
                      _vm._v(" "),
                      _c("v-text-field", {
                        attrs: { label: "Website", id: "site", required: "" }
                      }),
                      _vm._v(" "),
                      _c("v-select", {
                        attrs: {
                          id: "conta",
                          label: "Contas Associada ao Cliente (Conexões)",
                          rules: [
                            function(v) {
                              return !!v || "Item is required"
                            }
                          ],
                          required: ""
                        }
                      }),
                      _vm._v(" "),
                      _c(
                        "v-btn",
                        {
                          attrs: { color: "success" },
                          on: { click: _vm.submit }
                        },
                        [
                          _vm._v(
                            "\n                        " +
                              _vm._s(_vm.$t("message.submit")) +
                              "\n                    "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c("v-btn", { attrs: { color: "primary" } }, [
                        _vm._v(_vm._s(_vm.$t("message.clear")))
                      ])
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c("app-section-loader", { attrs: { status: _vm.loader } })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-4c50182f", module.exports)
  }
}

/***/ })

});