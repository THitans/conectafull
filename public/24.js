webpackJsonp([24],{

/***/ 1307:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(1359)
/* template */
var __vue_template__ = __webpack_require__(1376)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/views/users/UserProfile.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-39b55748", Component.options)
  } else {
    hotAPI.reload("data-v-39b55748", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1359:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_Components_Widgets_UserDetail__ = __webpack_require__(1360);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_Components_Widgets_UserDetail___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_Components_Widgets_UserDetail__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_Components_Widgets_Skills__ = __webpack_require__(1368);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_Components_Widgets_Skills___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_Components_Widgets_Skills__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_Components_Widgets_Education__ = __webpack_require__(1370);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_Components_Widgets_Education___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_Components_Widgets_Education__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_Components_Widgets_ContactRequest__ = __webpack_require__(1372);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_Components_Widgets_ContactRequest___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_Components_Widgets_ContactRequest__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_Components_Widgets_UserActivity__ = __webpack_require__(1374);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_Components_Widgets_UserActivity___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_Components_Widgets_UserActivity__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//







/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    UserDetail: __WEBPACK_IMPORTED_MODULE_0_Components_Widgets_UserDetail___default.a,
    Skills: __WEBPACK_IMPORTED_MODULE_1_Components_Widgets_Skills___default.a,
    Education: __WEBPACK_IMPORTED_MODULE_2_Components_Widgets_Education___default.a,
    ContactRequest: __WEBPACK_IMPORTED_MODULE_3_Components_Widgets_ContactRequest___default.a,
    UserActivity: __WEBPACK_IMPORTED_MODULE_4_Components_Widgets_UserActivity___default.a
  },

  data: function data() {
    return {
      user: {
        name: "",
        email: "",
        cpf: "",
        selectType: null,
        selectRole: null,
        schema: '',
        types: ["Admin", "Cliente"],
        roles: [],
        checkbox: false
      },
      links: [{
        id: 1,
        title: 'message.usersNewTitle',
        icon: 'ti-user mr-3 primary--text',
        path: '/users/new'
      }, {
        id: 3,
        title: 'message.usersList',
        icon: 'ti-list mr-3 success--text',
        path: '/users/list'
      }]
    };
  },
  mounted: function mounted() {
    this.getUser();
  },

  methods: {
    getUser: function getUser() {
      var _this = this;

      axios.post("/api/me").then(function (response) {
        _this.loader = false;
        var user = response.data.data.user,
            roles = response.data.data.roles;

        _this.user.name = user.name;
        _this.user.email = user.email;
        _this.user.cpf = user.cpf;
        _this.user.selectType = user.type;
        _this.user.schema = user.schema;
        _this.user.roles = roles;
      }).catch(function (error) {
        _this.loader = false;
        Vue.notify({
          group: 'loggedIn',
          type: 'error',
          text: 'Erro ao Buscar Usuário!'
        });
        console.log(error);
      });
    }
  }
});

/***/ }),

/***/ 1360:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(1361)
/* template */
var __vue_template__ = __webpack_require__(1367)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/components/Widgets/UserDetail.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-1e988327", Component.options)
  } else {
    hotAPI.reload("data-v-1e988327", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1361:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ImageInput_ImageInput__ = __webpack_require__(1362);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ImageInput_ImageInput___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__ImageInput_ImageInput__);
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['name', 'email', 'roles'],
  data: function data() {
    return {
      avatar: null,
      saving: false,
      saved: false,
      imageProfile: '',
      hasImageProfile: false
    };
  },

  components: {
    ImageInput: __WEBPACK_IMPORTED_MODULE_0__ImageInput_ImageInput___default.a
  },
  watch: {
    avatar: {
      handler: function handler() {
        this.saved = false;
      },
      deep: true
    }
  },
  mounted: function mounted() {
    this.getImage();
  },

  methods: {
    uploadImage: function uploadImage() {
      var _this = this;

      this.saving = true;
      setTimeout(function () {
        return _this.savedAvatar();
      }, 1000);
    },
    savedAvatar: function savedAvatar() {
      var _headers,
          _this2 = this;

      var settings = {
        headers: (_headers = {
          'Content-Type': 'multipart/form-data'
        }, _defineProperty(_headers, 'Content-Type', 'application/x-www-form-urlencoded'), _defineProperty(_headers, 'Accept', 'text/plain'), _headers)
      };
      axios.post('/api/acl/user/upload', this.avatar.formData, settings).then(function (response) {
        // console.log(response)
      }).catch(function (error) {
        _this2.loader = false;
        Vue.notify({
          group: 'loggedIn',
          type: 'error',
          text: 'Erro ao Enviar Image! Tente mais tarde.'
        });
        console.log(error);
      });

      this.saving = false;
      this.saved = true;
    },
    getImage: function getImage() {
      var _this3 = this;

      var settings = {
        headers: {
          'Content-type': 'image/jpeg'
        }
      };
      axios.get('/api/acl/user/profileImageUrl', settings).then(function (response) {
        if (response.data.success == true) {
          _this3.hasImageProfile = true;

          _this3.imageProfile = response.data.profileImageUrl;
        }
      }).catch(function (error) {
        console.log(error);
      });
    }
  }
});

/***/ }),

/***/ 1362:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1363)
}
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(1365)
/* template */
var __vue_template__ = __webpack_require__(1366)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/components/ImageInput/ImageInput.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5dceb954", Component.options)
  } else {
    hotAPI.reload("data-v-5dceb954", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1363:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1364);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("69ad81b7", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-5dceb954\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!./imageInput.scss", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-5dceb954\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!./imageInput.scss");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1364:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)(false);
// imports


// module
exports.push([module.i, "\n.image-input .click-image {\n  cursor: pointer;\n}\n", ""]);

// exports


/***/ }),

/***/ 1365:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'image-input',
  data: function data() {
    return {
      errorDialog: null,
      errorText: '',
      uploadFieldName: 'file',
      maxSize: 2000
    };
  },
  props: {
    // Use "value" here to enable compatibility with v-model
    value: Object
  },
  methods: {
    launchFilePicker: function launchFilePicker() {
      this.$refs.file.click();
    },
    onFileChange: function onFileChange(fieldName, file) {
      console.log(file);
      var maxSize = this.maxSize;

      var imageFile = file[0];

      //check if user actually selected a file
      if (file.length > 0) {
        var size = imageFile.size / maxSize / maxSize;
        if (!imageFile.type.match('image.*')) {
          // check whether the upload is an image
          this.errorDialog = true;
          this.errorText = 'Please choose an image file';
        } else if (size > 1) {
          // check whether the size is greater than the size limit
          this.errorDialog = true;
          this.errorText = 'Your file is too big! Please select an image under 1MB';
        } else {
          // Append file into FormData & turn file into image URL
          var formData = new FormData();
          var imageURL = URL.createObjectURL(imageFile);
          formData.append(fieldName, imageFile);
          // Emit FormData & image URL to the parent component
          this.$emit('input', { formData: formData, imageURL: imageURL });
        }
      }
    }
  }
});

/***/ }),

/***/ 1366:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "image-input " },
    [
      _c(
        "div",
        {
          staticClass: "click-image",
          on: {
            click: function($event) {
              _vm.launchFilePicker()
            }
          }
        },
        [_vm._t("activator")],
        2
      ),
      _vm._v(" "),
      _c("input", {
        ref: "file",
        staticStyle: { display: "none" },
        attrs: { type: "file", name: _vm.uploadFieldName },
        on: {
          change: function($event) {
            _vm.onFileChange($event.target.name, $event.target.files)
          }
        }
      }),
      _vm._v(" "),
      _c(
        "v-dialog",
        {
          attrs: { "max-width": "300" },
          model: {
            value: _vm.errorDialog,
            callback: function($$v) {
              _vm.errorDialog = $$v
            },
            expression: "errorDialog"
          }
        },
        [
          _c(
            "v-card",
            [
              _c("v-card-text", { staticClass: "subheading" }, [
                _vm._v(_vm._s(_vm.errorText))
              ]),
              _vm._v(" "),
              _c(
                "v-card-actions",
                [
                  _c("v-spacer"),
                  _vm._v(" "),
                  _c(
                    "v-btn",
                    {
                      attrs: { flat: "" },
                      on: {
                        click: function($event) {
                          _vm.errorDialog = false
                        }
                      }
                    },
                    [_vm._v("Got it!")]
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-5dceb954", module.exports)
  }
}

/***/ }),

/***/ 1367:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "profile-head app-card mb-30" }, [
    _c("div", { staticClass: "profile-top" }, [
      _c("img", {
        attrs: {
          src: "/static/img/profile-banner.jpg",
          alt: _vm.name,
          width: "1920",
          height: "165"
        }
      })
    ]),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "profile-bottom border-bottom-light-1" },
      [
        _c(
          "v-layout",
          {
            attrs: {
              "align-center": "",
              "justify-center": "",
              row: "",
              "fill-height": ""
            }
          },
          [
            _c(
              "div",
              { staticClass: "user-image text-xs-center mb-3" },
              [
                _c(
                  "image-input",
                  {
                    model: {
                      value: _vm.avatar,
                      callback: function($$v) {
                        _vm.avatar = $$v
                      },
                      expression: "avatar"
                    }
                  },
                  [
                    _c(
                      "div",
                      { attrs: { slot: "activator" }, slot: "activator" },
                      [
                        !_vm.avatar
                          ? _c(
                              "v-avatar",
                              {
                                directives: [
                                  { name: "ripple", rawName: "v-ripple" }
                                ],
                                staticClass: "grey lighten-3 mb-3",
                                attrs: { size: "120px" }
                              },
                              [
                                _vm.hasImageProfile == false
                                  ? _c("span", [_vm._v("Clique para Adcionar")])
                                  : _vm._e(),
                                _vm._v(" "),
                                _vm.hasImageProfile == true
                                  ? _c("img", {
                                      attrs: {
                                        src: this.imageProfile,
                                        alt: "avatar"
                                      }
                                    })
                                  : _vm._e()
                              ]
                            )
                          : _c(
                              "v-avatar",
                              {
                                directives: [
                                  { name: "ripple", rawName: "v-ripple" }
                                ],
                                staticClass: "mb-3",
                                attrs: { size: "150px" }
                              },
                              [
                                _c("img", {
                                  attrs: {
                                    src: _vm.avatar.imageURL,
                                    alt: "avatar"
                                  }
                                })
                              ]
                            )
                      ],
                      1
                    )
                  ]
                ),
                _vm._v(" "),
                _c("v-slide-x-transition", [
                  _vm.avatar && _vm.saved == false
                    ? _c(
                        "div",
                        [
                          _c(
                            "v-btn",
                            {
                              staticClass: "primary",
                              attrs: { loading: _vm.saving },
                              on: { click: _vm.uploadImage }
                            },
                            [_vm._v("Clique para Salvar")]
                          )
                        ],
                        1
                      )
                    : _vm._e()
                ])
              ],
              1
            )
          ]
        ),
        _vm._v(" "),
        _c("div", { staticClass: "user-list-content" }, [
          _c("div", { staticClass: "text-xs-center" }, [
            _c("h3", { staticClass: "fw-bold" }, [_vm._v(_vm._s(_vm.name))]),
            _vm._v(" "),
            _c("p", [_vm._v(_vm._s(_vm.email))]),
            _vm._v(" "),
            _c("div", { staticClass: "social-list clearfix mb-4" }, [
              _c(
                "ul",
                { staticClass: "list-inline d-inline-block" },
                _vm._l(_vm.roles, function(role) {
                  return _c(
                    "li",
                    [
                      _c(
                        "v-chip",
                        { attrs: { color: "orange", "text-color": "white" } },
                        [
                          _c(
                            "v-avatar",
                            [_c("v-icon", [_vm._v("account_circle")])],
                            1
                          ),
                          _vm._v(
                            "\n                                " +
                              _vm._s(role) +
                              "\n                            "
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                }),
                0
              )
            ])
          ])
        ])
      ],
      1
    ),
    _vm._v(" "),
    _c("div", { staticClass: "user-activity text-xs-center" })
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-1e988327", module.exports)
  }
}

/***/ }),

/***/ 1368:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = null
/* template */
var __vue_template__ = __webpack_require__(1369)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/components/Widgets/Skills.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-9a91f426", Component.options)
  } else {
    hotAPI.reload("data-v-9a91f426", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1369:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("v-card-text", [
    _c("ul", { staticClass: "list-unstyled card-list" }, [
      _c(
        "li",
        [
          _c("p", { staticClass: "fw-bold" }, [_vm._v("HTML")]),
          _vm._v(" "),
          _c("v-progress-linear", {
            attrs: { value: "20", height: "5", color: "white" }
          })
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "li",
        [
          _c("p", { staticClass: "fw-bold" }, [_vm._v("Css3")]),
          _vm._v(" "),
          _c("v-progress-linear", {
            attrs: { value: "90", height: "5", color: "white" }
          })
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "li",
        [
          _c("p", { staticClass: "fw-bold" }, [_vm._v("Photoshop")]),
          _vm._v(" "),
          _c("v-progress-linear", {
            attrs: { value: "30", height: "5", color: "white" }
          })
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "li",
        [
          _c("p", { staticClass: "fw-bold" }, [_vm._v("Javascript")]),
          _vm._v(" "),
          _c("v-progress-linear", {
            attrs: { value: "75", height: "5", color: "white" }
          })
        ],
        1
      )
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-9a91f426", module.exports)
  }
}

/***/ }),

/***/ 1370:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = null
/* template */
var __vue_template__ = __webpack_require__(1371)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/components/Widgets/Education.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-dd673566", Component.options)
  } else {
    hotAPI.reload("data-v-dd673566", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1371:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-list",
    { attrs: { "two-line": "" } },
    [
      _c(
        "v-list-tile",
        [
          _c(
            "v-list-tile-content",
            [
              _c("v-list-tile-sub-title", [
                _c("h5", { staticClass: "fw-bold gray--text" }, [
                  _vm._v("Product Designer")
                ]),
                _vm._v(" "),
                _c("span", [_vm._v("AirHelper - London, United Kingdom")])
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c("v-list-tile-action", [
            _c("span", { staticClass: "small" }, [_vm._v("2016 - 2017")])
          ])
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "v-list-tile",
        [
          _c(
            "v-list-tile-content",
            [
              _c("v-list-tile-sub-title", [
                _c("h5", { staticClass: "fw-bold gray--text" }, [
                  _vm._v("App Designer")
                ]),
                _vm._v(" "),
                _c("span", [_vm._v("AirHelper - London, United Kingdom")])
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c("v-list-tile-action", [
            _c("span", { staticClass: "small" }, [_vm._v("2017 - 2018")])
          ])
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "v-list-tile",
        [
          _c(
            "v-list-tile-content",
            [
              _c("v-list-tile-sub-title", [
                _c("h5", { staticClass: "fw-bold gray--text" }, [
                  _vm._v("Service Designer")
                ]),
                _vm._v(" "),
                _c("span", [_vm._v("AirHelper - London, United Kingdom")])
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c("v-list-tile-action", [
            _c("span", { staticClass: "small" }, [_vm._v("2015 - 2016")])
          ])
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-dd673566", module.exports)
  }
}

/***/ }),

/***/ 1372:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = null
/* template */
var __vue_template__ = __webpack_require__(1373)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/components/Widgets/ContactRequest.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-3540767a", Component.options)
  } else {
    hotAPI.reload("data-v-3540767a", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1373:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "media pos-relative" },
    [
      _c("img", {
        staticClass: "img-responsive rounded-circle mr-4",
        attrs: {
          src: "/static/avatars/user-12.jpg",
          alt: "user profile",
          width: "95",
          height: "95"
        }
      }),
      _vm._v(" "),
      _c("div", { staticClass: "media-body" }, [
        _c("span", { staticClass: "pink--text" }, [_vm._v("Request")]),
        _vm._v(" "),
        _c("h2", [_vm._v("Andre Hicks")]),
        _vm._v(" "),
        _c("span", [_vm._v("Sr. Develoepr @Oracle")]),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "btn-wrapper" },
          [
            _c("v-btn", { attrs: { color: "success" } }, [_vm._v("Accept")]),
            _vm._v(" "),
            _c("v-btn", { attrs: { color: "error" } }, [_vm._v("Reject")])
          ],
          1
        )
      ]),
      _vm._v(" "),
      _c(
        "v-avatar",
        { staticClass: "overlap", attrs: { color: "warning" } },
        [_c("v-icon", { attrs: { dark: "" } }, [_vm._v("ti-id-badge")])],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-3540767a", module.exports)
  }
}

/***/ }),

/***/ 1374:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = null
/* template */
var __vue_template__ = __webpack_require__(1375)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/components/Widgets/UserActivity.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5b039765", Component.options)
  } else {
    hotAPI.reload("data-v-5b039765", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1375:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", [
      _c("div", { staticClass: "pa-3" }, [
        _c("p", { staticClass: "mb-0" }, [_vm._v("Today")])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "media-listing" }, [
        _c("ul", { staticClass: "list-unstyled" }, [
          _c("li", { staticClass: "media border-bottom-light-1" }, [
            _c("img", {
              staticClass: "rounded-circle mr-4 img-responsive",
              attrs: {
                src: "/static/avatars/user-7.jpg",
                alt: "user profile",
                width: "60",
                height: "60"
              }
            }),
            _vm._v(" "),
            _c("div", { staticClass: "media-body" }, [
              _c("span", { staticClass: "fw-bold" }, [
                _vm._v("Louise Kate "),
                _c("span", { staticClass: "small" }, [_vm._v("@louisekate")])
              ]),
              _vm._v(" "),
              _c("p", { staticClass: "mb-0" }, [
                _vm._v(
                  "The new common language will be more simple and regular than the existing European languages. It will be as simple as Occidental; in fact, it will be Occidental."
                )
              ])
            ])
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "media border-bottom-light-1" }, [
            _c("img", {
              staticClass: "rounded-circle mr-4 img-responsive",
              attrs: {
                src: "/static/avatars/user-8.jpg",
                alt: "user profile",
                width: "60",
                height: "60"
              }
            }),
            _vm._v(" "),
            _c("div", { staticClass: "media-body" }, [
              _c("span", { staticClass: "fw-bold" }, [
                _vm._v("Annie Lee "),
                _c("span", { staticClass: "small" }, [_vm._v("@Annielee")])
              ]),
              _vm._v(" "),
              _c("p", { staticClass: "mb-0" }, [_vm._v("Posted new photo")]),
              _vm._v(" "),
              _c("div", { staticClass: "img-post" }, [
                _c("img", {
                  staticClass: "img-responsive",
                  attrs: { src: "/static/img/profile-post.jpg" }
                })
              ])
            ])
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "media border-bottom-light-1" }, [
            _c("img", {
              staticClass: "rounded-circle mr-4 img-responsive",
              attrs: {
                src: "/static/avatars/user-9.jpg",
                alt: "user profile",
                width: "60",
                height: "60"
              }
            }),
            _vm._v(" "),
            _c("div", { staticClass: "media-body" }, [
              _c("span", { staticClass: "fw-bold mb-3 d-block" }, [
                _vm._v("Mark Anthony "),
                _c("span", { staticClass: "small" }, [_vm._v("@louisekate")])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "card pa-3 primary white--text" }, [
                _c("h3", { staticClass: "mb-0" }, [
                  _vm._v(
                    "The new common language will be more simple and regular than the existing European languages."
                  )
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "media border-bottom-light-1" }, [
            _c("img", {
              staticClass: "rounded-circle mr-4 img-responsive",
              attrs: {
                src: "/static/avatars/user-10.jpg",
                alt: "user profile",
                width: "60",
                height: "60"
              }
            }),
            _vm._v(" "),
            _c("div", { staticClass: "media-body" }, [
              _c("span", { staticClass: "fw-bold" }, [
                _vm._v("Annie Lee "),
                _c("span", { staticClass: "small" }, [_vm._v("@louisekate")])
              ]),
              _vm._v(" "),
              _c("p", [_vm._v("Posted 4 photos")]),
              _vm._v(" "),
              _c("ul", { staticClass: "list-inline row layout wrap" }, [
                _c("li", { staticClass: "flex xs6 sm3 md3 lg3" }, [
                  _c("img", {
                    staticClass: "img-responsive",
                    attrs: {
                      src: "/static/img/gallery1.jpg",
                      width: "200",
                      height: "200",
                      alt: "image post"
                    }
                  })
                ]),
                _vm._v(" "),
                _c("li", { staticClass: "flex xs6 sm3 md3 lg3" }, [
                  _c("img", {
                    staticClass: "img-responsive",
                    attrs: {
                      src: "/static/img/gallery2.jpg",
                      width: "200",
                      height: "200",
                      alt: "image post"
                    }
                  })
                ]),
                _vm._v(" "),
                _c("li", { staticClass: "flex xs6 sm3 md3 lg3" }, [
                  _c("img", {
                    staticClass: "img-responsive",
                    attrs: {
                      src: "/static/img/gallery3.jpg",
                      width: "200",
                      height: "200",
                      alt: "image post"
                    }
                  })
                ]),
                _vm._v(" "),
                _c("li", { staticClass: "flex xs6 sm3 md3 lg3" }, [
                  _c("img", {
                    staticClass: "img-responsive",
                    attrs: {
                      src: "/static/img/gallery4.jpg",
                      width: "200",
                      height: "200",
                      alt: "image post"
                    }
                  })
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "media border-bottom-light-1" }, [
            _c("img", {
              staticClass: "rounded-circle mr-4 img-responsive",
              attrs: {
                src: "/static/avatars/user-11.jpg",
                alt: "user profile",
                width: "60",
                height: "60"
              }
            }),
            _vm._v(" "),
            _c("div", { staticClass: "media-body" }, [
              _c("span", { staticClass: "fw-bold" }, [
                _vm._v("Mark Anthony "),
                _c("span", { staticClass: "small" }, [_vm._v("@louisekate")])
              ]),
              _vm._v(" "),
              _c("p", [_vm._v("Postd a new blog in website")]),
              _vm._v(" "),
              _c("div", { staticClass: "media media-full" }, [
                _c("img", {
                  staticClass: "img-responsive mr-4",
                  attrs: {
                    src: "/static/img/post-2.png",
                    alt: "post image",
                    width: "300",
                    height: "180"
                  }
                }),
                _vm._v(" "),
                _c("div", { staticClass: "media-body" }, [
                  _c("h5", { staticClass: "fw-bold" }, [
                    _vm._v("How to setup your estore in 10 min.")
                  ]),
                  _vm._v(" "),
                  _c("p", [
                    _vm._v(
                      "Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper "
                    )
                  ])
                ])
              ])
            ])
          ])
        ])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-5b039765", module.exports)
  }
}

/***/ }),

/***/ 1376:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("page-title-bar"),
      _vm._v(" "),
      _c(
        "v-container",
        { attrs: { fluid: "", "grid-list-xl": "", "pt-0": "" } },
        [
          _c(
            "v-layout",
            { attrs: { row: "", wrap: "" } },
            [
              _c(
                "v-flex",
                {
                  staticClass: "col-height-auto",
                  attrs: { xs12: "", md12: "", lg12: "", sm12: "" }
                },
                [
                  _c(
                    "div",
                    [
                      _c("user-detail", {
                        attrs: {
                          name: _vm.user.name,
                          email: _vm.user.email,
                          roles: _vm.user.roles
                        }
                      })
                    ],
                    1
                  )
                ]
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-39b55748", module.exports)
  }
}

/***/ })

});