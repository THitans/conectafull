webpackJsonp([34],{

/***/ 1309:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(1379)
/* template */
var __vue_template__ = __webpack_require__(1380)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/views/users/UsersEdit.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-58410ac8", Component.options)
  } else {
    hotAPI.reload("data-v-58410ac8", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1379:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_nprogress__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_nprogress___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_nprogress__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_Helpers_helpers__ = __webpack_require__(46);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      user: {
        valid: true,
        name: "",
        nameRules: [function (v) {
          return !!v || "Nome é obrigatório";
        }, function (v) {
          return v && v.length <= 100 || "Nome deve ser menor que 100";
        }],
        email: "",
        emailRules: [function (v) {
          return !!v || "Email é obrigatório";
        }, function (v) {
          return (/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(v) || "Email deve ser válido"
          );
        }],
        cpf: "",
        cpfRules: [function (v) {
          return !!v || "CPF é obrigatório";
        }, function (v) {
          return (/^\d+/.test(v) || "CPF deve ser válido"
          );
        }],
        selectType: null,
        selectRole: null,
        schema: '',
        types: ["Admin", "Cliente"],
        roles: [],
        checkbox: false
      },
      errors: {},
      links: [{
        id: 1,
        title: 'message.usersNewTitle',
        icon: 'ti-user mr-3 primary--text',
        path: '/users/new'
      }, {
        id: 3,
        title: 'message.usersList',
        icon: 'ti-list mr-3 success--text',
        path: '/users/list'
      }]
    };
  },
  mounted: function mounted() {
    // console.log(this.errors.cpf)
    this.getUser(this.$route.params.userId);
  },


  methods: {
    submit: function submit() {
      if (this.$refs.form.validate()) {
        console.log("form submit");
        this.updateUser(this.$route.params.userId);
      }
    },
    clear: function clear() {
      this.$refs.form.reset();
      this.errors = {};
    },
    getUser: function getUser(userId) {
      var _this = this;

      axios.get("/api/acl/user/edit/" + userId).then(function (response) {
        _this.loader = false;
        var result = response.data.data,
            user = result.user,
            roles = result.roles,
            selectedRoles = result.selectedRoles;

        _this.user.name = user.name;
        _this.user.email = user.email;
        _this.user.cpf = user.cpf;
        _this.user.selectType = user.type;
        _this.user.schema = user.schema;
        _this.user.roles = roles;
        _this.user.selectRole = selectedRoles;
      }).catch(function (error) {
        _this.loader = false;
        Vue.notify({
          group: 'loggedIn',
          type: 'error',
          text: 'Erro ao Buscar Usuário!'
        });
      });
    },
    updateUser: function updateUser(userId) {
      var _this2 = this;

      this.loader = true;
      var user = {
        name: this.user.name,
        email: this.user.email,
        cpf: this.user.cpf,
        type: this.user.selectType,
        roles: this.user.selectRole
      };

      axios.put("/api/acl/user/update/" + userId, user).then(function (response) {
        _this2.loader = false;
        Vue.notify({
          group: 'loggedIn',
          type: 'success',
          text: 'Atualizado com Sucesso!'
        });

        _this2.$router.push("/default/users/list");
      }).catch(function (error) {
        _this2.loader = false;
        Vue.notify({
          group: 'loggedIn',
          type: 'error',
          text: 'Erro ao atualizar!'
        });

        // Para erros da validção do laravel
        if (error.response.status == 422) {
          _this2.errors = {};
          _this2.errors = error.response.data.errors;
        }
      });
    },
    getMenuLink: function getMenuLink(path) {
      return '/' + Object(__WEBPACK_IMPORTED_MODULE_1_Helpers_helpers__["a" /* getCurrentAppLayout */])(this.$router) + path;
    }
  }
});

/***/ }),

/***/ 1380:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("page-title-bar"),
      _vm._v(" "),
      _c(
        "v-container",
        { attrs: { fluid: "", "grid-list-xl": "" } },
        [
          _c(
            "app-card",
            { attrs: { customClasses: "" } },
            [
              _c(
                "v-layout",
                {
                  attrs: {
                    "align-center": "",
                    "justify-end": "",
                    row: "",
                    "fill-height": ""
                  }
                },
                [
                  _c(
                    "v-menu",
                    {
                      staticClass: "align-end",
                      attrs: {
                        bottom: "",
                        "offset-y": "",
                        left: "",
                        "content-class": "userblock-dropdown",
                        "nudge-top": "-10",
                        "nudge-right": "0",
                        transition: "slide-y-transition"
                      }
                    },
                    [
                      _c(
                        "v-btn",
                        {
                          staticClass: "ma-0",
                          attrs: { slot: "activator", dark: "", icon: "" },
                          slot: "activator"
                        },
                        [
                          _c("v-icon", { attrs: { color: "grey" } }, [
                            _vm._v("more_vert")
                          ])
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "dropdown-content" },
                        [
                          _c(
                            "v-list",
                            { staticClass: "dropdown-list" },
                            [
                              _vm._l(_vm.links, function(link) {
                                return [
                                  _c(
                                    "v-list-tile",
                                    {
                                      key: link.id,
                                      attrs: { to: _vm.getMenuLink(link.path) }
                                    },
                                    [
                                      _c("i", { class: link.icon }),
                                      _vm._v(" "),
                                      _c("span", [
                                        _vm._v(_vm._s(_vm.$t(link.title)))
                                      ])
                                    ]
                                  )
                                ]
                              })
                            ],
                            2
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-layout",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    colClasses: "xs12",
                    heading: "Editar Usuário",
                    customClasses: "mb-30"
                  }
                },
                [
                  _c(
                    "v-form",
                    {
                      ref: "form",
                      attrs: { "lazy-validation": "" },
                      model: {
                        value: _vm.user.valid,
                        callback: function($$v) {
                          _vm.$set(_vm.user, "valid", $$v)
                        },
                        expression: "user.valid"
                      }
                    },
                    [
                      _vm.is("Desenvolvedor")
                        ? _c(
                            "v-alert",
                            {
                              attrs: {
                                value: true,
                                color: "success",
                                icon: "check_circle",
                                outline: ""
                              }
                            },
                            [
                              _vm._v(
                                "\n                        Schema: " +
                                  _vm._s(_vm.user.schema) +
                                  "\n                    "
                              )
                            ]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _c(
                        "v-layout",
                        { attrs: { row: "", wrap: "" } },
                        [
                          _c(
                            "v-flex",
                            { attrs: { xs12: "", sm12: "" } },
                            [
                              _c("v-select", {
                                attrs: {
                                  "item-text": "label",
                                  "item-value": "id",
                                  label: "Perfil",
                                  multiple: "",
                                  solo: "",
                                  chips: "",
                                  items: _vm.user.roles,
                                  rules: [
                                    function(v) {
                                      return !!v || "Este campo é obrigatório"
                                    }
                                  ],
                                  required: ""
                                },
                                model: {
                                  value: _vm.user.selectRole,
                                  callback: function($$v) {
                                    _vm.$set(_vm.user, "selectRole", $$v)
                                  },
                                  expression: "user.selectRole"
                                }
                              }),
                              _vm._v(" "),
                              _vm.errors.roles
                                ? _c(
                                    "div",
                                    { staticClass: "v-text-field__details" },
                                    [
                                      _c(
                                        "div",
                                        {
                                          staticClass:
                                            "v-messages theme--light error--text"
                                        },
                                        [
                                          _c(
                                            "div",
                                            {
                                              staticClass: "v-messages__wrapper"
                                            },
                                            [
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "v-messages__message"
                                                },
                                                [
                                                  _vm._v(
                                                    _vm._s(_vm.errors.roles[0])
                                                  )
                                                ]
                                              )
                                            ]
                                          )
                                        ]
                                      )
                                    ]
                                  )
                                : _vm._e()
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-flex",
                            { attrs: { xs12: "", sm6: "" } },
                            [
                              _c("v-text-field", {
                                attrs: {
                                  label: "Nome Completo",
                                  rules: _vm.user.nameRules,
                                  counter: 100,
                                  required: ""
                                },
                                model: {
                                  value: _vm.user.name,
                                  callback: function($$v) {
                                    _vm.$set(_vm.user, "name", $$v)
                                  },
                                  expression: "user.name"
                                }
                              }),
                              _vm._v(" "),
                              _vm.errors.name
                                ? _c(
                                    "div",
                                    { staticClass: "v-text-field__details" },
                                    [
                                      _c(
                                        "div",
                                        {
                                          staticClass:
                                            "v-messages theme--light error--text"
                                        },
                                        [
                                          _c(
                                            "div",
                                            {
                                              staticClass: "v-messages__wrapper"
                                            },
                                            [
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "v-messages__message"
                                                },
                                                [
                                                  _vm._v(
                                                    _vm._s(_vm.errors.name[0])
                                                  )
                                                ]
                                              )
                                            ]
                                          )
                                        ]
                                      )
                                    ]
                                  )
                                : _vm._e()
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-flex",
                            { attrs: { xs12: "", sm6: "" } },
                            [
                              _c("v-text-field", {
                                attrs: {
                                  label: "Email",
                                  rules: _vm.user.emailRules,
                                  required: ""
                                },
                                model: {
                                  value: _vm.user.email,
                                  callback: function($$v) {
                                    _vm.$set(_vm.user, "email", $$v)
                                  },
                                  expression: "user.email"
                                }
                              }),
                              _vm._v(" "),
                              _vm.errors.email
                                ? _c(
                                    "div",
                                    { staticClass: "v-text-field__details" },
                                    [
                                      _c(
                                        "div",
                                        {
                                          staticClass:
                                            "v-messages theme--light error--text"
                                        },
                                        [
                                          _c(
                                            "div",
                                            {
                                              staticClass: "v-messages__wrapper"
                                            },
                                            [
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "v-messages__message"
                                                },
                                                [
                                                  _vm._v(
                                                    _vm._s(_vm.errors.email[0])
                                                  )
                                                ]
                                              )
                                            ]
                                          )
                                        ]
                                      )
                                    ]
                                  )
                                : _vm._e()
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-flex",
                            { attrs: { xs12: "", sm6: "" } },
                            [
                              _c("v-text-field", {
                                attrs: {
                                  label: "CPF",
                                  rules: _vm.user.cpfRules,
                                  required: ""
                                },
                                model: {
                                  value: _vm.user.cpf,
                                  callback: function($$v) {
                                    _vm.$set(_vm.user, "cpf", $$v)
                                  },
                                  expression: "user.cpf"
                                }
                              }),
                              _vm._v(" "),
                              _vm.errors.cpf
                                ? _c(
                                    "div",
                                    { staticClass: "v-text-field__details" },
                                    [
                                      _c(
                                        "div",
                                        {
                                          staticClass:
                                            "v-messages theme--light error--text"
                                        },
                                        [
                                          _c(
                                            "div",
                                            {
                                              staticClass: "v-messages__wrapper"
                                            },
                                            [
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "v-messages__message"
                                                },
                                                [
                                                  _vm._v(
                                                    _vm._s(_vm.errors.cpf[0])
                                                  )
                                                ]
                                              )
                                            ]
                                          )
                                        ]
                                      )
                                    ]
                                  )
                                : _vm._e()
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-flex",
                            { attrs: { xs12: "", sm6: "" } },
                            [
                              _c("v-select", {
                                attrs: {
                                  label: "Tipo de Usuário",
                                  items: _vm.user.types,
                                  rules: [
                                    function(v) {
                                      return !!v || "Este campo é obrigatório"
                                    }
                                  ],
                                  required: ""
                                },
                                model: {
                                  value: _vm.user.selectType,
                                  callback: function($$v) {
                                    _vm.$set(_vm.user, "selectType", $$v)
                                  },
                                  expression: "user.selectType"
                                }
                              }),
                              _vm._v(" "),
                              _vm.errors.types
                                ? _c(
                                    "div",
                                    { staticClass: "v-text-field__details" },
                                    [
                                      _c(
                                        "div",
                                        {
                                          staticClass:
                                            "v-messages theme--light error--text"
                                        },
                                        [
                                          _c(
                                            "div",
                                            {
                                              staticClass: "v-messages__wrapper"
                                            },
                                            [
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "v-messages__message"
                                                },
                                                [
                                                  _vm._v(
                                                    _vm._s(_vm.errors.types[0])
                                                  )
                                                ]
                                              )
                                            ]
                                          )
                                        ]
                                      )
                                    ]
                                  )
                                : _vm._e()
                            ],
                            1
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-btn",
                        {
                          attrs: {
                            disabled: !_vm.user.valid,
                            color: "success"
                          },
                          on: { click: _vm.submit }
                        },
                        [
                          _vm._v(
                            "\n                        " +
                              _vm._s(_vm.$t("message.update")) +
                              "\n                    "
                          )
                        ]
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-58410ac8", module.exports)
  }
}

/***/ })

});