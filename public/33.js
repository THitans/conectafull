webpackJsonp([33],{

/***/ 1492:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(1493)
/* template */
var __vue_template__ = __webpack_require__(1494)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/views/users/UsersPassword.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-408b09ad", Component.options)
  } else {
    hotAPI.reload("data-v-408b09ad", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1493:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_nprogress__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_nprogress___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_nprogress__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_Helpers_helpers__ = __webpack_require__(46);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      loader: false,
      user: {
        valid: true,
        currentPassword: "",
        currentPasswordRules: [function (v) {
          return !!v || "Nome é obrigatório";
        }, function (v) {
          return v && v.length <= 100 || "Nome deve ser menor que 100";
        }],
        newPassword: "",
        newPasswordRules: [function (v) {
          return !!v || "Email é obrigatório";
        }, function (v) {
          return (/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(v) || "Email deve ser válido"
          );
        }],
        newPasswordConfirmation: "",
        newPasswordConfirmationRules: [function (v) {
          return v.length == 0 || v.length == 11 || "CPF deve ter 11 números";
        }]
      },
      errors: {},
      links: [{
        id: 1,
        title: 'message.usersNewTitle',
        icon: 'ti-user mr-3 primary--text',
        path: '/users/new'
      }, {
        id: 3,
        title: 'message.usersList',
        icon: 'ti-list mr-3 success--text',
        path: '/users/list'
      }]
    };
  },
  mounted: function mounted() {
    // console.log(this.errors.cpf)
  },


  methods: {
    submit: function submit() {
      if (this.$refs.form.validate()) {
        this.changePassword();
      }
    },
    clear: function clear() {
      this.$refs.form.reset();
      this.errors = {};
    },
    changePassword: function changePassword() {
      var _this = this;

      this.loader = true;
      __WEBPACK_IMPORTED_MODULE_0_nprogress___default.a.start();
      var user = {
        currentPassword: this.user.currentPassword,
        password: this.user.newPassword,
        password_confirmation: this.user.newPasswordConfirmation
      };
      axios.post("/api/acl/user/store", user).then(function (response) {
        _this.$router.push("/default/users/list");
        __WEBPACK_IMPORTED_MODULE_0_nprogress___default.a.done();
        _this.loader = false;
        Vue.notify({
          group: 'loggedIn',
          type: 'success',
          text: 'Cadastro realizado com Sucesso!'
        });
        _this.clear();
      }).catch(function (error) {
        __WEBPACK_IMPORTED_MODULE_0_nprogress___default.a.done();
        _this.loader = false;
        Vue.notify({
          group: 'loggedIn',
          type: 'error',
          text: 'Erro ao salvar!'
        });

        // Para erros da validação do laravel
        if (error.response.status == 422) {
          _this.errors = {};
          _this.errors = error.response.data.errors;
        }
        console.log(error.response);
      });
    },
    getMenuLink: function getMenuLink(path) {
      return '/' + Object(__WEBPACK_IMPORTED_MODULE_1_Helpers_helpers__["a" /* getCurrentAppLayout */])(this.$router) + path;
    }
  }
});

/***/ }),

/***/ 1494:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function(){},staticRenderFns:[]}
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-408b09ad", module.exports)
  }
}

/***/ })

});