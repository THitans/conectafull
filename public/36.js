webpackJsonp([36],{

/***/ 1311:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(1383)
/* template */
var __vue_template__ = __webpack_require__(1384)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/views/acl/RolesList.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-fa4befb2", Component.options)
  } else {
    hotAPI.reload("data-v-fa4befb2", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1383:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_Helpers_helpers__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_nprogress__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_nprogress___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_nprogress__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      loader: true,
      headers: [{
        text: "#",
        sortable: false,
        value: "#"
      }, {
        text: "Perfil",
        sortable: false,
        value: "name"
      }, {
        text: "",
        sortable: false,
        value: ""
      }],
      rolesList: [],
      connectUsersList: null,
      links: [{
        id: 1,
        title: 'Novo Perfil',
        icon: 'ti-user mr-3 primary--text',
        path: '/acl/roles/new',
        shield: 'role.create'
      }]
    };
  },
  mounted: function mounted() {
    this.getRoles();
  },

  methods: {
    getRoles: function getRoles() {
      var _this = this;

      axios.get("/api/acl/role/index").then(function (response) {
        _this.loader = false;
        _this.rolesList = response.data.data;
      }).catch(function (error) {
        __WEBPACK_IMPORTED_MODULE_1_nprogress___default.a.done();
        Vue.notify({
          group: 'loggedIn',
          type: 'error',
          text: 'Erro ao Listar!'
        });
        console.log(error);
      });
    },
    onDeleteCartItem: function onDeleteCartItem() {
      console.log('dispara delete cart item');
    },
    getMenuLink: function getMenuLink(path) {
      return '/' + Object(__WEBPACK_IMPORTED_MODULE_0_Helpers_helpers__["a" /* getCurrentAppLayout */])(this.$router) + path;
    }
  }
});

/***/ }),

/***/ 1384:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("page-title-bar"),
      _vm._v(" "),
      _c("app-section-loader", { attrs: { status: _vm.loader } }),
      _vm._v(" "),
      _c(
        "v-container",
        { attrs: { fluid: "", "grid-list-xl": "" } },
        [
          _c(
            "v-layout",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    colClasses: "xs12",
                    heading: "Lista de Perfis",
                    customClasses: "mb-30"
                  }
                },
                [
                  _c(
                    "v-layout",
                    {
                      attrs: {
                        "align-center": "",
                        "justify-end": "",
                        row: "",
                        "fill-height": ""
                      }
                    },
                    [
                      _c(
                        "v-menu",
                        {
                          staticClass: "align-end",
                          attrs: {
                            bottom: "",
                            "offset-y": "",
                            left: "",
                            "content-class": "userblock-dropdown",
                            "nudge-top": "-10",
                            "nudge-right": "0",
                            transition: "slide-y-transition"
                          }
                        },
                        [
                          _c(
                            "v-btn",
                            {
                              staticClass: "ma-0",
                              attrs: { slot: "activator", dark: "", icon: "" },
                              slot: "activator"
                            },
                            [
                              _c("v-icon", { attrs: { color: "grey" } }, [
                                _vm._v("more_vert")
                              ])
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "dropdown-content" },
                            [
                              _c(
                                "v-list",
                                { staticClass: "dropdown-list" },
                                [
                                  _vm._l(_vm.links, function(link) {
                                    return _vm.shield(link.shield)
                                      ? [
                                          _c(
                                            "v-list-tile",
                                            {
                                              key: link.id,
                                              attrs: {
                                                to: _vm.getMenuLink(link.path)
                                              }
                                            },
                                            [
                                              _c("i", { class: link.icon }),
                                              _vm._v(" "),
                                              _c("span", [
                                                _vm._v(_vm._s(link.title))
                                              ])
                                            ]
                                          )
                                        ]
                                      : _vm._e()
                                  })
                                ],
                                2
                              )
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "table-responsive" },
                    [
                      _c("app-section-loader", {
                        attrs: { status: _vm.loader }
                      }),
                      _vm._v(" "),
                      _c("v-data-table", {
                        attrs: {
                          headers: _vm.headers,
                          items: _vm.rolesList,
                          "hide-actions": ""
                        },
                        scopedSlots: _vm._u([
                          {
                            key: "headers",
                            fn: function(props) {
                              return [
                                _c(
                                  "tr",
                                  _vm._l(props.headers, function(header) {
                                    return _c(
                                      "th",
                                      {
                                        key: header.text,
                                        staticClass: "text-xs-left fw-bold"
                                      },
                                      [
                                        _vm._v(
                                          "\n                                    " +
                                            _vm._s(header.text) +
                                            "\n                                "
                                        )
                                      ]
                                    )
                                  }),
                                  0
                                )
                              ]
                            }
                          },
                          {
                            key: "items",
                            fn: function(props) {
                              return [
                                _c("td", [_vm._v(_vm._s(props.item.id))]),
                                _vm._v(" "),
                                _c(
                                  "td",
                                  [
                                    _c("v-badge", { staticClass: "primary" }, [
                                      _vm._v(_vm._s(props.item.name))
                                    ])
                                  ],
                                  1
                                ),
                                _vm._v(" "),
                                _c(
                                  "td",
                                  { staticClass: "text-xs-center fw-bold" },
                                  [
                                    ![1, 3].includes(props.item.id) &&
                                    _vm.shield("role.edit")
                                      ? _c(
                                          "v-btn",
                                          {
                                            key: props.item.id,
                                            attrs: {
                                              to: _vm.getMenuLink(
                                                "/acl/roles/edit/" +
                                                  props.item.id
                                              ),
                                              flat: "",
                                              icon: "",
                                              color: "amber"
                                            },
                                            on: {
                                              click: function($event) {
                                                _vm.onDeleteCartItem(props.item)
                                              }
                                            }
                                          },
                                          [
                                            _c("v-icon", {
                                              staticClass: "zmdi zmdi-edit"
                                            })
                                          ],
                                          1
                                        )
                                      : _vm._e(),
                                    _vm._v(" "),
                                    ![1, 2, 3].includes(props.item.id) &&
                                    _vm.shield("role.destroy")
                                      ? _c(
                                          "v-btn",
                                          {
                                            attrs: {
                                              flat: "",
                                              icon: "",
                                              color: "error"
                                            },
                                            on: {
                                              click: function($event) {
                                                _vm.onDeleteCartItem(props.item)
                                              }
                                            }
                                          },
                                          [
                                            _c("v-icon", {
                                              staticClass: "zmdi zmdi-delete"
                                            })
                                          ],
                                          1
                                        )
                                      : _vm._e()
                                  ],
                                  1
                                )
                              ]
                            }
                          }
                        ])
                      })
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-fa4befb2", module.exports)
  }
}

/***/ })

});