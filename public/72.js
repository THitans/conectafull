webpackJsonp([72],{

/***/ 1512:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(2283)
}
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(2285)
/* template */
var __vue_template__ = __webpack_require__(2286)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-da5b3a54"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/views/team/TeamMemberCreate.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-da5b3a54", Component.options)
  } else {
    hotAPI.reload("data-v-da5b3a54", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 2283:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(2284);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("1c13d1f6", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-da5b3a54\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./TeamMemberCreate.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-da5b3a54\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./TeamMemberCreate.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 2284:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)(false);
// imports


// module
exports.push([module.i, "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", ""]);

// exports


/***/ }),

/***/ 2285:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_nprogress__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_nprogress___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_nprogress__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_Helpers_helpers__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_AppCard_AppCardHeading__ = __webpack_require__(569);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_AppCard_AppCardHeading___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__components_AppCard_AppCardHeading__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



//import { mapGetters } from "vuex";



/* harmony default export */ __webpack_exports__["default"] = ({

    components: { AppCardHeading: __WEBPACK_IMPORTED_MODULE_2__components_AppCard_AppCardHeading___default.a },
    data: function data() {

        return {
            selectCountry: ["United Kingdom"],
            addNewCardDialog: false,
            headers: [{ text: "Product", sortable: false, align: "center" }, { text: "", sortable: false }, { text: "Quantity", sortable: false, align: "center" }, { text: "Total", sortable: false, align: "center" }],
            form1: {
                id: null,
                valid: false,
                name: "",
                nameLast: "",
                nameRules: [function (v) {
                    return !!v || "Nome é Obrigatório";
                }, function (v) {
                    return v.length <= 10 || "No máximo 10 caractéres permitido";
                }],
                email: "",
                emailRules: [function (v) {
                    return !!v || "E-mail é obrigatório";
                }, function (v) {
                    return (/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(v) || "E-mail deve ser válido"
                    );
                }]
            },
            userLogged: this.getUserLogged()
        };
    },

    name: "VincularConta",
    conta: function conta() {
        return "adicionar Memb";
    },

    methods: {
        submit: function submit() {
            console.log("form submit");
            if (this.$refs.form.validate()) {
                this.createUser();
            }
        },
        clear: function clear() {
            this.$refs.form.reset();
            this.errors = {};
        },
        createUser: function createUser() {
            var _this = this;

            //NProgress.start();

            var user = {
                name: this.form1.name,
                last_name: this.form1.nameLast,
                email: this.form1.email,
                user_id: this.userLogged.id
            };
            axios.post("/user-team/store", user).then(function (response) {
                //Nprogress.done();
                Vue.notify({
                    group: 'loggedIn',
                    type: 'success',
                    text: 'Cadastro realizado com Sucesso!'
                });

                console.log('cadastrado com sucesso');
                _this.clear();
            }).catch(function (error) {
                //Nprogress.done();
                Vue.notify({
                    group: 'loggedIn',
                    type: 'error',
                    text: 'Erro ao salvar!'
                });

                // Para erros da validção do laravel
                if (error.response.status == 422) {
                    _this.errors = {};
                    _this.errors = error.response.data.errors;
                }
            });
        },
        getUserLogged: function getUserLogged() {
            var user = JSON.parse(localStorage.getItem('user'));

            if (user == null) {
                return null;
            }

            return user.data.data;
        }
    }
});

/***/ }),

/***/ 2286:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-container",
    { attrs: { fluid: "", "grid-list-xl": "", "py-0": "" } },
    [
      _c("page-title-bar"),
      _vm._v(" "),
      _c(
        "v-card",
        [
          _c(
            "v-form",
            {
              ref: "form",
              attrs: { "lazy-validation": "" },
              model: {
                value: _vm.form1.valid,
                callback: function($$v) {
                  _vm.$set(_vm.form1, "valid", $$v)
                },
                expression: "form1.valid"
              }
            },
            [
              _c("v-card-title", [
                _c("span", { staticClass: "headline" }, [
                  _vm._v(_vm._s(_vm.$t("message.teamAddNew")))
                ])
              ]),
              _vm._v(" "),
              _c("v-spacer"),
              _vm._v(" "),
              _c(
                "v-card-media",
                [
                  _c(
                    "v-container",
                    { attrs: { fluid: "", "grid-list-xl": "", "py-0": "" } },
                    [
                      _c(
                        "app-card",
                        {
                          attrs: {
                            heading: _vm.$t("message.infUser"),
                            customClasses: "mb-30"
                          }
                        },
                        [
                          _c(
                            "v-layout",
                            { attrs: { row: "", wrap: "" } },
                            [
                              _c(
                                "v-flex",
                                { attrs: { xs12: "", sm4: "" } },
                                [
                                  _c("v-text-field", {
                                    attrs: {
                                      label: "Primeiro Nome",
                                      rules: _vm.form1.nameRules,
                                      counter: 10,
                                      required: ""
                                    },
                                    model: {
                                      value: _vm.form1.name,
                                      callback: function($$v) {
                                        _vm.$set(_vm.form1, "name", $$v)
                                      },
                                      expression: "form1.name"
                                    }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-flex",
                                { attrs: { xs12: "", sm4: "" } },
                                [
                                  _c("v-text-field", {
                                    attrs: {
                                      label: "Sobronome",
                                      counter: 30,
                                      required: ""
                                    },
                                    model: {
                                      value: _vm.form1.nameLast,
                                      callback: function($$v) {
                                        _vm.$set(_vm.form1, "nameLast", $$v)
                                      },
                                      expression: "form1.nameLast"
                                    }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-flex",
                                { attrs: { xs12: "", sm4: "" } },
                                [
                                  _c("v-text-field", {
                                    attrs: {
                                      label: "E-mail",
                                      rules: _vm.form1.emailRules,
                                      required: ""
                                    },
                                    model: {
                                      value: _vm.form1.email,
                                      callback: function($$v) {
                                        _vm.$set(_vm.form1, "email", $$v)
                                      },
                                      expression: "form1.email"
                                    }
                                  })
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-card-actions",
                [
                  _c(
                    "v-btn",
                    { attrs: { color: "primary", to: "/default/team" } },
                    [_vm._v("Cancelar")]
                  ),
                  _vm._v(" "),
                  _c(
                    "v-btn",
                    {
                      attrs: { disabled: !_vm.form1.valid, color: "success" },
                      on: { click: _vm.submit }
                    },
                    [
                      _vm._v(
                        "\n                    " +
                          _vm._s(_vm.$t("message.submit")) +
                          "\n                "
                      )
                    ]
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-da5b3a54", module.exports)
  }
}

/***/ })

});