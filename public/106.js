webpackJsonp([106],{

/***/ 1511:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(2279)
}
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(2281)
/* template */
var __vue_template__ = __webpack_require__(2282)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-68fcda9c"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/views/team/TeamCreate.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-68fcda9c", Component.options)
  } else {
    hotAPI.reload("data-v-68fcda9c", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 2279:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(2280);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("174d1758", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-68fcda9c\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./TeamCreate.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-68fcda9c\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./TeamCreate.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 2280:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)(false);
// imports


// module
exports.push([module.i, "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", ""]);

// exports


/***/ }),

/***/ 2281:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_nprogress__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_nprogress___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_nprogress__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_Helpers_helpers__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__store_modules_auth_index__ = __webpack_require__(307);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_vuex__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_AppCard_AppCardHeading__ = __webpack_require__(569);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_AppCard_AppCardHeading___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__components_AppCard_AppCardHeading__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//









/* harmony default export */ __webpack_exports__["default"] = ({

    components: { AppCardHeading: __WEBPACK_IMPORTED_MODULE_4__components_AppCard_AppCardHeading___default.a },
    data: function data() {
        return {
            selectCountry: ["United Kingdom"],
            addNewCardDialog: false,
            loader: false,
            selectDeletedMember: null,
            headers: [{ text: "Product", sortable: false, align: "center" }, { text: "", sortable: false }, { text: "Quantity", sortable: false, align: "center" }, { text: "Total", sortable: false, align: "center" }],
            form1: {
                valid: false,
                name: "",
                nameLast: "",
                nameRules: [function (v) {
                    return !!v || "Nome é Obrigatório";
                }, function (v) {
                    return v.length <= 10 || "No máximo 10 caractéres permitido";
                }],
                email: "",
                emailRules: [function (v) {
                    return !!v || "E-mail é obrigatório";
                }, function (v) {
                    return (/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(v) || "E-mail deve ser válido"
                    );
                }]
            },
            userLogged: this.getUserLogged(),
            listaUserTeam: []
        };
    },

    name: "VincularConta",
    conta: function conta() {
        return "adicionar Memb";
    },
    mounted: function mounted() {
        console.log(localStorage.getItem('user'));
        this.getListTeam();
    },

    methods: {
        // confirmation dialog to delete
        onDeleteEmail: function onDeleteEmail(member) {
            this.$refs.deleteConfirmationDialog.openDialog();
            this.selectDeletedMember = member;
        },
        deleteEmail: function deleteEmail() {
            this.$refs.deleteConfirmationDialog.close();
            this.loader = true;
            var team = this.listaUserTeam.data;
            var index = team.indexOf(this.selectDeletedMember);
            this.removerUserTeam(index);
        },
        removerUserTeam: function removerUserTeam(index) {
            var _this = this;

            var user = {
                name: this.selectDeletedMember.name,
                last_name: this.selectDeletedMember.nameLast,
                email: this.selectDeletedMember.email,
                user_id_member: this.selectDeletedMember.id,
                user_id: this.userLogged.id
            };

            axios.post("/user-team/remove-member/", user).then(function (response) {

                console.log(response.data);

                console.log('cadastrado com sucesso');

                setTimeout(function () {
                    _this.loader = false;
                    _this.selectDeletedEmail = null;
                    _this.listaUserTeam.data.splice(index, 1);
                    //this.snackbar = true;
                    //this.snackbarMessage = "Email Deleted Successfully!";
                }, 1000);
            }).catch(function (error) {

                Vue.notify({
                    group: 'loggedIn',
                    type: 'error',
                    text: 'Erro ao Remover Membro!'
                });
                _this.loader = false;

                // Para erros da validção do laravel
                if (error.response.status == 422) {
                    _this.errors = {};
                    _this.errors = error.response.data.errors;
                }
            });
        },
        getListTeam: function getListTeam() {
            var _this2 = this;

            this.loader = true;
            axios.get("/user-team/list/" + this.userLogged.id).then(function (response) {
                //Nprogress.done();
                console.log(response.data);
                _this2.listaUserTeam = response.data;
                console.log('cadastrado com sucesso');
                _this2.loader = false;
            }).catch(function (error) {
                //Nprogress.done();
                Vue.notify({
                    group: 'loggedIn',
                    type: 'error',
                    text: 'Erro ao listar Grupo!'
                });
                _this2.loader = false;
                // Para erros da validção do laravel
                if (error.response.status == 422) {
                    _this2.errors = {};
                    _this2.errors = error.response.data.errors;
                }
            });
        },
        getUserLogged: function getUserLogged() {
            var user = JSON.parse(localStorage.getItem('user'));

            if (user == null) {
                return null;
            }

            return user.data.data;
        }
    }
});

/***/ }),

/***/ 2282:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-container",
    { attrs: { fluid: "", "grid-list-xl": "", "py-0": "" } },
    [
      _c("page-title-bar"),
      _vm._v(" "),
      _c(
        "app-card",
        { attrs: { heading: "", customClasses: "mb-30" } },
        [
          _c(
            "v-form",
            [
              _c(
                "v-btn",
                { attrs: { to: "team-add-user", color: "primary" } },
                [
                  _vm._v(
                    "\n                " +
                      _vm._s(_vm.$t("message.teamAddNew")) +
                      "\n            "
                  )
                ]
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c("v-spacer"),
      _vm._v(" "),
      _c("v-spacer"),
      _vm._v(" "),
      _c(
        "app-card",
        { attrs: { heading: "Membros da Equipe", customClasses: "mb-90" } },
        [
          _c("div", { staticClass: "text--accent-1 justify-center" }, [
            _c("p", [
              _vm._v(
                "\n                Aqui você será capaz de cadastrar membros de sua equipe para acessar sua conta.\n\n                Membros de sua equipe terão acesso total somente aos clientes que eles forem vinculados.\n\n                Para um membro ter acesso aos relatórios e dashboards ele precisa estar vinculado a um cliente.\n            "
              )
            ])
          ])
        ]
      ),
      _vm._v(" "),
      _vm._l(_vm.listaUserTeam.data, function(use) {
        return _c(
          "app-card",
          { key: use.id, attrs: { customClasses: "mb-30" } },
          [
            _c(
              "v-layout",
              { attrs: { row: "", wrap: "" } },
              [
                _c("v-flex", { attrs: { xs6: "", sm2: "" } }, [
                  _c(
                    "span",
                    { staticClass: "pt-4 d-block text-danger fw-bold" },
                    [_vm._v(" " + _vm._s(use.name))]
                  )
                ]),
                _vm._v(" "),
                _c("v-flex", { attrs: { xs6: "", sm4: "" } }, [
                  _c("span", { staticClass: " pt-4 d-block" }, [
                    _vm._v(" " + _vm._s(use.email))
                  ])
                ]),
                _vm._v(" "),
                _c(
                  "v-layout",
                  { attrs: { "icon-box": "", flex: "", lg1: "", sm2: "" } },
                  [
                    _c("v-flex", [
                      _c("span", [_c("i", { staticClass: "ti-reload" })])
                    ])
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "v-layout",
                  { attrs: { "icon-box": "", flex: "", lg1: "", sm2: "" } },
                  [
                    _c("v-flex", [
                      _c("span", [_c("i", { staticClass: "ti-link" })])
                    ])
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "v-layout",
                  { attrs: { row: "", wrap: "", "icon-box": "", sm2: "" } },
                  [
                    _c(
                      "v-flex",
                      { attrs: { xs12: "", sm6: "", md4: "", lg2: "" } },
                      [
                        _c(
                          "v-btn",
                          {
                            attrs: {
                              color: "error",
                              fab: "",
                              small: "",
                              dark: ""
                            }
                          },
                          [
                            _c(
                              "v-icon",
                              {
                                on: {
                                  click: function($event) {
                                    _vm.onDeleteEmail(use)
                                  }
                                }
                              },
                              [_vm._v("ti-trash")]
                            )
                          ],
                          1
                        )
                      ],
                      1
                    )
                  ],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c("delete-confirmation-dialog", {
              ref: "deleteConfirmationDialog",
              refInFor: true,
              attrs: {
                heading: "Remover de sua equipe?",
                message:
                  "Vocẽ deseja realmente remover o usuário da sua equipe?"
              },
              on: { onConfirm: _vm.deleteEmail }
            }),
            _vm._v(" "),
            _c("app-section-loader", { attrs: { status: _vm.loader } })
          ],
          1
        )
      })
    ],
    2
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-68fcda9c", module.exports)
  }
}

/***/ })

});