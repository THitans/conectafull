webpackJsonp([112],{

/***/ 1529:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_axios__ = __webpack_require__(568);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_axios___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_axios__);


var instance = __WEBPACK_IMPORTED_MODULE_0_axios___default.a.create({
    baseURL: 'http://reactify.theironnetwork.org/data/'
});

instance.defaults.headers.common['Access-Control-Allow-Headers'] = 'X-CSRF-Token';

/* harmony default export */ __webpack_exports__["a"] = (instance);

/***/ }),

/***/ 187:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(2277)
/* template */
var __vue_template__ = __webpack_require__(2278)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/views/users/UsersList.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-76d159a0", Component.options)
  } else {
    hotAPI.reload("data-v-76d159a0", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 2277:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_Api__ = __webpack_require__(1529);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_helpers__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_Helpers_helpers__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//






/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      loader: true,
      usersList: null,
      connectUsersList: null,
      radioGroup: 1,
      switch1: true,
      userLinks: [{
        id: 1,
        title: 'message.usersNewTitle',
        icon: 'ti-user mr-3 primary--text',
        path: '/users/new'
      }, {
        id: 3,
        title: 'message.usersList',
        icon: 'ti-list mr-3 success--text',
        path: '/users/list'
      }]
    };
  },
  mounted: function mounted() {
    this.getUsers();
    this.getConnections();
  },

  methods: {
    moment: function moment(date) {
      console.log(date);
      return __WEBPACK_IMPORTED_MODULE_3_moment___default()(date * 1000).format('DD MMM YYYY');
    },
    getImgSrc: function getImgSrc(connectedUsers) {
      if (this.connectUsersList) {
        for (var i = 0; i < this.connectUsersList.length; i++) {
          var user = this.connectUsersList[i];
          if (connectedUsers === user.id) {
            return user.img;
          }
        }
      }
    },
    getUsers: function getUsers() {
      var _this = this;

      axios.get("/acl/user/index").then(function (response) {
        _this.loader = false;
        _this.usersList = response.data.data;
      }).catch(function (error) {
        _this.loader = false;
        _this.$store.dispatch("verifyUserAuth", error.response);
        Vue.notify({
          group: 'loggedIn',
          type: 'error',
          text: 'Erro ao Listar!'
        });
      });
    },
    getConnections: function getConnections() {
      var _this2 = this;

      __WEBPACK_IMPORTED_MODULE_0_Api__["a" /* default */].get("vuely/connections.js").then(function (response) {
        _this2.connectUsersList = response.data;
      }).catch(function (error) {
        console.log(error);
      });
    },
    getImagePeople: function getImagePeople() {
      return '/static/avatars/user-' + Object(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["a" /* generateNumber */])(1, 36) + '.jpg';
    },
    generateNumber: function generateNumber(min, max) {
      return Object(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["a" /* generateNumber */])(min, max);
    },
    getMenuLink: function getMenuLink(path) {
      return '/' + Object(__WEBPACK_IMPORTED_MODULE_2_Helpers_helpers__["b" /* getCurrentAppLayout */])(this.$router) + path;
    }
  }
});

/***/ }),

/***/ 2278:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("page-title-bar"),
      _vm._v(" "),
      _c("app-section-loader", { attrs: { status: _vm.loader } }),
      _vm._v(" "),
      _c(
        "v-container",
        { attrs: { fluid: "", "grid-list-xl": "" } },
        [
          _c(
            "app-card",
            { attrs: { customClasses: "" } },
            [
              _c(
                "v-layout",
                {
                  attrs: {
                    "align-center": "",
                    "justify-end": "",
                    row: "",
                    "fill-height": ""
                  }
                },
                [
                  _c(
                    "v-btn",
                    {
                      attrs: { color: "primary", disabled: "" },
                      on: { click: function($event) {} }
                    },
                    [_vm._v("\n                    Todos\n                ")]
                  ),
                  _vm._v(" "),
                  _c(
                    "v-btn",
                    {
                      attrs: { color: "warning" },
                      on: { click: function($event) {} }
                    },
                    [_vm._v("\n                    Admins\n                ")]
                  ),
                  _vm._v(" "),
                  _c(
                    "v-btn",
                    {
                      attrs: { color: "warning" },
                      on: { click: function($event) {} }
                    },
                    [_vm._v("\n                    Clientes\n                ")]
                  ),
                  _vm._v(" "),
                  _c("v-spacer"),
                  _vm._v(" "),
                  _c(
                    "v-menu",
                    {
                      staticClass: "align-end",
                      attrs: {
                        bottom: "",
                        "offset-y": "",
                        left: "",
                        "content-class": "userblock-dropdown",
                        "nudge-top": "-10",
                        "nudge-right": "0",
                        transition: "slide-y-transition"
                      }
                    },
                    [
                      _c(
                        "v-btn",
                        {
                          staticClass: "ma-0",
                          attrs: { slot: "activator", dark: "", icon: "" },
                          slot: "activator"
                        },
                        [
                          _c("v-icon", { attrs: { color: "grey" } }, [
                            _vm._v("more_vert")
                          ])
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "dropdown-content" },
                        [
                          _c(
                            "v-list",
                            { staticClass: "dropdown-list" },
                            [
                              _vm._l(_vm.userLinks, function(userLink) {
                                return [
                                  _c(
                                    "v-list-tile",
                                    {
                                      key: userLink.id,
                                      attrs: {
                                        to: _vm.getMenuLink(userLink.path)
                                      }
                                    },
                                    [
                                      _c("i", { class: userLink.icon }),
                                      _vm._v(" "),
                                      _c("span", [
                                        _vm._v(_vm._s(_vm.$t(userLink.title)))
                                      ])
                                    ]
                                  )
                                ]
                              })
                            ],
                            2
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _vm.usersList !== null
            ? _c(
                "v-layout",
                { attrs: { row: "", wrap: "" } },
                _vm._l(_vm.usersList, function(user, index) {
                  return _c(
                    "app-card",
                    {
                      key: index,
                      attrs: { colClasses: "xs12 sm6 md4", footer: true }
                    },
                    [
                      _c(
                        "div",
                        { staticClass: "user-image text-xs-center mb-3" },
                        [
                          _c("img", {
                            staticClass: "img-responsive rounded-circle",
                            attrs: {
                              src: _vm.getImagePeople(),
                              alt: "user images",
                              width: "95",
                              height: "95"
                            }
                          })
                        ]
                      ),
                      _vm._v(" "),
                      _c("div", { staticClass: "user-list-content" }, [
                        _c(
                          "div",
                          { staticClass: "text-xs-center" },
                          [
                            _c("h3", { staticClass: "fw-bold" }, [
                              _vm._v(_vm._s(user.name))
                            ]),
                            _vm._v(" "),
                            user.type == "Admin"
                              ? _c(
                                  "v-chip",
                                  {
                                    attrs: {
                                      color: "green",
                                      "text-color": "white"
                                    }
                                  },
                                  [
                                    _c(
                                      "v-avatar",
                                      [
                                        _c("v-icon", [_vm._v("account_circle")])
                                      ],
                                      1
                                    ),
                                    _vm._v(
                                      "\n                            " +
                                        _vm._s(user.type) +
                                        "\n                        "
                                    )
                                  ],
                                  1
                                )
                              : _vm._e(),
                            _vm._v(" "),
                            user.type == "Cliente"
                              ? _c(
                                  "v-chip",
                                  {
                                    attrs: {
                                      color: "teal",
                                      "text-color": "white"
                                    }
                                  },
                                  [
                                    _vm._v(
                                      "\n                            " +
                                        _vm._s(user.type) +
                                        "\n                            "
                                    ),
                                    _c("v-icon", { attrs: { right: "" } }, [
                                      _vm._v("star")
                                    ])
                                  ],
                                  1
                                )
                              : _vm._e(),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "social-list clearfix mb-4" },
                              [
                                _c("ul", {
                                  staticClass: "list-inline d-inline-block"
                                })
                              ]
                            )
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          {
                            staticClass:
                              "layout justify-space-between border-tb-1 pa-2 mb-3"
                          },
                          [
                            _c("div", { staticClass: "align-item-start" }, [
                              _c("span", [_vm._v("Schema:")])
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "align-item-end" }, [
                              _c("ul", { staticClass: "list-inline" }, [
                                _c("li", { staticClass: "pa-0" }, [
                                  _vm._v(
                                    "\n                                    " +
                                      _vm._s(user.schema) +
                                      "\n                                "
                                  )
                                ])
                              ])
                            ])
                          ]
                        )
                      ]),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass: "d-flex height-auto",
                          attrs: { slot: "footer" },
                          slot: "footer"
                        },
                        [
                          _c("v-layout", { attrs: { row: "", wrap: "" } }, [
                            _c("div", { staticClass: "w-50 text-xs-center" }, [
                              _c("p", { staticClass: "mb-0 grey--text fs-12" }),
                              _vm._v(" "),
                              user.created_at
                                ? _c("h4", { staticClass: "fw-bold" })
                                : _vm._e()
                            ]),
                            _vm._v(" "),
                            _c(
                              "div",
                              {
                                staticClass: "w-50 text-xs-center border-left-1"
                              },
                              [
                                _c(
                                  "p",
                                  { staticClass: "mb-0 grey--text fs-12" },
                                  [_vm._v("Criado em")]
                                ),
                                _vm._v(" "),
                                user.created_at
                                  ? _c("h4", { staticClass: "fw-bold" }, [
                                      _vm._v(
                                        _vm._s(
                                          _vm._f("date")(
                                            user.created_at,
                                            "%d/%m/%Y"
                                          )
                                        )
                                      )
                                    ])
                                  : _vm._e()
                              ]
                            )
                          ])
                        ],
                        1
                      )
                    ]
                  )
                }),
                1
              )
            : _vm._e()
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-76d159a0", module.exports)
  }
}

/***/ })

});