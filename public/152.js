webpackJsonp([152],{

/***/ 2328:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(2333)
/* template */
var __vue_template__ = __webpack_require__(2334)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/views/acl/RolesCreate.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-a16f3d76", Component.options)
  } else {
    hotAPI.reload("data-v-a16f3d76", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 2333:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_nprogress__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_nprogress___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_nprogress__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_Helpers_helpers__ = __webpack_require__(43);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      loader: true,
      role: {
        valid: true,
        name: "",
        nameRules: [function (v) {
          return !!v || "Nome é obrigatório";
        }, function (v) {
          return v && v.length <= 100 || "Nome deve ser menor que 100";
        }]
      },
      errors: {},
      links: [{
        id: 3,
        title: 'Listar Perfis',
        icon: 'ti-list mr-3 success--text',
        path: '/acl/roles/list'
      }],
      permissionsList: [],
      selected: []
    };
  },
  mounted: function mounted() {
    this.getPermissions();
  },


  methods: {
    submit: function submit() {
      if (this.$refs.form.validate()) {
        console.log("form submit");
        this.createRole();
      }
    },
    clear: function clear() {
      this.$refs.form.reset();
      this.errors = {};
    },
    getPermissions: function getPermissions() {
      var _this = this;

      __WEBPACK_IMPORTED_MODULE_0_nprogress___default.a.start();
      axios.get("/acl/permission/toRoles").then(function (response) {
        __WEBPACK_IMPORTED_MODULE_0_nprogress___default.a.done();
        _this.loader = false;
        console.log(response.data.data);
        _this.permissionsList = response.data.data;
      }).catch(function (error) {
        __WEBPACK_IMPORTED_MODULE_0_nprogress___default.a.done();
        _this.loader = false;

        _this.$store.dispatch("verifyUserAuth", error.response);
        Vue.notify({
          group: 'loggedIn',
          type: 'error',
          text: 'Erro ao Listar!'
        });
        console.log(error);
      });
    },
    createRole: function createRole() {
      var _this2 = this;

      __WEBPACK_IMPORTED_MODULE_0_nprogress___default.a.start();
      var role = {
        name: this.role.name
      };
      axios.post("/acl/role/store", role).then(function (response) {
        __WEBPACK_IMPORTED_MODULE_0_nprogress___default.a.done();
        Vue.notify({
          group: 'loggedIn',
          type: 'success',
          text: 'Cadastro realizado com Sucesso!'
        });
        _this2.clear();
      }).catch(function (error) {
        __WEBPACK_IMPORTED_MODULE_0_nprogress___default.a.done();
        Vue.notify({
          group: 'loggedIn',
          type: 'error',
          text: 'Erro ao salvar!'
        });

        // Para erros da validção do laravel
        if (error.response.status == 422) {
          _this2.errors = {};
          _this2.errors = error.response.data.errors;
        }
      });
    },
    getMenuLink: function getMenuLink(path) {
      return '/' + Object(__WEBPACK_IMPORTED_MODULE_1_Helpers_helpers__["b" /* getCurrentAppLayout */])(this.$router) + path;
    }
  }
});

/***/ }),

/***/ 2334:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("page-title-bar"),
      _vm._v(" "),
      _c("app-section-loader", { attrs: { status: _vm.loader } }),
      _vm._v(" "),
      _c(
        "v-container",
        { attrs: { fluid: "", "grid-list-xl": "" } },
        [
          _c(
            "v-layout",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    colClasses: "xs12",
                    heading: "",
                    customClasses: "mb-30"
                  }
                },
                [
                  _c(
                    "v-layout",
                    {
                      attrs: {
                        "align-center": "",
                        "justify-end": "",
                        row: "",
                        "fill-height": ""
                      }
                    },
                    [
                      _c(
                        "v-menu",
                        {
                          staticClass: "align-end",
                          attrs: {
                            bottom: "",
                            "offset-y": "",
                            left: "",
                            "content-class": "userblock-dropdown",
                            "nudge-top": "-10",
                            "nudge-right": "0",
                            transition: "slide-y-transition"
                          }
                        },
                        [
                          _c(
                            "v-btn",
                            {
                              staticClass: "ma-0",
                              attrs: { slot: "activator", dark: "", icon: "" },
                              slot: "activator"
                            },
                            [
                              _c("v-icon", { attrs: { color: "grey" } }, [
                                _vm._v("more_vert")
                              ])
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "dropdown-content" },
                            [
                              _c(
                                "v-list",
                                { staticClass: "dropdown-list" },
                                [
                                  _vm._l(_vm.links, function(link) {
                                    return [
                                      _c(
                                        "v-list-tile",
                                        {
                                          key: link.id,
                                          attrs: {
                                            to: _vm.getMenuLink(link.path)
                                          }
                                        },
                                        [
                                          _c("i", { class: link.icon }),
                                          _vm._v(" "),
                                          _c("span", [
                                            _vm._v(_vm._s(link.title))
                                          ])
                                        ]
                                      )
                                    ]
                                  })
                                ],
                                2
                              )
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-form",
                    {
                      ref: "form",
                      attrs: { "lazy-validation": "" },
                      model: {
                        value: _vm.role.valid,
                        callback: function($$v) {
                          _vm.$set(_vm.role, "valid", $$v)
                        },
                        expression: "role.valid"
                      }
                    },
                    [
                      _c(
                        "v-layout",
                        { attrs: { row: "", wrap: "" } },
                        [
                          _c(
                            "v-flex",
                            { attrs: { xs12: "", sm12: "" } },
                            [
                              _c("v-text-field", {
                                attrs: {
                                  label: "Nome do Perfil",
                                  rules: _vm.role.nameRules,
                                  counter: 100,
                                  required: ""
                                },
                                model: {
                                  value: _vm.role.name,
                                  callback: function($$v) {
                                    _vm.$set(_vm.role, "name", $$v)
                                  },
                                  expression: "role.name"
                                }
                              })
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-layout",
            _vm._l(_vm.permissionsList, function(permissionGroup, index) {
              return _c(
                "app-card",
                {
                  key: permissionGroup.name,
                  attrs: {
                    colClasses: "xl4 lg5 md5 sm6 xs12",
                    heading: index,
                    customClasses: "device-share-widget"
                  }
                },
                _vm._l(permissionGroup, function(permission, index) {
                  return _c(
                    "v-layout",
                    { key: index, attrs: { row: "", wrap: "" } },
                    [
                      _c("v-checkbox", {
                        key: index,
                        attrs: {
                          label: permission.class,
                          value: permission.class
                        },
                        model: {
                          value: permission[index],
                          callback: function($$v) {
                            _vm.$set(permission, index, $$v)
                          },
                          expression: "permission[index]"
                        }
                      })
                    ],
                    1
                  )
                }),
                1
              )
            }),
            1
          ),
          _vm._v(" "),
          _c(
            "v-layout",
            {
              staticClass: "fixed-bottom-right mr-4 mb-2",
              attrs: { row: "", wrap: "" }
            },
            [
              _c(
                "v-flex",
                { attrs: { xs6: "" } },
                [
                  _c(
                    "v-btn",
                    {
                      attrs: { disabled: !_vm.role.valid, color: "success" },
                      on: { click: _vm.submit }
                    },
                    [
                      _vm._v(
                        "\n                    " +
                          _vm._s(_vm.$t("message.submit")) +
                          "\n                "
                      )
                    ]
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-flex",
                { attrs: { xs6: "" } },
                [
                  _c(
                    "v-btn",
                    { attrs: { color: "default" }, on: { click: _vm.clear } },
                    [_vm._v(_vm._s(_vm.$t("message.clear")))]
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-a16f3d76", module.exports)
  }
}

/***/ })

});