webpackJsonp([37],{

/***/ 1313:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(1387)
/* template */
var __vue_template__ = __webpack_require__(1388)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/views/acl/RolesEdit.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-dbbba0da", Component.options)
  } else {
    hotAPI.reload("data-v-dbbba0da", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1387:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_nprogress__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_nprogress___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_nprogress__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_Helpers_helpers__ = __webpack_require__(46);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      loader: true,
      alert: true,
      role: {
        valid: true,
        name: "",
        nameRules: [function (v) {
          return !!v || "Nome é obrigatório";
        }, function (v) {
          return v && v.length <= 100 || "Nome deve ser menor que 100";
        }]
      },
      permissions: [],
      errors: {},
      links: [{
        id: 3,
        title: 'Listar Perfis',
        icon: 'ti-list mr-3 success--text',
        path: '/acl/roles/list',
        shield: 'role.index'
      }],
      permissionsList: []
    };
  },
  mounted: function mounted() {
    this.getRole(this.$route.params.roleId);
  },


  methods: {
    submit: function submit() {
      if (this.$refs.form.validate()) {
        console.log("form submit");
        this.updateRole(this.$route.params.roleId);
      }
    },
    clear: function clear() {
      this.$refs.form.reset();
      this.errors = {};
      this.permissions = [];
    },
    formatValue: function formatValue(index, value) {
      return index + '.' + value;
    },
    getRole: function getRole(roleId) {
      var _this = this;

      this.loader = true;
      axios.get("/api/acl/role/edit/" + roleId).then(function (response) {
        _this.loader = false;
        var result = response.data.data,
            role = result.role;

        _this.permissionsList = result.permissions;
        _this.permissions = result.selectedPermissions.map(String); // seta os checkboxes selecionados

        _this.role.name = role.name;
      }).catch(function (error) {
        _this.loader = false;
        Vue.notify({
          group: 'loggedIn',
          type: 'error',
          text: 'Erro ao Buscar Usuário!'
        });
      });
    },
    updateRole: function updateRole(roleId) {
      var _this2 = this;

      var role = {
        name: this.role.name,
        permissions_id: this.permissions
      };
      axios.put("/api/acl/role/update/" + roleId, role).then(function (response) {
        __WEBPACK_IMPORTED_MODULE_0_nprogress___default.a.done();
        Vue.notify({
          group: 'loggedIn',
          type: 'success',
          text: 'Atualizado com Sucesso!'
        });

        _this2.clear();
        _this2.$router.push('/default/acl/roles/list');
      }).catch(function (error) {
        __WEBPACK_IMPORTED_MODULE_0_nprogress___default.a.done();
        Vue.notify({
          group: 'loggedIn',
          type: 'error',
          text: 'Erro ao Atualizar!'
        });

        // Para erros da validção do laravel
        if (error.response.status == 422) {
          _this2.alert = true;
          _this2.errors = {};
          _this2.errors = error.response.data.errors;
        }
      });
    },
    getMenuLink: function getMenuLink(path) {
      return '/' + Object(__WEBPACK_IMPORTED_MODULE_1_Helpers_helpers__["a" /* getCurrentAppLayout */])(this.$router) + path;
    }
  }
});

/***/ }),

/***/ 1388:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("page-title-bar"),
      _vm._v(" "),
      _c("app-section-loader", { attrs: { status: _vm.loader } }),
      _vm._v(" "),
      _c(
        "v-container",
        { attrs: { fluid: "", "grid-list-xl": "" } },
        [
          _c(
            "v-layout",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    colClasses: "xs12",
                    heading: "",
                    customClasses: "mb-30"
                  }
                },
                [
                  _c(
                    "v-layout",
                    {
                      attrs: {
                        "align-center": "",
                        "justify-end": "",
                        row: "",
                        "fill-height": ""
                      }
                    },
                    [
                      _c(
                        "v-menu",
                        {
                          staticClass: "align-end",
                          attrs: {
                            bottom: "",
                            "offset-y": "",
                            left: "",
                            "content-class": "userblock-dropdown",
                            "nudge-top": "-10",
                            "nudge-right": "0",
                            transition: "slide-y-transition"
                          }
                        },
                        [
                          _c(
                            "v-btn",
                            {
                              staticClass: "ma-0",
                              attrs: { slot: "activator", dark: "", icon: "" },
                              slot: "activator"
                            },
                            [
                              _c("v-icon", { attrs: { color: "grey" } }, [
                                _vm._v("more_vert")
                              ])
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "dropdown-content" },
                            [
                              _c(
                                "v-list",
                                { staticClass: "dropdown-list" },
                                [
                                  _vm._l(_vm.links, function(link) {
                                    return [
                                      _c(
                                        "v-list-tile",
                                        {
                                          key: link.id,
                                          attrs: {
                                            to: _vm.getMenuLink(link.path)
                                          }
                                        },
                                        [
                                          _c("i", { class: link.icon }),
                                          _vm._v(" "),
                                          _c("span", [
                                            _vm._v(_vm._s(link.title))
                                          ])
                                        ]
                                      )
                                    ]
                                  })
                                ],
                                2
                              )
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-form",
                    {
                      ref: "form",
                      attrs: { "lazy-validation": "" },
                      model: {
                        value: _vm.role.valid,
                        callback: function($$v) {
                          _vm.$set(_vm.role, "valid", $$v)
                        },
                        expression: "role.valid"
                      }
                    },
                    [
                      _c(
                        "v-layout",
                        { attrs: { row: "", wrap: "" } },
                        [
                          _c(
                            "v-flex",
                            { attrs: { xs12: "", sm12: "" } },
                            [
                              _c("v-text-field", {
                                attrs: {
                                  label: "Nome do Perfil",
                                  rules: _vm.role.nameRules,
                                  counter: 100,
                                  required: ""
                                },
                                model: {
                                  value: _vm.role.name,
                                  callback: function($$v) {
                                    _vm.$set(_vm.role, "name", $$v)
                                  },
                                  expression: "role.name"
                                }
                              }),
                              _vm._v(" "),
                              _vm.errors.name
                                ? _c(
                                    "div",
                                    { staticClass: "v-text-field__details" },
                                    [
                                      _c(
                                        "div",
                                        {
                                          staticClass:
                                            "v-messages theme--light error--text"
                                        },
                                        [
                                          _c(
                                            "div",
                                            {
                                              staticClass: "v-messages__wrapper"
                                            },
                                            [
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "v-messages__message"
                                                },
                                                [
                                                  _vm._v(
                                                    _vm._s(_vm.errors.name[0])
                                                  )
                                                ]
                                              )
                                            ]
                                          )
                                        ]
                                      )
                                    ]
                                  )
                                : _vm._e()
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _vm.errors.permissions_id
            ? _c(
                "v-alert",
                {
                  attrs: {
                    dismissible: "",
                    color: "warning",
                    icon: "priority_high",
                    outline: ""
                  },
                  model: {
                    value: _vm.alert,
                    callback: function($$v) {
                      _vm.alert = $$v
                    },
                    expression: "alert"
                  }
                },
                [
                  _vm._v(
                    "\n            " +
                      _vm._s(_vm.errors.permissions_id[0]) +
                      "\n        "
                  )
                ]
              )
            : _vm._e(),
          _vm._v(" "),
          _c(
            "v-layout",
            { attrs: { row: "", wrap: "" } },
            _vm._l(_vm.permissionsList, function(permissionGroup, indexGroup) {
              return _c(
                "app-card",
                {
                  key: permissionGroup.name,
                  attrs: {
                    colClasses: "md3 sm4 xs12",
                    heading: _vm.$tAcl(
                      "readable_name",
                      _vm.formatValue(indexGroup, "title"),
                      true
                    ),
                    customClasses: "device-share-widget"
                  }
                },
                _vm._l(permissionGroup, function(permission, index) {
                  return _c(
                    "v-layout",
                    { key: index, attrs: { row: "", wrap: "" } },
                    [
                      _c(
                        "v-tooltip",
                        { attrs: { top: "" } },
                        [
                          _c(
                            "v-checkbox",
                            {
                              key: index,
                              attrs: {
                                slot: "activator",
                                label: _vm.$tAcl(
                                  "readable_name",
                                  _vm.formatValue(indexGroup, permission.class),
                                  true
                                ),
                                value: index
                              },
                              slot: "activator",
                              model: {
                                value: _vm.permissions,
                                callback: function($$v) {
                                  _vm.permissions = $$v
                                },
                                expression: "permissions"
                              }
                            },
                            [
                              _vm._v(
                                "\n                            >\n                        "
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c("span", [
                            _vm._v(
                              _vm._s(
                                _vm.is("Desenvolvedor")
                                  ? _vm.formatValue(
                                      indexGroup,
                                      permission.class
                                    )
                                  : "Permissão"
                              )
                            )
                          ])
                        ],
                        1
                      )
                    ],
                    1
                  )
                }),
                1
              )
            }),
            1
          ),
          _vm._v(" "),
          _c(
            "v-layout",
            {
              staticClass: "fixed-bottom-right mr-4 mb-2",
              attrs: { row: "", wrap: "" }
            },
            [
              _c(
                "v-flex",
                { attrs: { xs6: "" } },
                [
                  _c(
                    "v-btn",
                    {
                      attrs: { disabled: !_vm.role.valid, color: "success" },
                      on: { click: _vm.submit }
                    },
                    [
                      _vm._v(
                        "\n                    " +
                          _vm._s(_vm.$t("message.submit")) +
                          "\n                "
                      )
                    ]
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-flex",
                { attrs: { xs6: "" } },
                [
                  _c(
                    "v-btn",
                    { attrs: { color: "default" }, on: { click: _vm.clear } },
                    [_vm._v(_vm._s(_vm.$t("message.clear")))]
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-dbbba0da", module.exports)
  }
}

/***/ })

});