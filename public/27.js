webpackJsonp([27],{

/***/ 1323:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1478)
}
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(1480)
/* template */
var __vue_template__ = __webpack_require__(1481)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-34145c1e"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/views/deskfull/Accounts.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-34145c1e", Component.options)
  } else {
    hotAPI.reload("data-v-34145c1e", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1339:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1340)
}
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(1342)
/* template */
var __vue_template__ = __webpack_require__(1343)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-75773638"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/components/AppCard/AccountCard.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-75773638", Component.options)
  } else {
    hotAPI.reload("data-v-75773638", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1340:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1341);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("567d49ea", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-75773638\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./AccountCard.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-75773638\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./AccountCard.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1341:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)(false);
// imports


// module
exports.push([module.i, "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", ""]);

// exports


/***/ }),

/***/ 1342:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__AppCardHeading__ = __webpack_require__(447);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__AppCardHeading___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__AppCardHeading__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
    name: "AccountCard",
    components: { AppCardHeading: __WEBPACK_IMPORTED_MODULE_0__AppCardHeading___default.a },
    props: ['account', 'id'],
    data: function data() {
        return {};
    }
});

/***/ }),

/***/ 1343:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "app-card",
    { attrs: { heading: this.account.name, customClasses: "mb-30" } },
    [
      _c(
        "v-chip",
        { attrs: { color: "error", "text-color": "white" } },
        [
          _c("v-avatar", [_c("v-icon", [_vm._v("ti-google")])], 1),
          _vm._v("\n        " + _vm._s(this.account.socialnetwork) + "\n    ")
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "v-layout",
        { attrs: { row: "", wrap: "" } },
        [
          _c("v-flex", { attrs: { xs6: "", sm4: "" } }, [
            _c("span", { staticClass: "small pt-4 d-block" }, [
              _vm._v(" " + _vm._s(this.account.email))
            ])
          ]),
          _vm._v(" "),
          _c("v-flex", { attrs: { xs6: "", sm4: "" } }, [
            _c("span", { staticClass: "small pt-4 d-block" }, [
              _vm._v(" " + _vm._s(this.account.refresh_token))
            ])
          ]),
          _vm._v(" "),
          _c(
            "v-layout",
            { attrs: { row: "", wrap: "", "icon-box": "" } },
            [
              _c(
                "v-flex",
                { attrs: { xs12: "", sm6: "", md4: "", lg2: "" } },
                [
                  _c(
                    "v-btn",
                    {
                      attrs: { color: "error", fab: "", small: "", dark: "" },
                      on: {
                        click: function($event) {
                          _vm.$emit("deleteAccount", _vm.id)
                        }
                      }
                    },
                    [_c("v-icon", [_vm._v("ti-trash")])],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-75773638", module.exports)
  }
}

/***/ }),

/***/ 1478:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1479);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("4e5f6ba9", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-34145c1e\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./Accounts.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-34145c1e\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./Accounts.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1479:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)(false);
// imports


// module
exports.push([module.i, "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", ""]);

// exports


/***/ }),

/***/ 1480:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_Helpers_helpers__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_Components_AppCard_AccountCard__ = __webpack_require__(1339);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_Components_AppCard_AccountCard___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_Components_AppCard_AccountCard__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_axios__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_axios___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_axios__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
    components: { AccountCard: __WEBPACK_IMPORTED_MODULE_1_Components_AppCard_AccountCard___default.a },
    data: function data() {
        return {
            managerAccount: {},
            addNewAccountDialog: false,
            selectDeletedAccount: null,
            selectDeletedCustomer: null,
            showCustomersCard: false,
            loader: true,
            loaderCustomers: false,
            loadingGoogle: false,
            headers: [{ text: "Tipo conta", sortable: false }, { text: "Nome", sortable: false }, { text: "E-mail", sortable: false }, { text: "Sincronizar", sortable: false }, { text: "Contas", sortable: false }, { text: "Remover conta", sortable: false }],
            headersCustomers: [{ text: "Nome", sortable: false }, { text: "Id", sortable: false }, { text: "Gerencia clientes", sortable: false }, { text: "Conta teste", sortable: false }, { text: "Convidar", sortable: false }, { text: "Cancelar vínculo", sortable: false }, { text: "Remover ", sortable: false }],
            logoGoogle: {
                url: "/static/images-conecta/accounts-logo-google-ad_words.png",
                alt: "Adwords"
            },
            accounts: [],
            customers: []

        };
    },

    methods: {
        sync: function sync(account) {
            var _this = this;

            this.loaderCustomers = true;
            this.showCustomersCard = true;
            this.customers = [];
            __WEBPACK_IMPORTED_MODULE_2_axios___default.a.get('/api/accounts/' + account.id + '/sync').then(function (response) {
                _this.customers = response.data;
                _this.loaderCustomers = false;
                _this.showCustomersCard = true;
            }).catch(function (error) {
                return console.log(error);
            });
        },
        getSocialNetworkIcon: function getSocialNetworkIcon(type) {
            var icon = '';
            if (type === 'Adwords') {
                icon = this.logoGoogle;
            }
            return icon;
        },
        googleOauth: function googleOauth() {
            var _this2 = this;

            // window.location="/oauth2";
            this.loadingGoogle = true;
            __WEBPACK_IMPORTED_MODULE_2_axios___default.a.get('/oauth2').then(function (response) {
                window.location = response.data;
                _this2.loadingGoogle = false;
            }).catch(function (error) {
                _this2.loadingGoogle = false;
                console.error(error);
            });
        },
        getManagerAccount: function getManagerAccount() {
            var _this3 = this;

            __WEBPACK_IMPORTED_MODULE_2_axios___default.a.get('/api/accounts/manager').then(function (response) {
                if (response.data.success) {
                    _this3.managerAccount = response.data.result;
                    if (_this3.managerAccount.firstTime === true) {}
                }
            }).catch(function (error) {
                console.error(error.message);
            });
        },
        getPendingInvitations: function getPendingInvitations() {
            var _this4 = this;

            __WEBPACK_IMPORTED_MODULE_2_axios___default.a.get('/api/accounts/invitations').then(function (response) {
                if (response.data.success) {
                    _this4.managerAccount = response.data.result;
                    if (_this4.managerAccount.firstTime === true) {}
                }
            }).catch(function (error) {
                console.error(error.message);
            });
        },
        getAccounts: function getAccounts() {
            var _this5 = this;

            __WEBPACK_IMPORTED_MODULE_2_axios___default.a.get('/api/accounts').then(function (response) {
                _this5.accounts = response.data;
                _this5.accounts.map(function (account) {
                    account.icon = _this5.getSocialNetworkIcon(account.socialnetwork);
                });
                _this5.loader = false;
            }).catch(function (error) {
                return console.log(error);
            });
        },
        getCustomers: function getCustomers(account) {
            var _this6 = this;

            this.loaderCustomers = true;
            __WEBPACK_IMPORTED_MODULE_2_axios___default.a.get('/api/accounts/' + account + '/customers').then(function (response) {
                _this6.customers = response.data;
                _this6.loaderCustomers = false;
                _this6.showCustomersCard = true;
            }).catch(function (error) {
                return console.log(error);
            });
        },
        deleteCustomer: function deleteCustomer(customer) {
            this.$refs.customerDeleteConfirmationDialog.openDialog();
            this.selectDeletedCustomer = customer;
        },
        sendInvitation: function sendInvitation(customer) {
            var _this7 = this;

            this.loaderCustomers = true;
            __WEBPACK_IMPORTED_MODULE_2_axios___default.a.get('/api/customers/' + customer.id + '/invite').then(function (response) {
                _this7.loaderCustomers = false;
                if (response.data.success === true) {
                    _this7.showMessage('success', 'Convite enviado com sucesso');
                } else {
                    _this7.showMessage('error', response.data.message);
                }
            }).catch(function (error) {
                _this7.loaderCustomers = false;
                var message = 'Ocorreu um erro ao enviar o convite';
                if (error.response.data.message) {
                    message = error.response.data.message[0];
                }
                _this7.showMessage('error', message);
            });
        },
        inactivateLink: function inactivateLink(customer) {
            var _this8 = this;

            this.loaderCustomers = true;
            __WEBPACK_IMPORTED_MODULE_2_axios___default.a.get('/api/customers/' + customer.id + '/inactivate').then(function (response) {
                _this8.loaderCustomers = false;
                if (response.data.success === true) {
                    _this8.showMessage('success', 'Vínculo excluído com sucesso.');
                } else {
                    _this8.showMessage('error', response.data.message);
                }
            }).catch(function (error) {
                _this8.loaderCustomers = false;
                var message = 'Ocorreu um erro ao desvincular a conta.';
                if (error.response.data.message) {
                    message = error.response.data.message[0];
                }
                _this8.showMessage('error', message);
            });
        },
        onDeleteCustomer: function onDeleteCustomer() {
            var _this9 = this;

            this.$refs.customerDeleteConfirmationDialog.close();
            this.loaderCustomers = true;
            __WEBPACK_IMPORTED_MODULE_2_axios___default.a.delete('/api/customers/' + this.selectDeletedCustomer.id).then(function (response) {
                if (response.data == true) {
                    _this9.$store.dispatch("onDeleteCustomer", _this9.selectDeletedCustomer);
                    _this9.getCustomers(_this9.selectDeletedCustomer.account_id);
                } else {
                    alert('Erro ao excluir conta');
                }
            }).catch(function (error) {
                return console.log(error);
            });
        },
        showCustomers: function showCustomers(account) {
            this.getCustomers(account.id);
        },
        deleteAccount: function deleteAccount(account) {
            this.$refs.deleteConfirmationDialog.openDialog();
            this.selectDeletedAccount = account;
        },

        // delete card
        onDeleteAccount: function onDeleteAccount() {
            var _this10 = this;

            this.$refs.deleteConfirmationDialog.close();
            this.loader = true;
            this.showCustomersCard = false;
            __WEBPACK_IMPORTED_MODULE_2_axios___default.a.delete('/api/accounts/' + this.selectDeletedAccount.id).then(function (response) {
                if (response.data == true) {
                    _this10.$store.dispatch("onDeleteAccount", _this10.selectDeletedAccount);
                    _this10.getAccounts();
                } else {
                    alert('Erro ao excluir conta');
                }
            }).catch(function (error) {
                return console.log(error);
            });
        },
        handleErrors: function handleErrors() {
            // verifica se houve erro na autenticacao Oauth
            if (this.$route.query.error) {
                var error = this.$route.query.error;
                var mensagem = this.$t('message.oauth.' + error);
                //verifica se a mensagem nao foi encontrada no arquivo de internacionalizacao
                if (mensagem === 'message.oauth.' + error) {
                    //se nao foi, utiliza uma mensagem padrao
                    mensagem = this.$t('message.oauth.OTHERS');
                }
                this.$router.push('deskfull-accounts', function () {
                    Vue.notify({
                        group: 'loggedIn',
                        type: 'error',
                        text: mensagem
                    });
                });
            }
        },
        showMessage: function showMessage(type, message) {
            Vue.notify({
                group: 'loggedIn',
                type: type,
                text: message
            });
        }
    },

    mounted: function mounted() {
        this.handleErrors();
        // this.getManagerAccount();
        this.getAccounts();
    },


    name: "VincularConta"
});

/***/ }),

/***/ 1481:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-container",
    { attrs: { fluid: "" } },
    [
      _c("page-title-bar"),
      _vm._v(" "),
      _c(
        "v-card",
        [
          _c(
            "v-card-actions",
            [
              _c(
                "v-btn",
                {
                  attrs: { color: "primary" },
                  on: {
                    click: function($event) {
                      _vm.addNewAccountDialog = true
                    }
                  }
                },
                [
                  _vm._v(
                    "\n                " +
                      _vm._s(_vm.$t("message.addNewConta")) +
                      "\n            "
                  )
                ]
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-card-text",
            [
              _c("app-section-loader", { attrs: { status: _vm.loader } }),
              _vm._v(" "),
              _c("v-data-table", {
                staticClass: "elevation-1",
                attrs: { headers: _vm.headers, items: _vm.accounts },
                scopedSlots: _vm._u([
                  {
                    key: "items",
                    fn: function(props) {
                      return [
                        _c(
                          "td",
                          [
                            _c("v-avatar", [
                              _c("img", {
                                staticClass: "img-responsive",
                                attrs: {
                                  src: props.item.icon.url,
                                  alt: props.item.icon.alt
                                }
                              })
                            ])
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(props.item.name) + " ")]),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(props.item.email))]),
                        _vm._v(" "),
                        _c(
                          "td",
                          [
                            _c(
                              "v-btn",
                              {
                                attrs: { flat: "", icon: "", color: "cyan" },
                                on: {
                                  click: function($event) {
                                    _vm.sync(props.item)
                                  }
                                }
                              },
                              [_c("v-icon", { staticClass: "ti-reload" })],
                              1
                            )
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "td",
                          [
                            _c(
                              "v-btn",
                              {
                                attrs: { flat: "", icon: "", color: "cyan" },
                                on: {
                                  click: function($event) {
                                    _vm.showCustomers(props.item)
                                  }
                                }
                              },
                              [
                                _c("v-icon", {
                                  staticClass: "zmdi zmdi-accounts"
                                })
                              ],
                              1
                            )
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "td",
                          [
                            _c(
                              "v-btn",
                              {
                                attrs: { flat: "", icon: "", color: "error" },
                                on: {
                                  click: function($event) {
                                    _vm.deleteAccount(props.item)
                                  }
                                }
                              },
                              [
                                _c("v-icon", {
                                  staticClass: "zmdi zmdi-delete"
                                })
                              ],
                              1
                            )
                          ],
                          1
                        )
                      ]
                    }
                  }
                ])
              }),
              _vm._v(" "),
              _c("delete-confirmation-dialog", {
                ref: "deleteConfirmationDialog",
                attrs: {
                  heading: "Tem certeza que deseja excluir?",
                  message: "Tem certeza que deseja excluir essa conta?"
                },
                on: { onConfirm: _vm.onDeleteAccount }
              })
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c("v-spacer"),
      _vm._v(" "),
      _c(
        "v-card",
        {
          directives: [
            {
              name: "show",
              rawName: "v-show",
              value: _vm.showCustomersCard,
              expression: "showCustomersCard"
            }
          ]
        },
        [
          _c(
            "v-card-text",
            [
              _c("app-section-loader", {
                attrs: { status: _vm.loaderCustomers }
              }),
              _vm._v(" "),
              _c("v-data-table", {
                staticClass: "elevation-1",
                attrs: {
                  headers: _vm.headersCustomers,
                  items: _vm.customers,
                  "hide-actions": ""
                },
                scopedSlots: _vm._u([
                  {
                    key: "items",
                    fn: function(props) {
                      return [
                        _c("td", [
                          _vm._v(_vm._s(props.item.descriptiveName) + " ")
                        ]),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(props.item.customerId))]),
                        _vm._v(" "),
                        _c("td", [
                          _vm._v(
                            _vm._s(props.item.canManageClients ? "Sim" : "Não")
                          )
                        ]),
                        _vm._v(" "),
                        _c("td", [
                          _vm._v(_vm._s(props.item.testAccount ? "Sim" : "Não"))
                        ]),
                        _vm._v(" "),
                        _c(
                          "td",
                          [
                            _c(
                              "v-btn",
                              {
                                attrs: { flat: "", icon: "", color: "blue" },
                                on: {
                                  click: function($event) {
                                    _vm.sendInvitation(props.item)
                                  }
                                }
                              },
                              [_c("v-icon", [_vm._v("link")])],
                              1
                            )
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "td",
                          [
                            _c(
                              "v-btn",
                              {
                                attrs: { flat: "", icon: "", color: "error" },
                                on: {
                                  click: function($event) {
                                    _vm.inactivateLink(props.item)
                                  }
                                }
                              },
                              [_c("v-icon", [_vm._v("link_off")])],
                              1
                            )
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "td",
                          [
                            _c(
                              "v-btn",
                              {
                                attrs: { flat: "", icon: "", color: "error" },
                                on: {
                                  click: function($event) {
                                    _vm.deleteCustomer(props.item)
                                  }
                                }
                              },
                              [_c("v-icon", [_vm._v("delete")])],
                              1
                            )
                          ],
                          1
                        )
                      ]
                    }
                  }
                ])
              }),
              _vm._v(" "),
              _c("delete-confirmation-dialog", {
                ref: "customerDeleteConfirmationDialog",
                attrs: {
                  heading: "Tem certeza que deseja excluir?",
                  message: "Tem certeza que deseja excluir essa conta?"
                },
                on: { onConfirm: _vm.onDeleteCustomer }
              })
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "v-dialog",
        {
          attrs: { "max-width": "500" },
          model: {
            value: _vm.addNewAccountDialog,
            callback: function($$v) {
              _vm.addNewAccountDialog = $$v
            },
            expression: "addNewAccountDialog"
          }
        },
        [
          _c(
            "v-card",
            [
              _c("v-card-title", [
                _c("span", { staticClass: "headline" }, [
                  _vm._v(_vm._s(_vm.$t("message.addNewConta")))
                ])
              ]),
              _vm._v(" "),
              _c("v-spacer"),
              _vm._v(" "),
              _c("v-responsive", [
                _c(
                  "div",
                  { staticClass: "mb-4", attrs: { align: "center" } },
                  [
                    _c(
                      "v-btn",
                      {
                        attrs: {
                          color: "blue lighten-5",
                          fab: "",
                          large: "",
                          dark: "",
                          loading: _vm.loadingGoogle
                        },
                        on: { click: _vm.googleOauth }
                      },
                      [
                        _c("v-avatar", [
                          _c("img", {
                            staticClass: "img-responsive",
                            attrs: {
                              src: _vm.logoGoogle.url,
                              alt: _vm.logoGoogle.alt
                            }
                          })
                        ])
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "v-btn",
                      {
                        attrs: {
                          disabled: "",
                          color: "primary",
                          fab: "",
                          large: "",
                          dark: ""
                        }
                      },
                      [_c("v-icon", [_vm._v("ti-instagram")])],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "v-btn",
                      {
                        attrs: {
                          disabled: "",
                          color: "warning",
                          fab: "",
                          large: "",
                          dark: ""
                        }
                      },
                      [_c("v-icon", [_vm._v("ti-facebook")])],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "v-btn",
                      {
                        attrs: {
                          disabled: "",
                          color: "orange",
                          fab: "",
                          large: "",
                          dark: ""
                        }
                      },
                      [_c("v-icon", [_vm._v("ti-bar-chart")])],
                      1
                    )
                  ],
                  1
                )
              ])
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-34145c1e", module.exports)
  }
}

/***/ })

});