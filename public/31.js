webpackJsonp([31],{

/***/ 1320:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1409)
}
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(1411)
/* template */
var __vue_template__ = __webpack_require__(1412)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-974333fe"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/views/deskfull/ReportsClients.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-974333fe", Component.options)
  } else {
    hotAPI.reload("data-v-974333fe", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1409:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1410);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("0d856442", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-974333fe\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./ReportsClients.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-974333fe\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./ReportsClients.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1410:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)(false);
// imports


// module
exports.push([module.i, "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", ""]);

// exports


/***/ }),

/***/ 1411:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_axios__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_axios___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_axios__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
    name: "DeskFullClients",
    data: function data() {
        return {
            accounts: [],
            clients: {},
            customers: [],
            loader: false
        };
    },

    methods: {
        getClients: function getClients() {
            var _this = this;

            this.loader = true;
            __WEBPACK_IMPORTED_MODULE_0_axios___default.a.get('/api/deskfull-clients').then(function (response) {
                _this.clients = response.data;
                _this.loader = false;
            }).catch(function (error) {

                //if (err.response.status === 401) {
                //  Vue.auth.destroyToken();
                //  router.push('/session/login')
                //}else{
                Vue.notify({
                    group: 'loggedIn',
                    type: 'error',
                    text: 'Não foi possível buscar os clientes'
                });

                //}
                _this.loader = false;
            });
        }
    },

    mounted: function mounted() {
        this.getClients();
    }
});

/***/ }),

/***/ 1412:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("page-title-bar"),
      _vm._v(" "),
      _c("app-section-loader", { attrs: { status: _vm.loader } }),
      _vm._v(" "),
      _c(
        "v-container",
        { attrs: { fluid: "", "grid-list-xl": "" } },
        [
          _c(
            "v-layout",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    colClasses: "xs12",
                    heading: "Clientes",
                    customClasses: "mb-30"
                  }
                },
                [
                  _vm.clients.length
                    ? _c("span", { staticClass: "grey--text" }, [
                        _vm._v(
                          "Selecione um cliente para visualizar seus relatórios"
                        )
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  !_vm.clients.length
                    ? _c(
                        "div",
                        [
                          _c("span", { staticClass: "grey--text" }, [
                            _vm._v("Você ainda não cadastrou nenhum cliente. ")
                          ]),
                          _vm._v(" "),
                          _c("v-btn", { attrs: { to: "deskfull-client" } }, [
                            _vm._v("Cadastrar")
                          ])
                        ],
                        1
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _vm._l(_vm.clients, function(client) {
                    return _c(
                      "v-card-text",
                      { key: client.id },
                      [
                        _c(
                          "v-layout",
                          { attrs: { row: "", wrap: "" } },
                          [
                            _c(
                              "v-flex",
                              { attrs: { xs6: "", sm2: "" } },
                              [
                                _c("v-img", {
                                  attrs: {
                                    src: "/static/img/blog-6.jpg",
                                    height: "40px"
                                  }
                                })
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c("v-flex", { attrs: { xs6: "", sm2: "" } }, [
                              _c(
                                "span",
                                {
                                  staticClass:
                                    "pt-4 d-block text-danger fw-bold"
                                },
                                [_vm._v(" " + _vm._s(client.nome))]
                              )
                            ]),
                            _vm._v(" "),
                            _c("v-flex", { attrs: { xs6: "", sm4: "" } }, [
                              _c("span", { staticClass: " pt-4 d-block" }, [
                                _vm._v(" " + _vm._s(client.email))
                              ])
                            ]),
                            _vm._v(" "),
                            _c(
                              "v-layout",
                              {
                                attrs: {
                                  row: "",
                                  wrap: "",
                                  "icon-box": "",
                                  sm2: ""
                                }
                              },
                              [
                                _c(
                                  "v-flex",
                                  {
                                    attrs: {
                                      xs12: "",
                                      sm6: "",
                                      md4: "",
                                      lg2: ""
                                    }
                                  },
                                  [
                                    _c(
                                      "v-btn",
                                      {
                                        attrs: {
                                          to:
                                            "deskfull-reports?client=" +
                                            client.user_id +
                                            "&deskfull=" +
                                            client.id,
                                          color: "success",
                                          fab: "",
                                          dark: ""
                                        }
                                      },
                                      [
                                        _c("v-icon", { attrs: { dark: "" } }, [
                                          _vm._v(
                                            "\n                                        ti-bar-chart\n                                    "
                                          )
                                        ])
                                      ],
                                      1
                                    )
                                  ],
                                  1
                                )
                              ],
                              1
                            )
                          ],
                          1
                        )
                      ],
                      1
                    )
                  })
                ],
                2
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-974333fe", module.exports)
  }
}

/***/ })

});