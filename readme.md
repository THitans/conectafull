# ConectaFull

- Tema baseado no Vuetify: https://vuetifyjs.com/pt-BR/components/buttons
- Doc do Tema: https://iron-network.gitbook.io/vuely/
- Thema demo: http://vuely.theironnetwork.org/default/dashboard/ecommerce

## Instalação

É necessário ter o postgres instalado em sua máquina:
 
- `Node v9.11.2`
- `Npm v5.6.0`
- `PHP7.1`
- `Postgres 10.6`

Rode na raiz do projeto

- `cp .env.example .env`
- `composer install`
- `npm install`
- `sudo chmod -R 777 storage/`
- `composer dumpautoload`
- `php artisan migrate --seed`

Watch assets

- `npm run watch`

Serve php

- `php artisan serve`

#### Pacotes extras

##### Production
    - Valida senhas fracas: https://github.com/unicodeveloper/laravel-password/tree/1.0.3 ! Não está instalado
    - Salva no banco todos os email enviado: https://github.com/shvetsgroup/laravel-email-database-log ! Não está instalado
    - SoftDeleteCascade Usando o deleted_at: https://github.com/Askedio/laravel-soft-cascade/tree/5.7.0
    - Lê aqruivos de LOG de storage/logs: https://github.com/ARCANEDEV/LogViewer/tree/4.6.3

##### Local
    - Lista as rotas em /routes: https://github.com/garygreen/pretty-routes
    - Gera Model a Partir do banco de Dados: https://github.com/krlove/eloquent-model-generator/tree/v1.2.7
    - DebugBar: https://github.com/barryvdh/laravel-debugbar/tree/v3.2.1


##### JS
    - Ícones: http://zavoloklom.github.io/material-design-iconic-font/v1/
    - SweetAlert - https://sweetalert.js.org/guides/

### Tentant

- Criado o Middleware `SchemaSwitch`

*Para rotas que serão utilizadas pelo cliente*, adicione o middleware como no exemplo abaixo que está em: `modules/ACL/routes/acl.php`

````
    ...
    $this->group(['prefix' => '/user', 'middleware' => 'schema.switch'], function () {
    ...
````

### ACL

Controle de Acesso de Usuário

- Ao Criar uma nova rota utilize o padrão abaixo para o PHP
- Se tiver prefixo adicione `'prefix' => 'acl'`. caso contrário só remover.
- Pra verificar se sua rota está OK `php artisan route:list`.

````php
$this->group([
    'prefix' => 'acl',
    'middleware' => ['auth:api', 'needsPermission', 'needsRole']],
    function () {
    ...
        $this->get('/create', [
            'uses' => 'UserController@create',
            'as' => 'user.create',
            'shield' => 'user.create'
        ]);
    ...
````

##### Para o Vue em: `resources/js/router/default.js`

*ATENÇÃO o valor do parametro `shield` deve ser o mesmo do `shield` da rota do PHP:*

````javascript
  {
    path: '/default/users/new',
    component: UsersCreate,
    meta: {
      requiresAuth: true,
      title: 'message.usersNewTitle',
      breadcrumb: 'Usuários / Novo',
      shield: 'user.create'
    }
  },
````

Em `resources/js/store/modules/sidebar/data.js` NÃO coloque título repetidos

````javascript
    {
      action: 'zmdi-globe',
      title: 'message.config',
      active: false,
      items: [
        {title: 'message.users', path: '/users/list', shield: 'user.index'},
        {title: 'message.acl.roles', path: '/acl/roles/list', shield: 'role.index'},
        {title: 'message.acl.permissions', path: '/acl/permissions/list', is: 'Desenvolvedor'},
      ]
    }
````

Após criada a Rota no PHP e no VUE:

- A) Em `resources/js/lang/pt_BR/index.js` adicione em `acl.readable_name` em acordo com o shield de suas rotas:

  *Abaixo exemplo para o controller: `modules/ACL/app/Http/Controllers/UserController.php`*

````javascript
    user: {
        title: 'Usuários',
        index: 'Listar Usuários',
        create: 'Criar Usuáriol',
        edit: 'Editar Usuárioll',
        destroy: 'Excluir Usuáriol',
    }
````

Para Inserir no Banco de Dados as permissões que vc criou use o seguinte comando:

`php artisan louzada:create:permission --force`

Há uma sequencia de comando preparadas na pasta `bin/`

`bash bin/reset` apaga todos os registros e insere os usuários padrão e as permissions padrão.

# resources/js/store/modules/sidebar/data.js

### Email

Configure no .env

```
APP_NAME=Conecta
...
APP_URL=http://localhost:8000
...

MAIL_DRIVER=smtp
MAIL_HOST=smtp.gmail.com
MAIL_PORT=587
MAIL_USERNAME=EMAIL
MAIL_PASSWORD=SENHA
MAIL_ENCRYPTION=tls
MAIL_FROM_NAME='Conecta'
MAIL_FROM_ADDRESS=nao.responda@conecta.com.br
```

*Já existe no model de User 2 métodos de envio de email específico*
### CRIE O SEU ALEX em `modules/Mailer/app/Notification`

````php
    /**
     * Send the password reset notification.
     *
     * @param  string $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    /**
     * Utilizado pelo module de Mailer
     *
     * @param $token
     */
    public function sendConfirmMailNotification($token)
    {
        $this->notify(new ConfirmationMailNotification($token));
    }
````

Registre a sua classe de envio de email em: `modules/Mailer/app/MailerServiceProvider.php` no método `register()`


### Deployer

*Faça o deploy com o user `deployer`*

`su - deployer`

*aS INFORMAÇÕES de Banco e senha estão no google drive em `https://docs.google.com/document/d/1L0nS8HIZWjubtqCfxXwm8PnmCVVPxOnOMzcb6sqpUTA/edit`

Finalizar configuração deployer
https://www.digitalocean.com/community/tutorials/automatically-deploy-laravel-applications-deployer-ubuntu

- Comando Básicos

+++
Para cria usuário pgsql:
 - sudo -i -u postgres
 - createuser --interactive

Backup do banco
pg_dump -d regulacao -U postgres -O -x > /tmp/regulacao.sql


 - GRANT ALL PRIVILEGES ON DATABASE seu_banco TO seu_usuário; # Define privilegios de user ao banco
 - ALTER USER user_name WITH PASSWORD 'new_password'; # altera pass de user
+++

\du # Lista usuários postgres
\l # Lista databases
\c nome_banco # Conecta no DB

#### Equipe de Desenvolvimento

- [Alex Sandro](https://www.linkedin.com/in/horecio/)
- [Horecio Araújo Dias](https://www.linkedin.com/in/horecio/)
- [Igor](https://www.linkedin.com/in/igor-martins-de-souza-58435b107/)