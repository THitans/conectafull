<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => env('SES_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => ACL\Model\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'adwords' => [
        'client_id' => env('ADWORDS_AUTH_CLIENT_ID'),
        'secret_key' => env('ADWORDS_AUTH_CLIENT_SECRET'),
        'client_customer_id' => env('ADWORDS_CLIENT_CUSTOMER_ID'),
        'refresh_token' => env('ADWORDS_REFRESH_TOKEN'),
        'manager_name' => env('ADWORDS_MANAGER_NAME'),
        'manager_email' => env('ADWORDS_MANAGER_EMAIL'),
    ]
];
