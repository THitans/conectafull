<?php

$production = [

    /*
     * Laravel Framework Service Providers...
     */
    Illuminate\Auth\AuthServiceProvider::class,
    Illuminate\Broadcasting\BroadcastServiceProvider::class,
    Illuminate\Bus\BusServiceProvider::class,
    Illuminate\Cache\CacheServiceProvider::class,
    Illuminate\Foundation\Providers\ConsoleSupportServiceProvider::class,
    Illuminate\Cookie\CookieServiceProvider::class,
    Illuminate\Database\DatabaseServiceProvider::class,
    Illuminate\Encryption\EncryptionServiceProvider::class,
    Illuminate\Filesystem\FilesystemServiceProvider::class,
    Illuminate\Foundation\Providers\FoundationServiceProvider::class,
    Illuminate\Hashing\HashServiceProvider::class,
    Illuminate\Mail\MailServiceProvider::class,
    Illuminate\Notifications\NotificationServiceProvider::class,
    Illuminate\Pagination\PaginationServiceProvider::class,
    Illuminate\Pipeline\PipelineServiceProvider::class,
    Illuminate\Queue\QueueServiceProvider::class,
    Illuminate\Redis\RedisServiceProvider::class,
    Illuminate\Auth\Passwords\PasswordResetServiceProvider::class,
    Illuminate\Session\SessionServiceProvider::class,
    Illuminate\Translation\TranslationServiceProvider::class,
    Illuminate\Validation\ValidationServiceProvider::class,
    Illuminate\View\ViewServiceProvider::class,

    /*
     * Package Service Providers...
     */

    /*
     * Application Service Providers...
     */
    App\Providers\AppServiceProvider::class,
    App\Providers\AuthServiceProvider::class,
    // App\Providers\BroadcastServiceProvider::class,
    App\Providers\EventServiceProvider::class,
    App\Providers\RouteServiceProvider::class,
    // https://github.com/Askedio/laravel-soft-cascade/tree/5.7.0
    Askedio\SoftCascade\Providers\GenericServiceProvider::class,
    // https://github.com/ARCANEDEV/LogViewer/tree/4.6.3
    Arcanedev\LogViewer\LogViewerServiceProvider::class,
    // https://github.com/pacuna/Laravel-PGSchema
    \Pacuna\Schemas\SchemasServiceProvider::class,
    //https://jwt-auth.readthedocs.io/en/develop/laravel-installation/
    Tymon\JWTAuth\Providers\LaravelServiceProvider::class,
    // http://image.intervention.io/getting_started/installation
    Intervention\Image\ImageServiceProvider::class,

    /**
     * Modules
     */
     \ACL\ACLServiceProvider::class,
     \Audit\AuditServiceProvider::class,
     \DeskFull\DeskFullServiceProvider::class,
     \Mailer\MailerServiceProvider::class,
];

$local = [
    /**
     * https://github.com/barryvdh/laravel-debugbar
     */
    Barryvdh\Debugbar\ServiceProvider::class,
    Krlove\EloquentModelGenerator\Provider\GeneratorServiceProvider::class,
    PrettyRoutes\ServiceProvider::class,
];

$packages = $production;

if (env('APP_ENV') == 'local') {
    $packages = array_merge($production, $local);
}

return $packages;
