<?php

namespace App\Console\Commands;

use App\Services\GetRefreshToken;
use Illuminate\Console\Command;

class adwordsToken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'adwords:token';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates adwords refresh token';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        GetRefreshToken::main();
    }
}
