<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class DBCreateDataBase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:dataBase {database : DataBase name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cria a Banco de dados ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {

            \DB::beginTransaction();

            \DB::statement('DROP DATABASE '.$this->argument('database'));
            $retorno = \DB::statement('CREATE DATABASE :schema', array('schema' => $this->argument('database')));
            \DB::commit();
            return $retorno;
        } catch (\Exception $exception) {

            \DB::rollBack();
            throw  new \Exception($exception->getMessage());
        }

    }
}
