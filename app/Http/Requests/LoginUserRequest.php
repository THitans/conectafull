<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginUserRequest extends FormRequest
{
    /**
     * LoginUserRequest constructor.
     */
    public function __construct()
    {
        $fields = ['email', 'password'];

        /**
         * Se não passar todos os valores exigidos no post lança Exceção
         */
        verify_fields_request($fields, app('request')->all());
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|string|max:255',
            'password' => 'required|string|min:6|max:255',
        ];
    }
}
