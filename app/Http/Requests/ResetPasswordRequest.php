<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ResetPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|dumbpwd|min:8',
        ];
    }

    public function messages()
    {
        return [
            'password.dumbpwd' => 'Atenção! Essa senha é muito comum. Por favor, crie uma mais segura!'
        ];
    }
}
