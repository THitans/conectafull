<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Requests\LoginUserRequest;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends APIController
{
    use AuthenticatesUsers;

    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'logout']]);
    }

    public function login(LoginUserRequest $request)
    {
        if ($lockedOut = $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($token = auth('api')->attempt($request->only('email', 'password'))) {
            return $this->sendLoginResponse($request, $token);
        }

        if (!$lockedOut) {
            $this->incrementLoginAttempts($request);
        }

        return $this->sendFailedLoginResponse($request);
    }


    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return \Auth::guard('api');
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    protected function sendLoginResponse(Request $request, $token)
    {
        $this->clearLoginAttempts($request);
//        $this->setApiKey();

        return $this->respond([
            'data' => [
                'user' => auth('api')->user(),
                'acl' => [
                    'superUser' => auth('api')->user()->isSuperUser(),
                    'suporte' => auth('api')->user()->isSuporte(),
                    'permissions' => auth('api')->user()->getAllPermissions()->pluck('name'),
                ],
                'jwt' => [
                    'access_token' => $token,
                    'token_type' => 'bearer',
//                    'expires_in' => env('JWT_TTL') * 60
                ]
            ]
        ]);
    }

    /**
     * Get the failed login response instance.
     * @overide
     */
    protected function sendFailedLoginResponse()
    {
        return $this->respondUnathourize();
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        $user           = auth('api')->user();
        $roles  = app('acl.model.role')->findToSelect($user->id)->pluck('name');

        return $this->respond([
            'data' => compact('user', 'roles')
        ]);
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        auth()->logout();

        return $this->respond([
            'message' => 'Logout realizado com sucesso!'
        ]);
    }
}
