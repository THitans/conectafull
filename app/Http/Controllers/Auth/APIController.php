<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

class APIController extends Controller
{
    protected function respondNotFound($message = 'Página não encontrada')
    {
        return $this->respondWithError($message, 404);
    }

    protected function respondInternalError($message = 'Internal Server Error')
    {
        return $this->respondWithError($message, 500);
    }

    protected function respondUnathourize($message = 'Acesso não autorizado! Email ou senha inválido')
    {
        return $this->respondWithError($message, 401);
    }

    protected function respond(array $data, $statusCode = 200, $headers = [])
    {
        $data['success'] = true;

        return response()->json($data, $statusCode, $headers);
    }

    protected function respondWithError($message, $statusCode = 500)
    {
        return response()->json([
            'success' => false,
            'message' => $message
        ], $statusCode);
    }
}