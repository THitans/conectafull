<?php
/**
 * Created by PhpStorm.
 * User: horecio@gmail.com
 * Date: 17/03/19
 * Time: 21:09
 */

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;

trait ForgotPasswordResponse
{
    /**
     * @inheritdoc
     */
    protected function sendResetLinkResponse(Request $request, $response)
    {
        return $this->respond([
            'message' => 'Link enviado com sucesso'
        ]);
    }

    /**
     * @inheritdoc
     */
    protected function sendResetLinkFailedResponse(Request $request, $response)
    {
        return $this->respondWithError([$response]);
    }

    private function sendResetResponse($request, $response)
    {
        return $this->respond([
            'message' => 'Link enviado com sucesso'
        ]);
    }

    private function sendResetFailedResponse($request, $response)
    {
        return $this->respondWithError($response);
    }
}