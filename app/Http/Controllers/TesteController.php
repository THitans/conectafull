<?php

namespace App\Http\Controllers;

use App\Services\AdwordsService;
use DeskFull\Model\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class TesteController extends Controller
{

    public function getData()
    {

        return [
            "labels" => ["Janeiro", "Fevereiro", "Março"],
            "datasets" => [
                0 => [
                    "label" => "Compras",
                    "backgroundColor" => "#c4c4c4",
                    "data" => [60,20, 25]
                ],
                1 => [
                    "label" => "Vendas",
                    "backgroundColor" => "#F4F441",
                    "data" => [15,33, 44]
                ],
                2 => [
                    "label" => "Outros",
                    "backgroundColor" => "#ED4007",
                    "data" => [44,66, 12]
                ]
            ]
        ];
    }
    public function getTabelaReal(Customer $customer)
    {
        $campaignid = request('campaignid');
        $data =  AdwordsService::getCampaignPerformance(AdwordsService::makeSession($customer->account, $customer->customerId), null, 'LAST_30_DAYS', $campaignid);
        $headers = [];
        foreach($data['headers'] as $header) {
            $headers[] = ["text" => $header, "sortable" => false];
        }
        return ["headers"=>$headers, "data" => $data['data']];
    }
    public function getTopPalavrasChave()
    {
        $headers = [
            [ "text"=> 'Nome da Campanha', "sortable"=> false],
            [ "text"=> 'Cliques', "sortable"=> false],
            [ "text"=> 'Taxa de Conversão (%)', "sortable"=> false],
            [ "text"=> 'Total de Conversões', "sortable"=> false],
            [ "text"=> 'Custo por Conversão', "sortable"=> false],
            [ "text"=> 'Custo Total', "sortable"=> false],
        ];
        $data = [
            [ "[TG][Leads]", "6549", "33", "4", "14.26", "254"],
            [ "[Abv][SDA]", "21", "80", "67", "12.46", "1249"],
            [ "[TG][SD]", "59", "9", "98", "33.23", "125"],
        ];
        return ['headers'=>$headers, 'data'=>$data];
    }
    public function getCampanhaPrincipal()
    {
        $headers = [
            [ "text"=> 'Tipo de Dispositivo', "sortable"=> false],
            [ "text"=> "Cliques", "sortable"=> false],
            [ "text"=> "Impressões", "sortable"=> false],
            [ "text"=> "CPC Médio", "sortable"=> false],
            [ "text"=> "CTR (%)", "sortable"=> false],
            [ "text"=> "Custo ($)", "sortable"=> false],
        ];
        $data = [
            ["Dispositivos móveis", "284", "6802", "2.7400", "4.11", "778.66"],
            ["Computadores", "180", "588", "12.3566", "19.11", "8930.66"]
        ];
        return ['headers'=>$headers, 'data'=>$data];
    }
    public function uploadTest()
    {
        $file = base64_decode(request('pdf'));
        $directories = Storage::directories();
        if(!in_array('reports', $directories)) {
            Storage::makeDirectory('reports');
        }
        Storage::put('reports/oi.pdf', $file);
        return ['status'=>'ok'];
    }

    public function getTopAnuncios()
    {
        $fields = [
            'CampaignName',
            'Clicks',
            'ConversionRate',
            'Conversions',
            'CostPerConversion',
            'Cost'
        ];

        $params = request()->all();
        if(isset($params['CustomerId'])){
            $customer = Customer::find($params['CustomerId']);
            $campaign = $params['CampaignId'];
            if(is_null($campaign) OR $campaign == 'null') {
                unset($params['CampaignId']);
            }
            $session = AdwordsService::makeSession($customer->account, $customer->customerId);
            $results = AdwordsService::getCampaignPerformance($session, $params, $fields);

        }
        $headers = [
            'Nome da campanha',
            'Cliques',
            'Taxa de conversão',
            'Conversões',
            'Custo por Conversão',
            'Custo Total'
        ];

        foreach($headers as $k=>$header) {
            $headers[$k] = ["text" => $header, "sortable" => true];
        }

        if($results['success']) {
            $data = $results['result']['datasets'];
        }

        return ['success'=>true,'headers'=>$headers, 'data' => $data];
    }
}
