<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 06/02/19
 * Time: 17:51
 */

namespace App\Http\Repositories;

class UserRepositoryEloquent
{

    /**
     * Passa as informações do Usuário se tiver o mesmo email no banco de dados
     * irá retornar o usuário existente no banco de dados caso contrario cria um novo
     *
     * @param $request
     * @return mixed
     * @throws \Exception
     */
    public function store($request)
    {
        try {

            \DB::beginTransaction();

            $user = app('acl.model.user')::where('email', $request['email'])->get()->first();

            if(is_null($user)){

                $user = app('acl.model.user')->fill($request->except('roles'));
                $user->password = bcrypt($request['password']);
                $user->save();
            }

            \DB::commit();

            return $user;

        } catch (\Exception $exception) {

            report($exception);
            \DB::rollBack();

            throw  new \Exception($exception->getMessage());
        }

    }

}