<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 06/02/19
 * Time: 17:51
 */

namespace App\Http\Repositories;

use DeskFull\Model\Customer;
use DeskFull\Model\DeskFullClient;
use DeskFull\Model\UserTeam;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class DeskFullClientRepositoryEloquent
{
    public function store($request)
    {
        try {

            \DB::beginTransaction();

            $retorno = DeskFullClient::create($request->all());

            $customers = [];
            //  TODO Adiciona as contas associadas do cliente
            foreach ($request['customers'] as $objeto) {

                $ct = Customer::where('id', '=', $objeto['id'])->first();
                array_push($customers, $ct);
            }

            $retorno->customers()->saveMany($customers);
            \DB::commit();

            // TODO Adiciona os usuários da equipe dele que irão visualizar
            $listaUserTeam = [];
            foreach ($request['userTeams'] as $userTeamMember) {

                $ct = UserTeam::where('user_id_member', '=', $userTeamMember['id'])
                    ->where('user_id', '=', $request['user_id'])->first();
                array_push($listaUserTeam, $ct);
            }

            $retorno->userTeams()->saveMany($listaUserTeam);

            \DB::commit();

            return $retorno;
        } catch (\Exception $exception) {

            \DB::rollBack();
            throw  new \Exception($exception->getMessage());
        }
    }

    public function find($request)
    {
        $deskFullClient = DeskFullClient::find($request->clientId);

        $deskFullClient->customers;
        $deskFullClient->userTeams;

        return $deskFullClient;
    }

    public function list($request)
    {

        $deskFullClient = DeskFullClient::where('deskfull_client.user_id', '=', $request->user_id)
            ->select('deskfull_client.*')->get();

        foreach ($deskFullClient as $client) {

            $client['user_teams'] = DeskFullClient::join('user_team_has_deskfull_client as td', 'deskfull_client.id', '=', 'td.deskfull_client_id')
                ->join('user_team as user_teams', 'td.user_team_id', '=', 'user_teams.id')
                ->join('users as us', 'user_teams.user_id_member', '=', 'us.id')
                ->where('deskfull_client.id', '=', $client->id)
                ->select('us.*')->get();

        }
        return $deskFullClient;

    }

    public function delete($request)
    {

        return DeskFullClient::where('id', '=', $request->id)->delete();
    }

    public function update($request)
    {

        try {

            \DB::beginTransaction();

            $deskFull = DeskFullClient::find($request->id);

            $usertemsRemover = $deskFull->userTeams;
            $usertemsSalvar = $request->userTeams;

            $custormesRemove = $deskFull->customers;
            $custormesSalvar = $request->customers;

            $this->confereCustomersSalveRemove($custormesRemove, $custormesSalvar, $deskFull);
            $deskFull->update($request->all());

            $this->confereTeamSalveRemove($usertemsRemover, $usertemsSalvar, $deskFull, $request);

            \DB::commit();
            return true;
        } catch (\Exception $exception) {

            \DB::rollBack();
            throw  new \Exception($exception->getMessage());
        }
    }

    public function confereCustomersSalveRemove($custormesRemove, $custormesSalvar, $deskFull)
    {
        try {

            foreach ($custormesRemove as $keyTeam => $AccountsNovo) {

                foreach ($custormesSalvar as $key => $AccountsAntigo) {

                    if ($AccountsNovo->id == $AccountsAntigo['id']) {

                        array_pull($custormesSalvar, $key);
                        array_pull($custormesRemove, $keyTeam);
                    }
                }
            }
            $this->removeAccounts($custormesRemove, $deskFull);
            $this->salvarAccounts($custormesSalvar, $deskFull);

            return true;
        } catch (\Exception $exception) {

            throw  new \Exception($exception->getMessage());
        }
    }

    public function removeAccounts($custormesRemove, $deskFull)
    {

        foreach ($custormesRemove as $accounts) {

            $deskFull->customers()->detach($accounts->id);

        }


    }

    public function salvarAccounts($custormesSalvar, $deskFull)
    {

        //  TODO Adiciona as contas associadas do cliente

        $curstorms = [];
        foreach ($custormesSalvar as $objeto) {

            $ct = Customer::where('id', '=', $objeto['id'])->first();
            array_push($curstorms, $ct);
        }
        $deskFull->customers()->saveMany($curstorms);

    }

    /**
     * @param $usertemsRemover
     * @param $usertemsSalvar
     * @param $deskFull
     * @param $request
     * @return mixed
     *  Metodo que confere os usuário do team que vão ser removidos e salvos.
     */
    private function confereTeamSalveRemove($usertemsRemover, $usertemsSalvar, $deskFull, $request)
    {

        try {

            foreach ($usertemsRemover as $keyTeam => $userTeamNovo) {

                foreach ($usertemsSalvar as $key => $userTeamAntigo) {

                    if ($userTeamNovo->user_id_member == $userTeamAntigo['id']) {

                        array_pull($usertemsSalvar, $key);
                        array_pull($usertemsRemover, $keyTeam);
                    }
                }
            }
            $this->deskfullTeamRemove($usertemsRemover, $deskFull);
            return $this->deskfullTeamSave($usertemsSalvar, $deskFull, $request);

        } catch (\Exception $exception) {

            throw  new \Exception($exception->getMessage());
        }
    }

    /**
     * @param $usertemsSalvar
     * @param $deskFull
     * @param $request
     * @return mixed
     *  Função que Salva os usuários do Team
     */
    private function deskfullTeamSave($usertemsSalvar, $deskFull, $request)
    {
        try {
            $listaUserTeam = [];
            foreach ($usertemsSalvar as $userTeamMember) {

                $ct = UserTeam::where('user_id_member', '=', $userTeamMember['id'])
                    ->where('user_id', '=', $request['user_id'])->first();
                array_push($listaUserTeam, $ct);
            }
            return $deskFull->userTeams()->saveMany($listaUserTeam);
        } catch (\Exception $exception) {

            throw  new \Exception($exception->getMessage());
        }
    }

    /**
     * @param $usertemsRemover
     * @param $deskFull
     * Função que remove os usuários de Team do deskfullClient
     */
    private function deskfullTeamRemove($usertemsRemover, $deskFull)
    {
        try {

            foreach ($usertemsRemover as $team) {

                $deskFull->userTeams()->detach($team->id);

            }
        } catch (\Exception $exception) {

            throw  new \Exception($exception->getMessage());
        }

    }

    /**
     * Lista todos os clientes das equipes que ele participa conforme os schema dos clientes
     *
     */
    public function listTeamClientUser($userLider)
    {
        return DB::table($userLider->schema . '.deskfull_client')
            ->join($userLider->schema . '.user_team_has_deskfull_client as uthdc', 'deskfull_client.id', '=', 'uthdc.deskfull_client_id')
            ->join('public.user_team as ut', 'uthdc.user_team_id', '=', 'ut.id')
            ->where('ut.user_id_member', '=', auth('api')->user()->id)
            ->where('deskfull_client.deleted_at', '=', null)
            ->select('deskfull_client.*')
            ->get();
    }



    /**
     *  Lista os team do USUÁRIO LOGADO
     */
    public function listTeam()
    {
        return DeskFullClient::join('user_team_has_deskfull_client as uthdc', 'deskfull_client.id', '=', 'uthdc.deskfull_client_id')
            ->join('public.user_team as ut', 'uthdc.user_team_id', '=', 'ut.id')
            ->where('ut.user_id', '=', auth('api')->user()->id)
            ->where('deskfull_client.deleted_at', '=', null)

            ->select('deskfull_client.*')
            ->get();
    }


    /**
     * Lista todos os clientes das equipes que ele participa conforme os schema dos clientes
     *
     */
    public function listTeamClientUserDeskfullId($userLider, $deskfullId)
    {
        return DB::table($userLider->schema . '.deskfull_client')
            ->join($userLider->schema . '.user_team_has_deskfull_client as uthdc', 'deskfull_client.id', '=', 'uthdc.deskfull_client_id')
            ->join('public.user_team as ut', 'uthdc.user_team_id', '=', 'ut.id')
            ->join($userLider->schema . '.deskful_client_has_conta as cc', 'deskfull_client.id', '=', 'cc.deskfull_client_id')
            ->join('public.customers as ac', 'ac.id', '=', 'cc.customers_id')
            ->where('deskfull_client.deleted_at', '=', null)
            //->where('ut.user_id_member', '=', auth('api')->user()->id)
            ->where('deskfull_client.id','=',$deskfullId)
            ->where('ac.is_active','=','true')
            ->select('ac.*')
            ->get();
    }


}