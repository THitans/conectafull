<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 06/02/19
 * Time: 17:51
 */

namespace App\Http\Repositories;

use Artesaos\Defender\Facades\Defender;
use DeskFull\Model\UserTeam;
use Illuminate\Http\Request;

class UserTeamRepositoryEloquent
{

    private $userRepository;

    /**
     * UserTeamRepositoryEloquent constructor.
     */
    public function __construct(UserRepositoryEloquent $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function listUserTeam($request)
    {

        try {

            $listTeam = UserTeam::where('user_id', '=', $request->user_id)
                ->join("users as us", "user_id_member", "us.id")
                ->select('*')->get();


            return $listTeam;
        } catch (\Exception $exception) {

            throw  new \Exception($exception->getMessage());
        }
    }

    public function removerMember($request)
    {
        try {

            \DB::beginTransaction();

            $userTeam = UserTeam::where('user_id', '=', $request->user_id)
                ->where('user_id_member', '=', $request->user_id_member)->first();

            $retorn = $userTeam->delete();

            \DB::commit();

            return $retorn;

        } catch (\Exception $exception) {

            report($exception);
            \DB::rollBack();

            throw  new \Exception($exception->getMessage());
        }
    }

    public function store($request)
    {

        $userTeam = null;
        try {
            $request['password'] = '123456';
            $request['type'] = 'Cliente';

            // TODO cria um novo usuário retorna o usuário existente no banco de dados
            $user = $this->userRepository->store($request);

            //$request['user_id'] = 1;
            if (!is_null($user)) {

                $request['user_id_member'] = $user->id;

                $userTeam = UserTeam::where('user_id', '=', $request['user_id'])
                    ->where('user_id_member', '=', $request['user_id_member'])
                    ->get();

                if (isset($userTeam)) {
                    \DB::beginTransaction();

                    $userTeam = UserTeam::create($request->all());

                    $equipeRole = Defender::findRole('Membro da Equipe');

                    $user->attachRole($equipeRole);
                    //Enviar email
                    //app('mailer.login_activation')->sendActivationMail($user);

                    \DB::commit();
                    return $userTeam;

                }

                return ['message' => "Usuário já participa do Time!"];

            } else {
                return $this->respondWithError("Erro ao Salvar Usuário!", 500);
            }

        } catch (\Exception $exception) {

            report($exception);
            \DB::rollBack();

            throw  new \Exception($exception->getMessage());
        }
    }


}