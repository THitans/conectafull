<?php

namespace App\Http\Middleware;

use Closure;

class SchemaSwitch
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//
        if (auth('api')->user()->type == 'Cliente' && isset(auth('api')->user()->schema )) {
//            define('STDIN',fopen("php://stdin","r"));
            \PGSchema::switchTo(auth('api')->user()->schema);
        }

        return $next($request);
    }
}
