<?php
/**
 * Created by PhpStorm.
 * User: ifto
 * Date: 2/18/19
 * Time: 1:56 PM
 */

namespace App\Services;

use Google\AdsApi\Common\GuzzleLogMessageFormatter;
use Google\AdsApi\Common\GuzzleLogMessageFormatterProvider;


class CustomAdWordsGuzzleLogMessageFormatterProvider implements
GuzzleLogMessageFormatterProvider
{

    private static $HTTP_HEADERS_TO_SCRUB = ['Authorization', 'developerToken'];

    private $session;
    private $shouldLogResponsePayload;
    private $redactedResponsePayloadMessage;

    /**
     * Creates a new AdWords Guzzle log message formatter provider with the
     * specified settings.
     *
     * @param AdWordsSession $session the AdWords session that makes a Guzzle
     *     HTTP call
     * @param bool $shouldLogResponsePayload whether the formatter should
     *     show the response payload in the log
     * @param string|null $redactedResponsePayloadMessage the substitution
     *     message to use in case the response payload is redacted
     */
    public function __construct(
        CustomAdWordsSession $session,
        $shouldLogResponsePayload,
        $redactedResponsePayloadMessage = null
    ) {
        $this->session = $session;
        $this->shouldLogResponsePayload = $shouldLogResponsePayload;
        $this->redactedResponsePayloadMessage = $redactedResponsePayloadMessage;
    }

    /**
     * @see GuzzleLogMessageFormatterProvider::getGuzzleLogMessageFormatter
     */
    public function getGuzzleLogMessageFormatter()
    {
        return new GuzzleLogMessageFormatter(
            self::$HTTP_HEADERS_TO_SCRUB,
            ['clientCustomerId' => $this->session->getClientCustomerId()],
            $this->shouldLogResponsePayload,
            $this->redactedResponsePayloadMessage
        );
    }
}
