<?php

namespace App\Services;

use App\Exceptions\GoogleApiException;
use DeskFull\Model\Account;
use DeskFull\Model\AdwordsAccount;
use DeskFull\Model\Customer;
use Google\AdsApi\AdWords\AdWordsServices;
use Google\AdsApi\AdWords\Query\v201809\ReportQueryBuilder;
use Google\AdsApi\AdWords\Reporting\v201809\DownloadFormat;
use Google\AdsApi\AdWords\Reporting\v201809\ReportDownloader;
use Google\AdsApi\AdWords\Reporting\v201809\ReportDownloadError;
use Google\AdsApi\AdWords\ReportSettingsBuilder;
use Google\AdsApi\AdWords\v201809\cm\ApiException;
use Google\AdsApi\AdWords\v201809\cm\CampaignService;
use Google\AdsApi\AdWords\v201809\cm\Selector;
use Google\AdsApi\AdWords\v201809\mcm\CustomerService;
use Google\AdsApi\AdWords\v201809\mcm\ManagedCustomerService;
use Google\AdsApi\AdWords\v201809\cm\Operator;
use Google\AdsApi\AdWords\v201809\mcm\LinkOperation;
use Google\AdsApi\AdWords\v201809\mcm\LinkStatus;
use Google\AdsApi\AdWords\v201809\mcm\ManagedCustomerLink;
use Google\AdsApi\AdWords\v201809\mcm\MoveOperation;
use Google\AdsApi\AdWords\v201809\mcm\PendingInvitationSelector;
use Google\AdsApi\Common\OAuth2TokenBuilder;
use Google\Auth\OAuth2;

use Google\AdsApi\AdWords\v201809\cm\OrderBy;
use Google\AdsApi\AdWords\v201809\cm\Paging;
use Google\AdsApi\AdWords\v201809\cm\SortOrder;
use Google\AdsApi\AdWords\v201809\cm\ReportDefinitionReportType;
use Google\AdsApi\AdWords\Reporting\v201809\ReportDefinitionDateRangeType;


class AdwordsService
{
    const PAGE_LIMIT = 50;

    public static function getHierarchy(Customer $customer, $toArray = false)
    {
        $managedCustomerService = (new AdWordsServices())->get(
            self::makeSession($customer->account, $customer->customerId),
            ManagedCustomerService::class
        );
        // Create selector.
        $selector = new Selector();
        $selector->setFields(['CustomerId', 'Name', 'CanManageClients', 'TestAccount']);
        $selector->setOrdering([new OrderBy('CustomerId', SortOrder::ASCENDING)]);
        $selector->setPaging(new Paging(0, self::PAGE_LIMIT));
        // Maps from customer IDs to accounts and links.
        $customerIdsToAccounts = [];
        $customerIdsToChildLinks = [];
        $customerIdsToParentLinks = [];
        $totalNumEntries = 0;
        do {
            // Make the get request.
            $page = $managedCustomerService->get($selector);
            // Create links between manager and clients.
            if ($page->getEntries() !== null) {
                $totalNumEntries = $page->getTotalNumEntries();
                if ($page->getLinks() !== null) {
                    foreach ($page->getLinks() as $link) {
                        // Cast the indexes to string to avoid the issue when 32-bit PHP
                        // automatically changes the IDs that are larger than the 32-bit max
                        // integer value to negative numbers.
                        $managerCustomerId = strval($link->getManagerCustomerId());
                        $customerIdsToChildLinks[$managerCustomerId][] = $link;
                        $clientCustomerId = strval($link->getClientCustomerId());
                        $customerIdsToParentLinks[$clientCustomerId] = $link;
                    }
                }
                foreach ($page->getEntries() as $account) {
                    $hierarchy['ids'][] = [
                        'customerId' => $account->getCustomerId(),
                        'name'=> $account->getName(),
                        'canManageClients'=> $account->getCanManageClients(),
                        'testAccount'=> $account->getTestAccount(),
                    ];
                    $customerIdsToAccounts[strval($account->getCustomerId())] = $account;
                }
            }
            // Advance the paging index.
            $selector->getPaging()->setStartIndex(
                $selector->getPaging()->getStartIndex() + self::PAGE_LIMIT
            );
        } while ($selector->getPaging()->getStartIndex() < $totalNumEntries);
        // Find the root account.
        $rootAccount = null;
        foreach ($customerIdsToAccounts as $account) {
            if (!array_key_exists(
                $account->getCustomerId(),
                $customerIdsToParentLinks
            )) {
                $rootAccount = $account;
                break;
            }
        }
        if ($rootAccount !== null) {

            $hierarchy['rootAccount'] = [
                'id' => $rootAccount->getCustomerId(),
                'name' => $rootAccount->getName(),
            ];
        } else {
            $hierarchy = [];
        }
        return $toArray ? $hierarchy : $customerIdsToAccounts;
    }

    public static function getCustomers(AdwordsAccount $account, Customer $customer = null)
    {

        if(empty($customer)) {
            $session = self::makeSession($account);
        } else {
            $session = self::makeSession($account, $customer->customerId);
        }
        $customerService = (new AdWordsServices())->get($session, CustomerService::class);
        $customers = $customerService->getCustomers();
        return $customers;
    }

    public static function sendInvitation(Customer $customer)
    {
        $linkOp = new LinkOperation();
        $link = new ManagedCustomerLink();
        $link->setManagerCustomerId(config('services.adwords.client_customer_id'));
        $link->setClientCustomerId($customer->customerId);
        $link->setLinkStatus(LinkStatus::PENDING);
        $linkOp->setOperand($link);
        $linkOp->setOperator(Operator::ADD);

        $session = self::makeRootSession();
        $managedCustomerService = (new AdWordsServices())->get($session,  ManagedCustomerService::class);
        $managedCustomerService->mutateLink([$linkOp]);
        return ['success'=>true];
    }

    /**
     * Get the pending invitations
     * **/
    public static function pendingInvitations()
    {
        $selector = new PendingInvitationSelector();
        $session = self::makeRootSession();
        $managedCustomerService = (new AdWordsServices())->get($session,  ManagedCustomerService::class);
        $invitations = $managedCustomerService->getPendingInvitations($selector);
        $result = [];
        if($invitations) {
            foreach($invitations as $invitation) {
                $client = $invitation->getClient();
                $manager = $invitation->getManager();
                $creationDate = $invitation->getCreationDate();
                $expirationDate = $invitation->getExpirationDate();
                $result[] = [
                    'client' => [
                        'customerId' => $client->getCustomerId(),
                        'name' => $client->getName()
                    ],
                    'manager' => [
                        'customerId' => $manager->getCustomerId(),
                        'name' => $manager->getName()
                    ],
                    'creationDate'=>$creationDate,
                    'expirationDate'=>$expirationDate
                ];
            }
        }

        return ['success'=>true,'result'=>$result];
    }

    /*
     * Set to inactive a link between a customer and a manager
     * */
    public static function inactivateLink(Customer $customer)
    {
        $linkOp = new LinkOperation();
        $link = new ManagedCustomerLink();
        $link->setClientCustomerId($customer->customerId);
        $link->setLinkStatus(LinkStatus::INACTIVE);
        $link->setManagerCustomerId(config('services.adwords.client_customer_id'));
        $linkOp->setOperand($link);
        $linkOp->setOperator(Operator::SET);

        $session = self::makeRootSession();
        $managedCustomerService = (new AdWordsServices())->get($session,  ManagedCustomerService::class);
        $managedCustomerService->mutateLink([$linkOp]);
        return ['success'=>true];
    }

    /**
    * Change the manager of a customer
     */
    public static function mutateManager( Customer $customer, $oldManager, $newManager=null)
    {
        if(empty($newManager)) {
            $newManager = config('services.adwords.client_customer_id');
        }
        $op = new MoveOperation();
        $op->setOldManagerCustomerId($oldManager);
        $op->setOperator(Operator::SET);
        $link = new ManagedCustomerLink();
        $link->setClientCustomerId($customer->customerId);
        $link->setManagerCustomerId($newManager);
        $link->setLinkStatus(LinkStatus::ACTIVE);
        $op->setOperand($link);

        $session = self::makeRootSession();
        $managedCustomerService = (new AdWordsServices())->get($session,  ManagedCustomerService::class);
        $managedCustomerService->mutateManager([$op]);
        return ['success'=>true];
    }

    public static function getCampaigns(Customer $customer)
    {
        $campaigns = [];

        $campaignService = (new AdWordsServices())->get(self::makeSession($customer->account, $customer->customerId), CampaignService::class);
        // Create selector.
        $selector = new Selector();
        $selector->setFields(['Id', 'Name', 'CampaignStatus', 'Status', 'ServingStatus', 'StartDate', 'EndDate', 'Labels']);
        $selector->setOrdering([new OrderBy('Name', SortOrder::ASCENDING)]);
        $selector->setPaging(new Paging(0, self::PAGE_LIMIT));
        $totalNumEntries = 0;
        do {
            // Make the get request.
            $page = $campaignService->get($selector);
            // Display results.
            if ($page->getEntries() !== null) {
                $totalNumEntries = $page->getTotalNumEntries();
                foreach ($page->getEntries() as $campaign) {
                    $campaigns[] = [
                        'id' => $campaign->getId(),
                        'name' => $campaign->getName(),
                        'servingStatus' => $campaign->getServingStatus(),
                        'status' => $campaign->getStatus(),
                        'startDate' => $campaign->getStartDate(),
                        'endDate' => $campaign->getEndDate(),
                        'labels' => $campaign->getLabels()
                    ];
                }
            }
            // Advance the paging index.
            $selector->getPaging()->setStartIndex(
                $selector->getPaging()->getStartIndex() + self::PAGE_LIMIT
            );
        } while ($selector->getPaging()->getStartIndex() < $totalNumEntries);

        return ['success'=>true, 'result'=>$campaigns];
    }

    public static function makeCustomerAuth(Account $account){

        $oauth2 = self::makeAuth();

        $oauth2->setRefreshToken($account->refresh_token);

        return $oauth2;
    }
    public static function makeRootAuth() {
        return self::makeAuth();
    }
    public static function makeAuth()
    {
        return new OAuth2([
            'authorizationUri' => 'https://accounts.google.com/o/oauth2/v2/auth',
            'tokenCredentialUri' => 'https://www.googleapis.com/oauth2/v4/token',
            'redirectUri' => config('app.url') . '/oauth2-callback',
            'clientId' => config('services.adwords.client_id'),
            'clientSecret' => config('services.adwords.secret_key'),
            'scope' => [
                'https://www.googleapis.com/auth/adwords',
                'https://www.googleapis.com/auth/userinfo.profile',
                'https://www.googleapis.com/auth/userinfo.email'
            ]
        ]);
    }
    public static function makeRootSession()
    {
        $oauth2 = AdwordsService::makeRootAuth();
        $oauth2->setRefreshToken(config('services.adwords.refresh_token'));
        $session = (new CustomAdWordsSessionBuilder())
            ->fromFile(config('app.adsapi_php_path'))
            ->withOAuth2Credential($oauth2)
            ->withClientCustomerId(config('services.adwords.client_customer_id'));
        return $session->build();
    }
    public static function makeSession(Account $account, $customerid = null)
    {
        $oauth2 = AdwordsService::makeCustomerAuth($account);
        $session = (new CustomAdWordsSessionBuilder())
            ->fromFile(config('app.adsapi_php_path'))
            ->withOAuth2Credential($oauth2);
        if(!is_null($customerid)) {
            $session->withClientCustomerId($customerid);
        }
        return $session->build();
    }

    public static function getRanges()
    {
        return [
            ReportDefinitionDateRangeType::ALL_TIME,
            ReportDefinitionDateRangeType::LAST_7_DAYS,
            ReportDefinitionDateRangeType::LAST_14_DAYS,
            ReportDefinitionDateRangeType::LAST_30_DAYS,
            ReportDefinitionDateRangeType::LAST_BUSINESS_WEEK,
            ReportDefinitionDateRangeType::LAST_WEEK,
            ReportDefinitionDateRangeType::LAST_MONTH,
            ReportDefinitionDateRangeType::THIS_MONTH,
            ReportDefinitionDateRangeType::TODAY,
            ReportDefinitionDateRangeType::YESTERDAY
        ];
    }

    protected static function getResults($session, $query)
    {
        $reportDownloader = new CustomReportDownloader($session);

        $reportSettingsOverride = (new ReportSettingsBuilder())
            ->includeZeroImpressions(false)
            ->build();

        $reportDownloadResult = $reportDownloader->downloadReportWithAwql(
            sprintf('%s', $query),
            DownloadFormat::XML,
            $reportSettingsOverride
        );
        $result = self::responseToArray($reportDownloadResult->getAsString());

        $result = $result['table'];
        $columns = $result['columns']['column'];
        $attributes = [];
        foreach($columns as $column) {
            $column = $column['@attributes'];
            $attributes[$column['name']] = $column['display'];
        }

        $values = [];
        if(isset($result['row'])) {
            $rows = $result['row'];
            if(isset($rows['@attributes'])){
                if(count($rows) == 1) {
                    $values[] = $rows['@attributes'];
                } else {
                    foreach($rows as $row) {
                        $row = isset($row['@attributes']) ? $row['@attributes'] : $row;
                        $values[] = $row;
                    }
                }
            } else {
                foreach($rows as $row) {
                    if(isset($row['@attributes'])) {
                        $values[] = $row['@attributes'];
                    }
                }
            }
        }
        return [
            'success' => true,
            'result' => [
                'headers' => $attributes,
                'datasets' => $values
            ]
        ];
    }
    public static function getPerformanceHistory(CustomAdWordsSession $session, $params, $fields=null) {
        if(empty($fields)) {
            $fields = [
                'CampaignId',
                'CampaignName',
                'CampaignStatus',
                'TotalAmount',
                'Amount',
                'Date',
                'AllConversions',
                'AllConversionValue',
                'AverageCost',
                'AverageCpc',
                'AverageCpm',
                'Clicks',
                'ConversionRate',
                'Conversions',
                'ConversionValue',
                'Cost',
                'Impressions',
                'ValuePerAllConversion',
                'ValuePerConversion'
            ];
        }

        return self::query($session, $fields, $params,ReportDefinitionReportType::CAMPAIGN_PERFORMANCE_REPORT);
    }
    public static function getAccountPerformance(CustomAdWordsSession $session, $params, $fields=null)
    {
        if(empty($fields)) {
            $fields = [
                "AverageCost",
                "AverageCpc",
                "Ctr",
                "Clicks",
                "Cost",
                "AveragePosition",
                "Impressions"
            ];
        }

        return self::query($session, $fields, $params,ReportDefinitionReportType::ACCOUNT_PERFORMANCE_REPORT);
    }
    public static function getClicksPerformance(CustomAdWordsSession $session, $params, $fields=null)
    {
        //esse relatório só pode ser usado para o período de um dia
        if(empty($fields)) {
            $fields = [
                'AccountDescriptiveName',
                'AdGroupName',
                'CampaignId',
                'CampaignName',
                'CampaignStatus',
                'UserListId',
                'AdGroupId',
                'Clicks',
                'ClickType',
                'Date',
                'Device',
                'MonthOfYear',
            ];
        }

        return self::query($session, $fields, $params,ReportDefinitionReportType::CLICK_PERFORMANCE_REPORT);
    }

    public static function query($session, $fields, $params, $report) {
        unset($params['CustomerId']);
        $query = (new ReportQueryBuilder())
            ->select($fields)
            ->from($report);

        if(isset($params['DateRange'])) {
            $dateRange = $params['DateRange'];

            if($dateRange === 'CUSTOM_DATE') {
                if(isset($params['StartDate']) && isset($params['EndDate'])) {
                    $startDate = str_replace('-','', trim($params['StartDate']));
                    $endDate = str_replace('-','', trim($params['EndDate']));
                    if($startDate != '' && $endDate != '') {
                        $query->during($startDate, $endDate);
                    }
                }
            } else {
                if($dateRange <> 'ALL_TIME') {
                    $query->duringDateRange($dateRange);
                }
            }
            unset($params['DateRange']);
            unset($params['StartDate']);
            unset($params['EndDate']);
        }
        foreach($params as $key=>$param) {
            $query = $query->where($key)->equalTo($param);
        }
        return self::getResults($session, $query->build());
    }

    public static function getCampaignPerformance(CustomAdWordsSession $session,  $params, $fields=null)
    {
        if(empty($fields)) {
            $fields = [
                'CampaignId',
                'CampaignName',
                'CampaignStatus',
                'TotalAmount',
                'Amount',
                'AllConversions',
                'AllConversionValue',
                'AverageCost',
                'AverageCpc',
                'AverageCpm',
                'Clicks',
                'ConversionRate',
                'Conversions',
                'ConversionValue',
                'Cost',
                'Impressions',
                'ValuePerAllConversion',
                'ValuePerConversion'
            ];
        }
        return self::query($session, $fields, $params,ReportDefinitionReportType::CAMPAIGN_PERFORMANCE_REPORT);
    }
    public static function getAdPerformance(CustomAdWordsSession $session,  $params, $fields=null)
    {
        if(empty($fields)) {
            $fields = [
                'Description',
                'CampaignName',
                'Impressions',
                'Ctr',
                'Clicks',
                'AveragePosition',
                'Conversions',
                'ConversionRate',
                'CostPerConversion',
                'Cost',
            ];
        }
        return self::query($session, $fields, $params,ReportDefinitionReportType::AD_PERFORMANCE_REPORT);
    }

    public static function getKeywordsPerformance(CustomAdWordsSession $session, array $params, array $fields=null)
    {
        if(empty($fields)) {
            $fields = [
                'KeywordTextMatchingQuery',
                'CampaignName',
                'Clicks',
                'ConversionRate',
                'Conversions',
                'CostPerConversion',
                'Cost'
            ];
        }
        return self::query($session, $fields, $params,ReportDefinitionReportType::SEARCH_QUERY_PERFORMANCE_REPORT);
    }
    public static function responseToArray($xmlResponse)
    {
        $xmlResponse = simplexml_load_string($xmlResponse);
        $json = json_encode($xmlResponse);

        return json_decode($json, true);
    }
}