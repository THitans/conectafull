<?php

namespace App\Exceptions;

use Exception;
use Google\AdsApi\AdWords\v201809\cm\ApiException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception $exception
     * @return void
     * @throws Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $exception
     * @return \Illuminate\Http\JsonResponse
     */
    public function render($request, Exception $exception)
    {
        report($exception);
        // Verifica a Exception do Form Request para retornar JSOn
        if ($exception instanceof \Illuminate\Validation\ValidationException) {
            return new JsonResponse([
                'success' => false,
                'errors' => $exception->errors()
            ], 422);
        }

        if ($exception instanceof \Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException) {
            return response()->json([
                'success' => false,
                'message' => 'Acesso não Autorizado ' . $exception->getMessage()
            ], 401);
        }

        // Exception do Sistema Sendler
        if ($exception instanceof \App\Exceptions\AppException) {
            return new JsonResponse([
                'success' => false,
                'message' => $exception->getMessage()
            ], $exception->getStatusCode());
        }

        // Exception do Sistema Sendler
        if ($exception instanceof NotFoundHttpException) {
            return new JsonResponse([
                'success' => false,
                'message' => 'Página Não Encontrada!'
            ], 404);
        }

        // Exception do Sistema Sendler
        if ($exception instanceof MethodNotAllowedHttpException) {
            return new JsonResponse([
                'success' => false,
                'message' => 'Método Não pertmitido para esta URL!'
            ], 405);
        }

        if ($exception instanceof ApiException) {
            $errors = $exception->getErrors();
            $messages = [];
            foreach ($errors as $error) {
                if (method_exists($error, 'getReason')) {
                    $reason = $error->getReason();
                    if($reason === 'UNKNOWN') {
                        $reason = $error->getErrorString();
                        $reason = explode('.', $reason)[1];
                    }
                } else {
                    $reason = $error->getErrorString();
                    $reason = explode('.', $reason)[1];
                }
                switch ($reason) {
                    case 'TWO_STEP_VERIFICATION_NOT_ENROLLED':
                        $messages[] = 'Falha com autenticação de 2 passos.';
                        break;
                    case 'INVALID_DURING_CLAUSE':
                        $messages[] = 'Período informado é inválido para o relatório selecionado.';
                        break;
                    case 'INVALID_PREDICATE_VALUE':
                        $messages[] = 'Parâmetros de filtragem inválidos.';
                        break;
                    case 'TEST_ACCOUNT_LINK_ERROR':
                        $messages[] = 'Essa operação não é permitida para contas teste.';
                        break;
                    case 'ALREADY_MANAGED_BY_THIS_MANAGER':
                        $messages[] = 'Conta convidada já é gerenciada pelo sistema.';
                        break;
                    case 'ALREADY_INVITED_BY_THIS_MANAGER':
                        $messages[] = 'Conta já foi convidada.';
                        break;
                    case 'NO_ACTIVE_LINKS_FOUND':
                        $messages[] = 'Conta não possui vínculo com o sistema.';
                        break;
                    case 'BAD_ID':
                        $messages[] = 'Cliente ainda não é gerenciado, portanto não possui vínculo.';
                        break;
                    default:
                        $messages[] = 'Ocorreu uma falha durante a comunicação com o serviço da Google. '. $exception->getMessage();
                }
            }
            return new JsonResponse([
                'success' => false,
                'message' => $messages
            ], 500);
        }

        return new JsonResponse([
            'success' => false,
            'message' => 'Erro no Sistema! Procure o Administrador. ' . $exception->getMessage()
        ], 500);
    }
}
