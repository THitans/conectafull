<?php

namespace App\Exceptions;

class AppException extends \Exception implements ExceptionInterface
{
    public function __construct($message, $code = 402)
    {
        parent::__construct($message, $code);
    }

    public function getStatusCode()
    {
        return $this->getCode();
    }
}