<?php
namespace App\Model;

trait UploadHelper
{
    private $file;
    private $fileUpload;
    private $pathFile;

    private function cropImage()
    {
    }

    private function fillMultPart()
    {
        $this->file = app('model.file');
        $this->file->name = $this->fileUpload->getClientOriginalName();
        $this->file->file_name = $this->fileUpload->getFilename();
        $this->file->extension = $this->fileUpload->getClientOriginalExtension();
        $this->file->path_file = $this->path;
        $this->file->mimetype_file = $this->fileUpload->getMimeType();
        $this->file->size = $this->fileUpload->getSize();

        return $this;
    }

    private function storeFile($requestFile)
    {
        $this->fileUpload = $requestFile;
        // TODO Cortar imagem quadrada no metodo cropImage antes de fazer upload
        $this->path = \Storage::disk('public')->putFile('profile', $this->fileUpload);

        return $this;
    }

    private function saveFile()
    {
        $this->file->save();

        return $this;
    }
}