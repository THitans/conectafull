<?php

namespace App\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $path_file
 * @property string $mimetype_file
 * @property int $size
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property MettingPlan[] $mettingPlans
 */
class File extends BaseModel implements FileInterface
{
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'path_file',
        'mimetype_file',
        'size'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
