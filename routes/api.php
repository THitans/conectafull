<?php

Route::post('/login', 'Auth\LoginController@login');
Route::post('/logout', 'Auth\LoginController@logout');
Route::post('/me', 'Auth\LoginController@me');

$this->post('send-forgot-password', [
    'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail',
    'as' => 'user.update_password',
]);
$this->post('/reset-password', 'Auth\ForgotPasswordController@reset');
Route::post('/upload-test', 'TesteController@uploadTest');

/*Route::prefix('deskfull')->group(function () {
    $this->group(['prefix' => '/user-team'], function () {
        $this->post('/store', [
            'uses' => 'UserTeamController@store',
            'as' => 'user.store',
            'shield' => 'user.create'
        ]);
    });
});
*/



