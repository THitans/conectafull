<?php
/*Route::get('{any}', function () {
    return view('welcome');
})->where('any', '.*');*/

Route::get('/', function() {
    return view('welcome');
});

Route::get('/default/{any}', function () {
    return view('welcome');
})->where('any', '.*');

Route::get('session/forgot-password', function () {
    return view('welcome');
});

Route::get('session/reset-password/{email}/{token}', function () {
    return view('welcome');
});

Route::get('/session/login', function () {
    return view('welcome');
});
