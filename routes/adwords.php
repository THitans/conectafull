<?php
Route::get('/oauth2', '\DeskFull\Http\Controllers\AdwordsController@oauth2')->name('.test');
Route::get('/oauth2-callback', '\DeskFull\Http\Controllers\AdwordsController@callback')->name('.callback');
