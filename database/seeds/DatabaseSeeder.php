<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if (isEnv('local')) {
            $this->call([
                AdminUserTableSeeder::class,
                ClienteUserTableSeeder::class
            ]);
        }

        if (isEnv('production')) {
            $this->call([
                AdminUserTableSeeder::class
            ]);
        }
    }
}
