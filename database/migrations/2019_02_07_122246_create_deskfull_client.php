<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeskfullClient extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deskfull_client', function (Blueprint $table) {
            $table->increments('id');

            $table->string('nome');
            $table->string('email');
            $table->string('site');

            // ID do susuário responsável
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('public.users');

            $table->integer('logo_id')->nullable()->unsigned();
            $table->foreign('logo_id')
                ->references('id')
                ->on('files');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deskfull_client');
    }
}
