<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeskFullClientAccount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deskful_client_has_conta', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('deskfull_client_id');
            $table->unsignedInteger('customers_id');
            $table->foreign('customers_id')->references('id')->on('public.customers');
            $table->foreign('deskfull_client_id')->references('id')->on('deskfull_client');

            $table->softDeletes();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deskful_client_has_conta');
    }
}
