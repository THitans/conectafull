<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTeamHasDeskfullClient extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_team_has_deskfull_client', function (Blueprint $table) {

            $table->increments('id');

            $table->unsignedInteger('user_team_id');
            $table->unsignedInteger('deskfull_client_id');

            $table->foreign('user_team_id')->references('id')->on('public.user_team');
            $table->foreign('deskfull_client_id')->references('id')->on('deskfull_client');

            $table->softDeletes();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_team_has_deskfull_client');
    }
}
