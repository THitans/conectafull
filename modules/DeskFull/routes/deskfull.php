<?php
$this->group([
    'middleware' => ['auth:api', 'needsPermission', 'needsRole']],
    function () {
        $this->group(['prefix' => '/accounts'], function () {
            $this->get('/', [
                'uses' => 'AccountsController@index',
                'as' => 'accounts.index',
                'shield' => 'accounts.index'
            ]);
            $this->get('/{account}/customers', [
                'uses' => 'AccountsController@customers',
                'as' => 'accounts.customers',
                'shield' => 'accounts.index'
            ]);
            $this->get('/{account}/sync', [
                'uses' => 'AccountsController@sync',
                'as' => 'accounts.sync',
                'shield' => 'accounts.index'
            ]);
            $this->delete('/{account}', [
                'uses' => 'AccountsController@destroy',
                'as' => 'accounts.destroy',
                'shield' => 'accounts.index'
            ]);
            $this->get('/manager', [
                'uses' => 'AccountsController@manager',
                'as' => 'accounts.manager',
                'shield' => 'accounts.index'
            ]);
            $this->get('/manage-accounts', [
                'uses' => 'AccountsController@manageAccounts',
                'as' => 'accounts.manageAccounts',
                'shield' => 'accounts.manage'
            ]);
            $this->get('/manage-accounts/{customer}/toggle-status', [
                'uses' => 'CustomersController@toggleStatus',
                'as' => 'accounts.manageAccounts',
                'shield' => 'accounts.manage'
            ]);
            $this->get('/invitations', [
                'uses' => 'AdwordsController@pendingInvitations',
                'as' => 'accounts.pendingInvitations',
                'shield' => 'accounts.manage'
            ]);

        });

        $this->group(['prefix' => '/customers'], function () {

            $this->get('/', [
                'uses' => 'CustomersController@userCustomers',
                'as' => 'customers.usercustomers',
                'shield' => 'accounts.index'
            ]);
            $this->get('/production-customers', [
                'uses' => 'CustomersController@productionCustomers',
                'as' => 'customers.productionCustomers',
                'shield' => 'customers.index'
            ]);
            $this->get('/customerid/{customerid}', [
                'uses' => 'CustomersController@show',
                'as' => 'customers.show',
                'shield' => 'accounts.index'
            ]);
            $this->delete('/{customer}', [
                'uses' => 'CustomersController@destroy',
                'as' => 'customers.destroy',
                'shield' => 'accounts.index'
            ]);
            $this->get('/{customer}/campaigns', [
                'uses' => 'AdwordsController@campaigns',
                'as' => 'reports.overview',
                'shield' => 'reports.overview'
            ]);
            $this->get('/{customer}/invite', [
                'uses' => 'AdwordsController@sendInvitation',
                'as' => 'customers.invite',
                'shield' => 'customers.index'
            ]);
            $this->get('/{customer}/inactivate', [
                'uses' => 'AdwordsController@inactivateLink',
                'as' => 'customers.inactivate',
                'shield' => 'customers.index'
            ]);

            $this->get('/{customer}/mutateManager/{oldManager}/{newManager}', [
                'uses' => 'AdwordsController@mutateManager',
                'as' => 'customers.mutate',
                'shield' => 'customers.index'
            ]);

            $this->get('/{customer}/mutateManager/{oldManager}', [
                'uses' => 'AdwordsController@makeUsManager',
                'as' => 'customers.makeUsManager',
                'shield' => 'customers.index'
            ]);
        });

        $this->group(['prefix' => '/hierarchies'], function () {
            $this->get('/{customer}', [
                'uses' => 'AdwordsController@hierarchies',
                'as' => 'hierarchies.hierarchies',
                'shield' => 'accounts.index'
            ]);
        });

        $this->group(['prefix' => '/campaigns'], function () {
            $this->get('/{customer}', [
                'uses' => 'AdwordsController@campaigns',
                'as' => 'campaigns.index',
                'shield' => 'accounts.index'
            ]);
            $this->get('/{customer}/performance', [
                'uses' => 'AdwordsController@campaignPerformance',
                'as' => 'campaigns.performance',
                'shield' => 'accounts.index'
            ]);
            $this->get('/{customer}/clicks', [
                'uses' => 'AdwordsController@clicksPerformance',
                'as' => 'campaigns.clicks',
                'shield' => 'accounts.index'
            ]);
            $this->get('/{customer}/history', [
                'uses' => 'AdwordsController@history',
                'as' => 'campaigns.history',
                'shield' => 'accounts.index'
            ]);
            $this->get('/{customer}/performance/{campaignid}', [
                'uses' => 'AdwordsController@history',
                'as' => 'campaigns.history',
                'shield' => 'accounts.index'
            ]);
        });

        $this->group(['prefix' => '/reports'], function () {
            $this->get('/overview', [
                'uses' => 'ReportsController@overview',
                'as' => 'reports.overview',
                'shield' => 'reports.overview'
            ]);

            $this->get('/daily-clicks', [
                'uses' => 'ReportsController@dailyClicks',
                'as' => 'reports.overview',
                'shield' => 'reports.overview'
            ]);

            $this->get('/daily-cost', [
                'uses' => 'ReportsController@dailyCost',
                'as' => 'reports.overview',
                'shield' => 'reports.overview'
            ]);

            $this->get('/daily-cpc', [
                'uses' => 'ReportsController@dailyCpc',
                'as' => 'reports.overview',
                'shield' => 'reports.overview'
            ]);

            $this->get('/daily-ctr', [
                'uses' => 'ReportsController@dailyCtr',
                'as' => 'reports.overview',
                'shield' => 'reports.overview'
            ]);

            $this->get('/daily-impressions', [
                'uses' => 'ReportsController@dailyImpressions',
                'as' => 'reports.overview',
                'shield' => 'reports.overview'
            ]);

            $this->get('/daily-position', [
                'uses' => 'ReportsController@dailyPosition',
                'as' => 'reports.overview',
                'shield' => 'reports.overview'
            ]);

            $this->get('/top-campaigns', [
                'uses' => 'ReportsController@topCampaigns',
                'as' => 'reports.overview',
                'shield' => 'reports.overview'
            ]);

            $this->get('/top-ads', [
                'uses' => 'ReportsController@topAds',
                'as' => 'reports.overview',
                'shield' => 'reports.overview'
            ]);

            $this->get('/top-keywords', [
                'uses' => 'ReportsController@topKeywords',
                'as' => 'reports.overview',
                'shield' => 'reports.overview'
            ]);

            $this->get('/top-devices', [
                'uses' => 'ReportsController@topDevices',
                'as' => 'reports.overview',
                'shield' => 'reports.overview'
            ]);
        });

        /** DESKFULL ROUTE
         *
         *  Não adicionar o o  'middleware' => 'schema.switch' pois A EQUIPE precisa está
         * no public
         */
        $this->group(['prefix' => 'deskfull-user-team'], function () {

            Route::post('/store', [
                'uses' => 'UserTeamController@store',
                'as' => 'deskfullUserTeam.store',
                'shield' => 'deskfullUserTeam.store'
            ]);

            Route::post('/remove-member', [
                'uses' => 'UserTeamController@removerMember',
                'as' => 'deskfullUserTeam.removerMember',
                'shield' => 'deskfullUserTeam.removerMember'
            ]);

            Route::get('/list/{user_id}', [
                'uses' => 'UserTeamController@listUserTeam',
                'as' => 'deskfullUserTeam.listUserTeam',
                'shield' => 'deskfullUserTeam.listUserTeam'
            ]);

        });

        $this->group(['prefix' => 'deskfull-clients'], function () {

            $this->get('/client-user-team/{user_id}', [
                'uses' => 'DeskFullClientController@listUserTeam',
                'as' => 'deskfullclients.listUserTeam',
                'shield' => 'deskfullclients.listUserTeam'
            ]);
        });

        $this->group(['prefix' => 'deskfull-clients', 'middleware' => 'schema.switch'], function () {
            $this->get('/', [
                'uses' => 'DeskFullClientController@index',
                'as' => 'reports.overview',
                'shield' => 'reports.overview'
            ]);

            $this->get('/{id}', [
                'uses' => 'DeskFullClientController@show',
                'as' => 'deskfullclients.show',
                'shield' => 'deskfullclients.show'
            ]);

            $this->get('/{userId}/{deskfull}/customers', [
                'uses' => 'DeskFullClientController@customers',
                'as' => 'reports.overview',
                'shield' => 'reports.overview'
            ]);

            $this->get('/client/{clientId}', [
                'uses' => 'DeskFullClientController@find',
                'as' => 'deskfullclients.find',
                'shield' => 'deskfullclients.find'
            ]);

            $this->post('/client-add', [
                'uses' => 'DeskFullClientController@store',
                'as' => 'deskfullclients.store',
                'shield' => 'deskfullclients.store'
            ]);

            $this->get('/client-list/{user_id}', [
                'uses' => 'DeskFullClientController@list',
                'as' => 'deskfullclients.list',
                'shield' => 'deskfullclients.list'
            ]);

            $this->post('/client-remove', [
                'uses' => 'DeskFullClientController@delete',
                'as' => 'deskfullclients.delete',
                'shield' => 'deskfullclients.delete'
            ]);
        });
        /**
         * END ROUTE DESKFULL
         */

    }
);
