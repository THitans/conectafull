<?php

namespace DeskFull\Model;

use App\Model\BaseModel;
use Illuminate\Database\Eloquent\Model;

class DeskFullClient extends BaseModel
{


    protected $table = 'deskfull_client';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'nome',
        'email',
        'site',
        'user_id',
    ];

    protected $dates = ['deleted_at'];


    static function finAllUserTeams($id)
    {
        return self::join('deskful_client_has_conta as dfc', 'deskfull_client.id', '=', 'dfc.deskfull_client_id')
                ->where('user_id', '=', $id)
                ->select('deskfull_client.*');
    }

    public function customers()
     {
        return $this->belongsToMany(Customer::class,"deskful_client_has_conta","deskfull_client_id","customers_id")
            ->withTimestamps();
    }

    public function userTeams()
    {
        return $this->belongsToMany( UserTeam::class,"user_team_has_deskfull_client","deskfull_client_id","user_team_id")
            ->withTimestamps();
    }

}
