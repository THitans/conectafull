<?php

namespace DeskFull\Model;

use App\Model\BaseModel;

class Customer extends BaseModel
{
    protected $table = 'public.customers';

    protected $guarded = ['id'];
    protected $dates = ['deleted_at'];

    public function account()
    {
        return $this->belongsTo('DeskFull\Model\Account');
    }

}