<?php

namespace DeskFull\Model;

use ACL\Model\User;
use App\Model\BaseModel;

class UserTeam extends BaseModel
{

    protected $table = 'public.user_team';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'user_id_member',
        'status',
    ];

    public function user()
    {
        return $this->hasMany(User::class, 'id', 'user_id');
    }

    public function userTeams()
    {
        return $this->hasMany(User::class, 'id', 'user_id_member') ;
    }
}
