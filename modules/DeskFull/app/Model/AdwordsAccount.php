<?php

namespace DeskFull\Model;

use App\Services\AdwordsService;
use Google\AdsApi\AdWords\v201809\cm\ApiException;
use Google\Auth\OAuth2;

class AdwordsAccount extends Account
{
    protected $table = 'accounts';

    public function fetchCustomers()
    {
        $customers = AdwordsService::getCustomers($this);
        $dbCustomers = [];
        $customerIds = [];
        foreach($customers as $customer) {
            $newCustomer = $this->addCustomer($customer);
            if(!in_array($customer->getCustomerId(), $customerIds)) {
                $dbCustomers[]= $newCustomer;
                $customerIds[] = $customer->getCustomerId();
            }
            if($customer->getCanManageClients()) {
                $managedCustomers = AdwordsService::getHierarchy($newCustomer);
                foreach($managedCustomers as $managedCustomer) {
                    $newManagedCustomer = $this->addCustomer($managedCustomer, true);
                    if(!in_array($managedCustomer->getCustomerId(), $customerIds)) {
                        $dbCustomers[] = $newManagedCustomer;
                        $customerIds[] = $managedCustomer->getCustomerId();
                    }
                }
            }
        }
        return $dbCustomers;
    }
    public function addCustomer($customer, $managedCustomer = false)
    {
        $ct = Customer::where('customerId', '=', $customer->getCustomerId())->where('account_id', '=', $this->id)->first();
        // Customer e ManagedCustomer possuem metodos diferentes para pegar o name
        $name = $managedCustomer ? $customer->getName() : $customer->getDescriptiveName();
        if(empty($ct)) {
            $customerObj = new Customer();
            $customerObj->customerId = $customer->getCustomerId();
            $customerObj->account_id = $this->id;
            $customerObj->descriptiveName = $name;
            $customerObj->canManageClients = $customer->getCanManageClients();
            $customerObj->testAccount = $customer->getTestAccount();

            $customerObj->save();
            return $customerObj;
        } else {
            $ct->update([
                'descriptiveName' => $name,
                'canManageClients' => $customer->getCanManageClients(),
                'testAccount' =>  $customer->getTestAccount()
            ]);
            return $ct;
        }
    }
    public function create(OAuth2 $oauth2, $authcode)
    {
        $oauth2->setCode($authcode);
        $oauth2->fetchAuthToken();
        $refresh_token = $oauth2->getRefreshToken();

        $idtoken = $oauth2->getIdToken();

        if(!is_null($idtoken)) {
            // Pega os dados do payload do token
            $partesToken = explode('.', $idtoken);
            $payload = json_decode(base64_decode($partesToken[1]), true);
            $name = $payload['name'];
            $email = $payload['email'];

            $this->name = $name;
            $this->email = $email;
            $this->socialnetwork = "Adwords";
            $this->status = Account::AUTHENTICATED_STATUS;
            $this->refresh_token =  $refresh_token;

            $this->save();
            try {
                $this->fetchCustomers();
                return ['status'=>true];
            } catch (ApiException $e) {
                $this->delete();
                $error = $e->getErrors()[0];
                $reason = $error->getReason();
                return ['status'=>false, 'reason'=> $reason];
            }
        }
        return ['status'=>false, 'reason'=> "NO_ID_TOKEN"];
    }
}