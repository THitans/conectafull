<?php

namespace DeskFull\Model;

use App\Model\BaseModel;

class Account extends BaseModel
{
    public const PENDING_STATUS = 'pending_auth';
    public const AUTHENTICATED_STATUS = 'authenticated';

    protected $guarded = ['id'];
    protected $hidden = ['refresh_token'];

    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('ACL\Model\User');
    }

    public function customers()
    {
        return $this->hasMany('DeskFull\Model\Customer');
    }

    public function cascadeDelete()
    {
        if ($this->delete()) {
            foreach ($this->customers as $customer) {
                $customer->delete();
            }
            return true;
        }
        return false;
    }

    public static function fromUuid($uuid)
    {
        return self::where('uuid', $uuid)->first();
    }

}
