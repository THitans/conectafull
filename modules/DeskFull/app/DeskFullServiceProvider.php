<?php

namespace DeskFull;

use DeskFull\Model\UserTeam;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class DeskFullServiceProvider extends ServiceProvider
{

    protected $namespace = 'DeskFull\Http\Controllers';

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();



        // registrar View
        //\View::addLocation(deskfull_path('resources/views'));

        // Registra pasta de Migrations
        //$this->loadMigrationsFrom(deskfull_path('database/migrations'));
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapWebRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
      //  \Route::middleware('api')
        //    ->namespace($this->namespace)
          //  ->group(deskfull_path('routes/deskfull.php'));
        \Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace)
            ->group(deskfull_path('routes/deskfull.php'));
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // Models
        //$this->app->bind('deskfull.model.userTeam', UserTeam::class);

    }
}
