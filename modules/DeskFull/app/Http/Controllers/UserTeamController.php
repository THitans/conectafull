<?php

namespace DeskFull\Http\Controllers;


use App\Http\Controllers\Auth\APIController;
use App\Http\Repositories\UserTeamRepositoryEloquent;
use Illuminate\Http\Request;

class UserTeamController extends APIController
{
    private $userTeamRepository;

    /**
     * UserTeamController constructor.
     */
    public function __construct(UserTeamRepositoryEloquent $teamRepository)
    {
        $this->userTeamRepository = $teamRepository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

    }

    /**
     * Remover Member Team
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function removerMember(Request $request)
    {
        try {

            $retorno = $this->userTeamRepository->removerMember($request);

            return $this->respond([
                'data' => $retorno
            ]);

        } catch (\Exception $exception) {

            return $this->respondWithError($exception->getMessage(), 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return response()->json(['true']);
        try {

            $retorno = $this->userTeamRepository->store($request);

            return $this->respond([
                'data' => $retorno
            ]);

        } catch (\Exception $exception) {

            return $this->respondWithError($exception->getMessage(), 500);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DeskFullModelAccount $deskFullModelAccount
     * @return \Illuminate\Http\Response
     */
    public function show(DeskFullModelAccount $deskFullModelAccount)
    {
        //
    }

    public function listUserTeam(Request $request, $userId)
    {
        try {

            $retorno = $this->userTeamRepository->listUserTeam($request);

            return $this->respond([
                'data' => $retorno
            ]);

        } catch (\Exception $exception) {

            return $this->respondWithError($exception->getMessage(), 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DeskFullModelAccount $deskFullModelAccount
     * @return \Illuminate\Http\Response
     */
    public function edit(DeskFullModelAccount $deskFullModelAccount)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\DeskFullModelAccount $deskFullModelAccount
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DeskFullModelAccount $deskFullModelAccount)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DeskFullModelAccount $deskFullModelAccount
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeskFullModelAccount $deskFullModelAccount)
    {
        //
    }
}
