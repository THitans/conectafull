<?php

namespace DeskFull\Http\Controllers;

use ACL\Model\User;
use App\Http\Controllers\Auth\APIController;
use App\Http\Requests\CreateFileRequest;
use App\Model\UploadHelper;
use DeskFull\Model\DeskFullClient;
use DeskFull\Model\UserTeam;
use Illuminate\Http\Request;
use App\Http\Repositories\DeskFullClientRepositoryEloquent;
use App\Http\Repositories\UserTeamRepositoryEloquent;

class DeskFullClientController extends APIController
{
    use UploadHelper;

    private $clientRepositoryEloquent;

    public function __construct(DeskFullClientRepositoryEloquent $clientRepositoryEloquent)
    {
        $this->clientRepositoryEloquent = $clientRepositoryEloquent;
    }

    public function listUserTeam(Request $request, UserTeamRepositoryEloquent $teamRepositoryEloquent)
    {

        return $teamRepositoryEloquent->listUserTeam($request);
    }

    public function store(Request $request)
    {
        //$request = ['nome'=>'Conecta manager','email'=>'email@gmail.com','site'=>'www.site.com.br'];
        if (isset($request->id)) {
            $retorno = $this->clientRepositoryEloquent->update($request);
        } else {
            $retorno = $this->clientRepositoryEloquent->store($request);
        }

        return $this->respond([
            'data' => $retorno
        ]);
    }

    public function uploadLogo(CreateFileRequest $request, $clienteId)
    {
        try {
            \DB::beginTransaction();

            $this->storeFile($request->file('file'))
                ->fillMultPart()
                ->saveFile();

            $cliente = DeskFullClient::find($clienteId);
            $cliente->logo_id = $this->file->id;
            $cliente->save();

            \DB::commit();

            return response()->json([
                'success' => true,
                'file' => $this->file,
            ], 200);
        } catch (\Exception $exception) {
            report($exception);
            \DB::rollBack();

            return response()->json([
                'success' => false,
                'message' => $exception->getMessage()
            ], 500);
        }
    }

    public function find(Request $request)
    {
        $retorno = $this->clientRepositoryEloquent->find($request);

        return $this->respond([
            'data' => $retorno
        ]);
    }

    public function list(Request $request)
    {
        return $this->clientRepositoryEloquent->list($request);
    }

    public function delete(Request $request)
    {
        return $this->clientRepositoryEloquent->delete($request);
    }

    public function index()
    {
        $listClientes = [];

        $userTeams = UserTeam::where('user_id_member', '=', auth('api')->user()->id)->get();

        /**
         * Lista todos os clientes das equipes que ele participa conforme os schema dos clientes
         *
         */
        foreach ($userTeams as $userT) {

            $user = User::find($userT->user_id);
            if($user->schema){
                $listC = $this->clientRepositoryEloquent->listTeamClientUser($user);

                if (count($listC) > 0) {

                    array_push($listClientes, $listC[0]);
                }
            }
        }
        /**
         * Se o cliente conter um schema então pesquisa no schema do mesmo
         */

        if (isset(auth('api')->user()->schema)) {
            $listMinha = $this->clientRepositoryEloquent->listTeam();

            if (count($listMinha) > 0)
                foreach ($listMinha as $meEquipe) {

                    array_push($listClientes, $meEquipe);
                }
        }
        return $listClientes;
    }

    public function show($id)
    {
        return DeskFullClient::findOrFail($id);
    }

    public function customers($userId, $deskfull)
    {
        $user = User::find($userId);

        if (!isset($user->schema)) {

            $deskFullClient = DeskFullClient::findOrFail($deskfull);
            $customers = [];
            if ($deskFullClient) {
                $allCustomers = $deskFullClient->customers;
                foreach($allCustomers as $customer) {
                    if($customer->is_active) {
                        $customers[] = $customer;
                    }
                }
            }

            return $customers;
        } else {
            $deskFullClient = $this->clientRepositoryEloquent->listTeamClientUserDeskfullId($user, $deskfull);
        }
        return $deskFullClient;
    }

}