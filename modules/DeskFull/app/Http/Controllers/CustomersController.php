<?php


namespace DeskFull\Http\Controllers;


use App\Http\Controllers\Controller;
use DeskFull\Model\Account;
use DeskFull\Model\Customer;
use Illuminate\Http\Request;

class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(Customer::all());
    }

    public function userCustomers()
    {
        $accounts = auth('api')->user()->accounts;

        $customers = [];
        foreach($accounts as $account) {
            foreach($account->customers as $customer) {
                $customers[] = $customer;
            }
        }
        return $customers;
    }

    public function toggleStatus(Customer $customer) {
        $customer->is_active = !$customer->is_active;
        if($customer->save()) {
            // envia email avisando ao cliente
            return response(['status'=>true]);
        }
        return response(['status'=>false, 'message'=>'Não foi possível alterar a situação do cliente.']);
    }


    public function productionCustomers()
    {
        $authenticated = [];

        if(auth('api')->user()) {
            $accounts = auth('api')->user()->accounts;
            foreach($accounts as $account) {
                if($account->status === Account::AUTHENTICATED_STATUS) {
                    $authenticated[] = $account;
                }
            }
        }

        $customers = [];

        foreach($authenticated as $account) {
            foreach($account->customers as $customer) {
                if(!$customer->testAccount)
                    $customers[] = $customer;
            }
        }

        return $customers;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customerid = request('customerid');

        $result = Customer::where('customerId', '=', $customerid)->first();

        return $result;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        if($customer->delete()) {
            return response('true', 200);
        }
        return response('false', 500 );
    }
}
