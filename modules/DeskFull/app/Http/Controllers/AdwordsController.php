<?php

namespace DeskFull\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\AdwordsService;
use Carbon\Exceptions\InvalidDateException;
use DeskFull\Model\Account;
use DeskFull\Model\AdwordsAccount;
use DeskFull\Model\Customer;
use Illuminate\Http\Request;

use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

class AdwordsController extends Controller
{
    const REDIRECT_URL = '/default/deskfull-accounts';

    public function callback(Request $request)
    {
        $baseurl = config('url') . self::REDIRECT_URL;
        if(request('error')) {
            return redirect()->to($baseurl . "?error=ACCESS_DENIED");
        }
        $payload = $request->all();
        $uuid = $payload['state'];

        $oauth2 = AdwordsService::makeAuth();

        $adwords = AdwordsAccount::fromUuid($uuid);
        if(empty($adwords)) {
            return redirect()->to($baseurl . "?error=USER_NOT_FOUND");
        }
        if(!isset($payload['code'])) {
            return redirect()->to($baseurl . "?error=AUTHENTICATION_FAILED");
        }
        $result = $adwords->create($oauth2, $payload['code']);
        if($result['status']) {
            return redirect()->to($baseurl);
        } else {
            return redirect()->to($baseurl . "?error=" . $result['reason']);
        }
    }

    public function oauth2(){
        $oauth2 = AdwordsService::makeAuth();
        $uuid = Str::uuid();
        $oauth2->setState($uuid);
        $adwords = new AdwordsAccount();
        $adwords->user_id = auth('api')->user()->id;
        $adwords->uuid = $uuid;
        $adwords->status = Account::PENDING_STATUS;
        $adwords->save();

        return $oauth2->buildFullAuthorizationUri(['prompt' => 'consent']);
    }

    public function sendInvitation(Customer $customer)
    {
        return AdwordsService::sendInvitation($customer);
    }

    public function pendingInvitations()
    {
        $result = AdwordsService::pendingInvitations();

        $authenticated = [];

        if(auth('api')->user()) {
            $accounts = auth('api')->user()->accounts;
            foreach($accounts as $account) {
                if($account->status === Account::AUTHENTICATED_STATUS) {
                    $authenticated[] = $account;
                }
            }
        }

        $customersIds = [];

        foreach($authenticated as $account) {
            foreach($account->customers as $customer) {
                $customersIds[] = $customer->customerId;
            }
        }

        $invitations = [];
        if($result['success']) {
            foreach($result['result'] as $invitation) {
                $client = $invitation['client'];
                if(in_array($client['customerId'], $customersIds)) {
                    $creationDate = $invitation['creationDate'];
                    $creationDate = subStr($creationDate, 0, 8);
                    try{
                        $creationDate = Carbon::createFromFormat('Ymd', $creationDate);
                        $creationDate = $creationDate->format('d/m/Y');
                    }catch (\Exception $e) {
                        $creationDate = '';
                    }

                    $expirationDate = $invitation['expirationDate'];
                    $expirationDate = subStr($expirationDate, 0, 8);
                    try{
                        $expirationDate = Carbon::createFromFormat('Ymd', $expirationDate);
                        $expirationDate = $expirationDate->format('d/m/Y');
                    } catch (\Exception $e) {
                        $expirationDate = '';
                    }

                    $invitations[] = [
                        'customerId' => $client['customerId'],
                        'name' => $client['name'],
                        'creationDate' => $creationDate,
                        'expirationDate' => $expirationDate,
                    ];
                }
            }
            $result['result'] = $invitations;
        }

        return $result;
    }
    public function inactivateLink(Customer $customer)
    {
        return AdwordsService::inactivateLink($customer);
    }
    public function makeUsManager(Customer $customer, $oldManager)
    {
        return AdwordsService::mutateManager($customer, $oldManager);
    }
    public function mutateManager(Customer $customer, $oldManager, $newManager)
    {
        return AdwordsService::mutateManager($customer, $oldManager, $newManager);
    }
    public function campaigns(Customer $customer)
    {
        $campaigns = AdwordsService::getCampaigns($customer);
        return $campaigns;
    }

    public function hierarchies(Customer $customer)
    {
        return AdWordsService::getHierarchy($customer, true);
    }

    public function campaignPerformance(Customer $customer)
    {
        $params = request()->all();
        return AdwordsService::getCampaignPerformance(AdwordsService::makeSession($customer->account, $customer->customerId), $params);
    }

    public function history(Customer $customer)
    {
        $params = request()->all();
        return AdwordsService::getPerformanceHistory(AdwordsService::makeSession($customer->account, $customer->customerId), $params);
    }

    public function clicksPerformance(Customer $customer)
    {
        $params = request()->all();
        return AdwordsService::getClicksPerformance(AdwordsService::makeSession($customer->account, $customer->customerId), $params);
    }

    public function dailyCost(Customer $customer)
    {
        $params = request()->all();
        return AdwordsService::getClicksPerformance(AdwordsService::makeSession($customer->account, $customer->customerId), $params);
    }

    public function customers(AdwordsAccount $account, Customer $customer)
    {
        return AdwordsService::getCustomers($account, $customer);
    }
    public function listCustomers(AdwordsAccount $account)
    {
        return $account->customers;
    }

}
