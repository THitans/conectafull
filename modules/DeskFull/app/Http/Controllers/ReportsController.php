<?php

namespace DeskFull\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Services\AdwordsService;
use Carbon\Carbon;
use DeskFull\Model\Customer;

class ReportsController extends Controller
{
    public function overview()
    {
        $params = request()->all();
        if(isset($params['CustomerId'])){
            if(isset($params['CampaignId'])) unset($params['CampaignId']);
            $customer = Customer::find($params['CustomerId']);
            $session = AdwordsService::makeSession($customer->account, $customer->customerId);
            $result = AdwordsService::getAccountPerformance($session, $params);
            if($result['success']){
                $result = $result['result']['datasets'][0];
                $result['ctr'] = str_replace('%','',$result['ctr']);
                return ['success'=>true, 'result'=>$result];
            }
        }
        return ['success'=>false, 'message'=>'erro'];
    }

    public function dailyClicks()
    {
        $params = request()->all();
        if(isset($params['CustomerId'])){
            $customer = Customer::find($params['CustomerId']);
            $session = AdwordsService::makeSession($customer->account, $customer->customerId);
            $campaign = 0;
            if(isset($params['CampaignId'])) $campaign = $params['CampaignId'];
            if(is_null($campaign) OR $campaign == 'null' OR $campaign == 0) {
                unset($params['CampaignId']);
                $results = AdwordsService::getAccountPerformance($session, $params, ['Date', 'Clicks']);
            } else {
                $results = AdwordsService::getPerformanceHistory($session, $params);
            }
        } else {
            $results = ['status'=>'false', 'error'=>'NO_CUSTOMER_ID'];
        }

        $labels = [];
        $data = [];
        if($results['success']) {
            $results = $results['result'];
            foreach($results['datasets'] as $item) {
                $labels[] = $item['day'];
                $data[] = $item['clicks'];
            }
        }
        array_multisort($labels, $data);
        foreach($labels as &$day) {
            $date = Carbon::createFromFormat('Y-m-d', $day);
            $day = $date->format('d/m/Y');
        }
        $datasets[0] = [
            "label" => "Cliques",
            "backgroundColor" => "#E98F24",
            "data" => $data
        ];
        return [
            "labels" => $labels,
            "datasets" => $datasets
        ];
    }
    public function dailyCost()
    {
        $params = request()->all();
        if(isset($params['CustomerId'])){
            if(isset($params['CampaignId'])) unset($params['CampaignId']);
            $customer = Customer::find($params['CustomerId']);
            $session = AdwordsService::makeSession($customer->account, $customer->customerId);
            $results = AdwordsService::getAccountPerformance($session, $params, ['Date', 'Cost']);
        }
        $labels = [];
        $data = [];
        $success = false;
        if($results['success']) {
            $success = true;
            $results = $results['result'];
            foreach($results['datasets'] as $item) {
                $labels[] = $item['day'];
                $data[] = $item['cost']/1000000;
            }
        }

        array_multisort($labels, $data);
        foreach($labels as &$day) {
            $date = Carbon::createFromFormat('Y-m-d', $day);
            $day = $date->format('d/m/Y');
        }
        $datasets[0] = [
            "label" => "Custo",
            "backgroundColor" => "#E98F24",
            "data" => $data
        ];
        return [
            "success" => $success,
            "labels" => $labels,
            "datasets" => $datasets
        ];
    }
    public function dailyCpc()
    {
        $params = request()->all();
        if(isset($params['CustomerId'])){
            if(isset($params['CampaignId'])) unset($params['CampaignId']);
            $customer = Customer::find($params['CustomerId']);
            $session = AdwordsService::makeSession($customer->account, $customer->customerId);
            $results = AdwordsService::getAccountPerformance($session, $params, ['Date', 'AverageCpc']);
        }
        $labels = [];
        $data = [];
        $success = false;
        if($results['success']) {
            $success = true;
            $results = $results['result'];
            foreach($results['datasets'] as $item) {
                $labels[] = $item['day'];
                $data[] = $item['avgCPC']/1000000;
            }
        }
        array_multisort($labels, $data);
        foreach($labels as &$day) {
            $date = Carbon::createFromFormat('Y-m-d', $day);
            $day = $date->format('d/m/Y');
        }
        $datasets[0] = [
            "label" => "CPC",
            "backgroundColor" => "#E98F24",
            "data" => $data
        ];
        return [
            "success" => $success,
            "labels" => $labels,
            "datasets" => $datasets
        ];
    }
    public function dailyCtr()
    {
        $params = request()->all();
        if(isset($params['CustomerId'])){
            if(isset($params['CampaignId'])) unset($params['CampaignId']);
            $customer = Customer::find($params['CustomerId']);
            $session = AdwordsService::makeSession($customer->account, $customer->customerId);
            $results = AdwordsService::getAccountPerformance($session, $params, ['Date', 'Ctr']);
        }
        $labels = [];
        $data = [];
        $success = false;
        if($results['success']) {
            $success = true;
            $results = $results['result'];
            foreach($results['datasets'] as $item) {
                $labels[] = $item['day'];
                $data[] = str_replace('%', '', $item['ctr']);
            }
        }

        array_multisort($labels, $data);
        foreach($labels as &$day) {
            $date = Carbon::createFromFormat('Y-m-d', $day);
            $day = $date->format('d/m/Y');
        }
        $datasets[0] = [
            "label" => "CTR",
            "backgroundColor" => "#E98F24",
            "data" => $data
        ];
        return [
            "success" => $success,
            "labels" => $labels,
            "datasets" => $datasets
        ];
    }
    public function dailyImpressions()
    {
        $params = request()->all();
        if(isset($params['CustomerId'])){
            if(isset($params['CampaignId'])) unset($params['CampaignId']);
            $customer = Customer::find($params['CustomerId']);
            $session = AdwordsService::makeSession($customer->account, $customer->customerId);
            $results = AdwordsService::getAccountPerformance($session, $params, ['Date', 'Impressions']);
        }
        $labels = [];
        $data = [];
        $success = false;
        if($results['success']) {
            $success = true;
            $results = $results['result'];
            foreach($results['datasets'] as $item) {
                $labels[] = $item['day'];
                $data[] = $item['impressions'];
            }
        }

        array_multisort($labels, $data);
        foreach($labels as &$day) {
            $date = Carbon::createFromFormat('Y-m-d', $day);
            $day = $date->format('d/m/Y');
        }
        $datasets[0] = [
            "label" => "Impressões",
            "backgroundColor" => "#E98F24",
            "data" => $data
        ];
        return [
            "success" => $success,
            "labels" => $labels,
            "datasets" => $datasets
        ];
    }

    public function dailyPosition()
    {
        $params = request()->all();
        if(isset($params['CustomerId'])){
            if(isset($params['CampaignId'])) unset($params['CampaignId']);
            $customer = Customer::find($params['CustomerId']);
            $session = AdwordsService::makeSession($customer->account, $customer->customerId);
            $results = AdwordsService::getAccountPerformance($session, $params, ['Date', 'AveragePosition']);
        }
        $labels = [];
        $data = [];
        $success = false;
        if($results['success']) {
            $success = true;
            $results = $results['result'];
            foreach($results['datasets'] as $item) {
                $labels[] = $item['day'];
                $data[] = $item['avgPosition'];
            }
        }

        array_multisort($labels, $data);
        foreach($labels as &$day) {
            $date = Carbon::createFromFormat('Y-m-d', $day);
            $day = $date->format('d/m/Y');
        }
        $datasets[0] = [
            "label" => "Posição diária",
            "backgroundColor" => "#E98F24",
            "data" => $data
        ];
        return [
            "success" => $success,
            "labels" => $labels,
            "datasets" => $datasets
        ];
    }

    public function topCampaigns()
    {
        $fields = [
            'CampaignName',
            'Clicks',
            'ConversionRate',
            'Conversions',
            'CostPerConversion',
            'Cost'
        ];

        $params = request()->all();
        if(isset($params['CustomerId'])){
            $customer = Customer::find($params['CustomerId']);
            $campaign = 0;
            if(isset($params['CampaignId'])) $campaign = $params['CampaignId'];
            if(is_null($campaign) OR $campaign == 'null' OR $campaign == 0) {
                unset($params['CampaignId']);
            }
            $session = AdwordsService::makeSession($customer->account, $customer->customerId);
            $results = AdwordsService::getCampaignPerformance($session, $params, $fields);

        }
        $headers = [
            'campaign'=>'Nome da campanha',
            'clicks'=>'Cliques',
            'convRate'=>'Taxa de conversão',
            'conversions'=>'Conversões',
            'costConv'=>'Custo por Conversão',
            'cost'=>'Custo Total'
        ];

        $formattedHeaders = [];
        foreach($headers as $k=>$header) {
            $formattedHeaders[] = ["text" => $header, "sortable" => true, "value"=>$k];
        }
        if($results['success']) {
            $data = $results['result']['datasets'];
        }

        return ['success'=>true,'headers'=>$formattedHeaders, 'data' => $data];
    }

    public function topAds()
    {
        $fields = [
            'Description',
            'CampaignName',
            'Impressions',
            'Ctr',
            'Clicks',
            'AveragePosition',
            'Conversions',
            'ConversionRate',
            'CostPerConversion',
            'Cost',
        ];
        // CPA ?
        $params = request()->all();
        if(isset($params['CustomerId'])){
            $customer = Customer::find($params['CustomerId']);
            $campaign = 0;
            if(isset($params['CampaignId'])) $campaign = $params['CampaignId'];
            if(is_null($campaign) OR $campaign == 'null' OR $campaign == 0) {
                unset($params['CampaignId']);
            }
            $session = AdwordsService::makeSession($customer->account, $customer->customerId);
            $results = AdwordsService::getAdPerformance($session, $params, $fields);

        }
        $headers = [
            'description'=>'Nome do anúncio',
            'campaign'=>'Nome da campanha',
            'impressions'=>'Impressões',
            'ctr'=>'CTR (%)',
            'clicks'=>'Cliques',
            'avgPosition'=>'Posição Média',
            'conversions'=>'Total de Conversões',
            'convRate'=>'Taxa de Conversão (%)',
            'costConv'=>'Custo por conversão',
            'cost'=>'Custo Total'
        ];
        $formattedHeaders = [];
        foreach($headers as $k=>$header) {
            $formattedHeaders[] = ["text" => $header, "sortable" => true, "value"=>$k];
        }

        if($results['success']) {
            $data = $results['result']['datasets'];
        }

        return ['success'=>true,'headers'=>$formattedHeaders, 'data' => $data];
    }

    public function topKeywords()
    {
        $fields = [
            'Query',
            'KeywordTextMatchingQuery',
            'CampaignName',
            'Clicks',
            'ConversionRate',
            'Conversions',
            'CostPerConversion',
            'Cost'
        ];

        $params = request()->all();
        if(isset($params['CustomerId'])){
            $customer = Customer::find($params['CustomerId']);
            $campaign = 0;
            if(isset($campaign['CampaignId'])) $campaign = $params['CampaignId'];
            if(is_null($campaign) OR $campaign == 'null') {
                unset($params['CampaignId']);
            }
            $session = AdwordsService::makeSession($customer->account, $customer->customerId);
            $results = AdwordsService::getKeywordsPerformance($session, $params, $fields);

        }
        $headers = [
            'query'=>'Termos da Busca',
            'keyword'=>'Palavra-chave',
            'campaign'=>'Nome da campanha',
            'clicks'=>'Cliques',
            'convRate'=>'Taxa de conversão',
            'conversions'=>'Conversões',
            'costConv'=>'Custo por Conversão',
            'cost'=>'Custo Total'
        ];

        $formattedHeaders = [];
        foreach($headers as $k=>$header) {
            $formattedHeaders[] = ["text" => $header, "sortable" => true, "value"=>$k];
        }
        if($results['success']) {
            $data = $results['result']['datasets'];
        }

        return ['success'=>true,'headers'=>$formattedHeaders, 'data' => $data];
    }

    public function topDevices()
    {
        $fields = [
            'Device',
            'Clicks',
            'Impressions',
            'AverageCpc',
            'Ctr',
            'Cost'
        ];

        $params = request()->all();
        if(isset($params['CustomerId'])){
            $customer = Customer::find($params['CustomerId']);
            $campaign = 0;
            if(isset($campaign['CampaignId'])) $campaign = $params['CampaignId'];
            if(is_null($campaign) OR $campaign == 'null') {
                unset($params['CampaignId']);
            }
            $session = AdwordsService::makeSession($customer->account, $customer->customerId);
            $results = AdwordsService::getAccountPerformance($session, $params, $fields);

        }
        $headers = [
            'device'=>'Tipo de dispositivo',
            'clicks'=>'Cliques',
            'impressions'=>'Impressões',
            'avgCPC'=>'CPC Médio',
            'ctr'=>'CTR(%)',
            'cost'=>'Custo (R$)'
        ];

        $formattedHeaders = [];
        foreach($headers as $k=>$header) {
            $formattedHeaders[] = ["text" => $header, "sortable" => true, "value"=>$k];
        }
        if($results['success']) {
            $data = $results['result']['datasets'];
        }

        return ['success'=>true,'headers'=>$formattedHeaders, 'data' => $data];
    }
}