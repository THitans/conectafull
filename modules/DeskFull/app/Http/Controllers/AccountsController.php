<?php

namespace DeskFull\Http\Controllers;


use App\Http\Controllers\Controller;
use DeskFull\Model\Account;
use DeskFull\Model\AdwordsAccount;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AccountsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $authenticated = [];

        if(auth('api')->user()) {
            $accounts = auth('api')->user()->accounts;
            foreach($accounts as $account) {
                if($account->status === Account::AUTHENTICATED_STATUS) {
                    $authenticated[] = $account;
                }
            }

        }
        return response($authenticated);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function manager()
    {
        return [
            'name' => config('manager_name'),
            'email' => config('manager_email'),
            'customerId' => config('client_customer_id')
        ];
    }

    public function sync(Account $account)
    {
        if($account->socialnetwork == 'Adwords') {
            $adwords = AdwordsAccount::where('id','=',$account->id)->first();
            return $adwords->fetchCustomers();
        }
        return [];
    }

    public function customers(Account $account)
    {
        return $account->customers;
    }

    public function manageAccounts()
    {
        $authenticated = [];

        if(auth('api')->user()) {
            $accounts = auth('api')->user()->accounts;
            foreach($accounts as $account) {
                if($account->status === Account::AUTHENTICATED_STATUS) {
                    $authenticated[] = $account;
                }
            }
        }

        $customers = [];

        foreach($authenticated as $account) {
            foreach($account->customers as $customer) {
                $customers[] = $customer;
            }
        }

        return $customers;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \DeskFull\Model\Account;  $account
     * @return \Illuminate\Http\Response
     */
    public function show(Account $account)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \DeskFull\Model\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function edit(Account $account)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \DeskFull\Model\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Account $account)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function destroy(Account $account)
    {
        if($account->cascadeDelete()) {
            return response('true', 200);
        }
        return response('false', 500 );
    }
}
