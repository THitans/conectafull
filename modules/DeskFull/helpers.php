<?php

if (!function_exists('deskfull_path')) {
    /**
     * Retorna o caminho para a pasta raiz de deskfull
     * @param null $path
     * @return string
     */
    function deskfull_path($path = null)
    {
        return $path ? __DIR__ . DIRECTORY_SEPARATOR . $path : __DIR__;
    }
}
