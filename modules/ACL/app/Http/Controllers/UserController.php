<?php

namespace ACL\Http\Controllers;

use ACL\Http\Requests\CreateUserRequest;
use App\Http\Requests\ForgotPasswordRequest;
use ACL\Http\Requests\UpdateUserPasswordRequest;
use ACL\Http\Requests\UpdateUserRequest;
use App\Http\Requests\CreateFileRequest;
use App\Http\Controllers\Auth\APIController;
use App\Model\UploadHelper;
use Illuminate\Http\Request;
use Pacuna\Schemas\Facades\PGSchema;

class UserController extends APIController
{
    use DatabaseUser, Schema, UploadHelper;

    public function index(Request $request)
    {
        try {
            $search = $request->input('search', '');
            $users = app('acl.service.user')->allUserCheckRoles($search)->get();

            return $this->respond([
                'data' => $users
            ]);
        } catch (\Exception $exception) {
            report($exception);

            return $this->respondWithError($exception->getMessage(), 500);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateUserRequest $request)
    {
        try {
            \DB::beginTransaction();

            $user = app('acl.model.user')->where('email', $request->email)->first();

            if (is_null($user)) {
                $user = app('acl.model.user')->fill($request->except('roles'));
                $user->status_cad = '1-Cadastrado';
                $user->password = bcrypt('123456');
                $user->save();
            }

            if ($user->type == 'Cliente') {
                $user->schema = $this->createClientDatabase($user->name);
                $user->save();
            }
            $role = \Defender::findRole('Admin');
            $user->attachRole($role);

            app('mailer.login_activation')->sendActivationMail($user);
            \DB::commit();

            // Cria as tabelas no Schema Criado se for type = Cliente
            if ($user->type == 'Cliente') {
                // https://github.com/pacuna/Laravel-PGSchema
//                define('STDIN', fopen("php://stdin", "r"));
                PGSchema::migrate($user->schema);
            }

            return $this->respond([
                'data' => $user
            ]);
        } catch (\Exception $exception) {
            report($exception);
            \DB::rollBack();

            return $this->respondWithError($exception->getMessage(), 500);
        }
    }

    public function edit($id)
    {
        try {
            $user = app('acl.model.user')->findOrFail($id);
            $roles = app('acl.service.user')->allRoleWithoutDeveloper()->get();
            $selectedRoles = app('acl.model.role')->findToSelect($user->id)->pluck('id');

            return $this->respond([
                'data' => compact('user', 'roles', 'selectedRoles')
            ]);
        } catch (\Exception $exception) {
            report($exception);
            \DB::rollBack();

            return $this->respondWithError($exception->getMessage(), 500);
        }
    }

    public function update(UpdateUserRequest $request, $id)
    {
        try {
            \DB::beginTransaction();

            $user = app('acl.model.user')->findOrFail($id);
            $user->fill($request->except('roles'));
            $user->save();

            // Se não vier Role alguma deve passar um array vazio para syncRoles()
            $roles = [];
            if (!is_null($request->roles)) {
                $roles = $request->roles;
            }
            $user->syncRoles($roles);

            $verifyToCreateSchema = $this->verifyToCreateSchema($user, $request->roles);

            if ($verifyToCreateSchema) {
                $user->schema = $this->createClientDatabase($user->name);
                $user->save();
            }

            \DB::commit();

            // Cria as tabelas no Schema Criado se for type = Cliente
            if ($verifyToCreateSchema) {
                // https://github.com/pacuna/Laravel-PGSchema
//                define('STDIN', fopen("php://stdin", "r"));
                PGSchema::migrate($user->schema);
            }

            return $this->respond([
                'data' => $user
            ]);
        } catch (\Exception $exception) {
            report($exception);
            \DB::rollBack();

            return $this->respondWithError($exception->getMessage(), 500);
        }
    }

    public function destroy($id)
    {
        try {
            User::findOrfail($id)->delete();

            return response()->json([
                'success' => true,
                'route' => route('user.index')
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'route' => route('user.index'),
                'message' => $exception->getMessage()
            ], 500);
        }
    }

    public function upload(CreateFileRequest $request)
    {
        try {
            \DB::beginTransaction();

            $this->storeFile($request->file('file'))
                ->fillMultPart()
                ->saveFile();

            $user = auth('api')->user();
            $user->image_id = $this->file->id;
            $user->save();

            \DB::commit();

            return response()->json([
                'success' => true,
                'file' => $this->file,
            ], 200);
        } catch (\Exception $exception) {
            report($exception);
            \DB::rollBack();

            return response()->json([
                'success' => false,
                'message' => $exception->getMessage()
            ], 500);
        }
    }

    private function getImageInfo($fileID)
    {
        if (is_null($fileID)) {
            return auth('api')->user()->imageInfo;
        }

        return app('model.file')->first(['path_file', 'mimetype_file']);
    }

    /*
     * Retorna a Imagem do Perfil do usuário Logado
     */
    public function profileImage(Request $request)
    {
        try {
            $imageInfo = $this->getImageInfo($request->fileID);

            if (is_null($imageInfo)) {
                return response()->json([
                    'success' => false,
                    'message' => 'Nenhuma Imagem Encontrada'
                ], 200);
            }

            $file = \Storage::get($imageInfo->path_file);

            return response()->json([
                'success' => true,
                'mime_type' => $imageInfo->mimetype_file,
                'image' => base64_encode($file),
            ], 200);
        } catch (\Exception $exception) {
            report($exception);

            return response()->json([
                'success' => false,
                'message' => $exception->getMessage()
            ], 500);
        }
    }

    private function getImageURL($fileID)
    {
        if (is_null($fileID)) {
            return auth('api')->user()->imageUrl;
        }

        // Se passar $fileID busca pelo ID
        return app('model.file')->where('id', $fileID)->first(['path_file'])['path_file'];
    }

    /*
     * Retorna a URL da Imagem do Perfil do usuário Logado
     */
    public function profileImageUrl(Request $request)
    {
        try {
            $imageUrl = $this->getImageURL($request->fileID);

            if (is_null($imageUrl)) {
                return response()->json([
                    'success' => false,
                    'message' => 'Nenhuma Imagem Encontrada'
                ], 200);
            }

            return response()->json([
                'success' => true,
                'profileImageUrl' => $imageUrl
            ], 200);
        } catch (\Exception $exception) {
            report($exception);

            return response()->json([
                'success' => false,
                'message' => $exception->getMessage()
            ], 500);
        }
    }

    public function editLogged()
    {
        if (!\auth('api')->user()->password_change_at) {
            flash('warning', 'Nota: É seu primeiro acesso, portanto, você deverá atualizar sua senha!');
        }

        return view('auth.passwords.change-password');
    }

    public function updatePassword(UpdateUserPasswordRequest $request)
    {
        try {
            $currentPassword = auth('api')->user()->getAuthPassword();

            if(! password_verify($request->currentPassword, $currentPassword)) {
                return response()->json([
                    'success' => false,
                    'errors' => ['currentPassword' => ['Essa não é sua senha atual']]
                ], 422);
            }

            $credentials = $request->only('password', 'password_confirmation');
            $user = auth('api')->user();
            $user->password = bcrypt($credentials['password']);
            $user->save();

            return $this->respond([
                'message' => 'Senha Atualizada com Sucesso'
            ]);
        } catch (\Exception $exception) {
            report($exception);

            return response()->json([
                'success' => false,
                'message' => $exception->getMessage()
            ], 500);
        }
    }

    public function sendLinkForgotPass(ForgotPasswordRequest $request)
    {
        try {
            if(is_null($user = app('acl.service.user')->existEmail($request->email))) {
                return response()->json([
                    'success' => false,
                    'errors' => ['email' => ['Este email não foi encontrado em nossos registros']]
                ], 422);
            }

            app('mailer.login_activation')->sendConfirmMailNotification($user);

            return $this->respond([
                'message' => 'Email enviado com sucesso'
            ]);
        } catch (\Exception $exception) {
            report($exception);

            return response()->json([
                'success' => false,
                'message' => $exception->getMessage()
            ], 500);
        }
    }

    public function restore($id)
    {
        try {
            User::withTrashed()->where('id', $id)->restore();
            flash('success', trans('flash.restore_sucess'));

            return redirect()->route('user.index');
        } catch (\Exception $e) {
            flash('danger', trans('flash.restore_error'), $e->getMessage());

            return \Redirect::route('user.index');
        }
    }
}
