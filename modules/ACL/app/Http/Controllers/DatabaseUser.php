<?php
/**
 * Created by PhpStorm.
 * User: default
 * Date: 31/01/19
 * Time: 21:47
 */

namespace ACL\Http\Controllers;

trait DatabaseUser
{
    /**
     * @param string $clientName
     * @return string
     */
    private function createClientDatabase(string $clientName)
    {
        $explodeName = explode(' ', trim(tirarAcentos($clientName)));
        $schemaName = '';
        foreach ($explodeName as $name) {
            $schemaName .= substr($name, 0, 2) . '_';
        }

        $schemaName = strtolower($schemaName) . uniqid();
        \DB::statement("CREATE SCHEMA {$schemaName}");

        return $schemaName;
    }
}