<?php

namespace ACL\Http\Controllers;

trait Schema
{
    private function verifyToCreateSchema($user, array $roles)
    {
        if (
            $user->type == 'Cliente' &&
            $user->schema == null &&
            in_array('2', $roles)
        ) {
            return true;
        }

        return false;
    }
}