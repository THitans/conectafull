<?php

namespace ACL\Http\Controllers;

use ACL\Http\Requests\UpdatePermissionRequest;
use ACL\Model\Permission;
use App\Http\Controllers\Auth\APIController;
use Illuminate\Http\Request;

class PermissionController extends APIController
{
    public function index()
    {
        try {
//            $permissions = app('acl.model.permission')->findAllByRoleOrUser(100, $request->role, $request->user);
            $permissions = app('acl.model.permission')->all();

            return $this->respond([
                'data' => $permissions
            ]);
        } catch (\Exception $exception) {
            report($exception);

            return $this->respondWithError($exception->getMessage(), 500);
        }
    }

    public function permissionsToRoles()
    {
        try {
            $processPermissions = Permission::get();
            if (!\Defender::hasRole(config('defender.superuser_role'))) {
                $processPermissions = auth('api')->user()->getRolesPermissions();
            }
            $permissions = [];
            foreach ($processPermissions as $permission) {
                $array = explode('.', $permission->name);
                if (isset($array[0]) && isset($array[1])) {
                    $permissions[$array[0]][$permission->id] = [
                        'name' => $permission->readable_name,
                        'class' => $array[1]
                    ];
                }
            }

            return $this->respond([
                'data' => $permissions
            ]);
        } catch (\Exception $exception) {
            report($exception);

            return $this->respondWithError($exception->getMessage(), 500);
        }
    }

    public function edit($id)
    {
        $permission = Permission::findOrfail($id);

        return view('acl.permission.edit', compact('permission'));
    }

    public function update($id, UpdatePermissionRequest $request)
    {
        Permission::findOrfail($id)->update($request->all());

        return redirect()->route('acl.permission.index', ['idRole' => 0, 'idUser' => 0]);
    }

    public function labelAutocomplete(Request $request)
    {
        return response()->json(
            Permission::select(['readable_name as title', 'name AS writer'])
                ->where('readable_name', 'ILIKE', "%{$request->get('q')}%")
                ->orderBy('readable_name')
                ->get()
        );
    }

    public function nameAutocomplete(Request $request)
    {
        return response()->json(
            Permission::select(['name as title', 'readable_name AS writer'])
                ->where('name', 'ILIKE', "%{$request->get('q')}%")
                ->orderBy('name')
                ->get()
        );
    }
}
