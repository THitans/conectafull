<?php

namespace ACL\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    public function __construct()
    {
        $cpf = preg_replace("/\D+/", "", app('request')->cpf);
        app('request')->request->set('cpf', $cpf);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:3|max:255',
            'cpf' => "nullable|string|max:11|unique:users,cpf,{$this->id}",
            'email' => "required|string|email|max:255|unique:users,email,{$this->id}",
            'type' => 'nullable|string|max:255',
            'roles' => 'required|array'
        ];
    }
}
