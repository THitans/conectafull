<?php

namespace ACL\Model;

use App\Model\File;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Mailer\EmailInterface;
use Mailer\Notification\ConfirmationMailNotification;
use OwenIt\Auditing\Contracts\Auditable;
use OwenIt\Auditing\Contracts\UserResolver;
use Artesaos\Defender\Traits\HasDefender;
use Askedio\SoftCascade\Traits\SoftCascadeTrait;
use App\Scope\SoftDeleting;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mailer\Notification\ResetPasswordNotification;
use Tymon\JWTAuth\Contracts\JWTSubject;


class User extends Authenticatable implements
    Auditable,
    UserResolver,
    JWTSubject,
    EmailInterface
{
    use Notifiable, \OwenIt\Auditing\Auditable, SoftDeletes, HasDefender, SoftCascadeTrait;

    protected $table = 'public.users';

    /**
     * The attributes that are mass assignable..
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'last_name',
        'cpf',
        'email',
        'password',
        'type',
        'schema'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = ['imageUrl'];

    public function getImageInfoAttribute()
    {
        if (! is_null($file = $this->belongsTo(File::class, 'image_id'))) {
            return $file->first(['path_file', 'mimetype_file']);
        }

        return null;
    }

    public function getImageUrlAttribute()
    {
        if (! is_null($file = $this->belongsTo(File::class, 'image_id')->first(['path_file']))) {
            return \Storage::url($file['path_file']);
        }

        return null;
    }

    /**
     * http://laravel-auditing.com/docs/5.0/general-configuration
     *
     * {@inheritdoc}
     */
    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }

    /**
     * Boot the soft deleting trait for a model.
     *
     * @return void
     */
    public static function bootSoftDeletes()
    {
        static::addGlobalScope(new SoftDeleting());
    }

    /**
     * Send the password reset notification.
     *
     * @param  string $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    /**
     * Utilizado pelo module de Mailer
     *
     * @param $token
     */
    public function sendConfirmMailNotification($token)
    {
        $this->notify(new ConfirmationMailNotification($token));
    }

    public function finAllUserWithoutDeveloper()
    {
        return self::leftJoin('role_user', 'role_user.user_id', '=', 'users.id')
            ->leftJoin('roles', 'role_user.role_id', '=', 'roles.id')
            ->where('roles.name', 'NOT LIKE', 'Desenvolvedor')
            ->orWhereNull('roles.name')
            ->select('users.*');
    }

    /**
     * Resolve the User.
     *
     * @return mixed|null
     */
    public static function resolve()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }

    public function accounts()
    {
        return $this->hasMany(\DeskFull\Model\Account::class);
    }

    public function deskfullClients()
    {
        return $this->hasMany(\DeskFull\Model\DeskFullClient::class);
    }

    /**
     * Verifica se Usuário logado é suporte
     * Perfil de suporte está no arquivo de configuração em config/audith.php
     *
     * @return bool
     */
    public function isSuporte(): bool
    {
        return \Defender::hasRole(config('defender.restore_role'));
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function getEmail()
    {
        return $this->attributes['email'];
    }
}
