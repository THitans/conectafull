<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 06/02/19
 * Time: 17:48
 */

namespace ACL\Repositories;


use Illuminate\Support\Facades\Response;

class UserRepositoryEloquent
{

    public function saveUser($request)
    {

        try {

            \DB::beginTransaction();

            $user = app('acl.model.user')->fill($request->except('roles'));

            $user->password = bcrypt('123456');
            $user->save();

        } catch (\Exception $exception) {
            report($exception);
            \DB::rollBack();

            return Response::json(array('msg' => 'Incorrect!'.$exception), 500);
        }
    }

}