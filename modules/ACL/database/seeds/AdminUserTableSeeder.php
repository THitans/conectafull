<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminUserTableSeeder extends Seeder
{
    use \ACL\Http\Controllers\DatabaseUser;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = DB::table('users')->where('email', 'admin@admin')->count();

        $suporte = \Defender::createRole(config('defender.restore_role'));
        $suporte->description = 'Perfil para Restaurar Arquivos Deletados';
        $suporte->save();

        $admin = \Defender::createRole('Admin');
        $superUserRole = \Defender::createRole(config('defender.superuser_role'));

        $equipeRole = \Defender::createRole('Membro da Equipe');

        if ($user < 1) {
            DB::table('users')->insert([
                'name' => 'Conecta',
                'email' => 'admin@admin.com',
                'cpf' => '01428452133',
                'status_cad' => '1-Cadastrado',
                'password' => bcrypt('123456'),
                'type' => 'Admin', // Admin = Usuário interno do sistema. | Cliente = Cliente da conecta é criado um schema
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ]);

            foreach (app('acl.model.user')->whereIn('email', [
                'admin@admin.com',
            ])->get() as $user) {
                $user->attachRole($superUserRole);
            }
            echo "Admin user criado!\n";
            echo "email: admin@admin.com\n";
            echo "cpf: 01428452133\n";
            echo "senha:123456\n";
        }
    }
}
