<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Generator as Faker;

class ClienteUserTableSeeder extends Seeder
{
    use \ACL\Http\Controllers\DatabaseUser;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        $admin = \Defender::findRole('Admin');

        $cliente = new \ACL\Model\User();
        $cliente->fill([
            'name' => $faker->name,
            'email' => 'cliente@admin.com',
            'cpf' => '01428452135',
            'status_cad' => '1-Cadastrado',
            'password' => bcrypt('123456'),
            'type' => 'Cliente', // Admin = Usuário interno do sistema. | Cliente = Cliente da conecta é criado um schema
        ]);
        $cliente->schema = $this->createClientDatabase($cliente->name);
        $cliente->save();
        echo "Cliente user criado!\n";
        echo "email: cliente@admin.com\n";
        echo "cpf: 01428452135\n";
        echo "senha:123456\n";

        foreach (app('acl.model.user')->whereIn('email', [
            'cliente@admin.com'
        ])->get() as $cliente) {
            $cliente->attachRole($admin);
        }
        \PGSchema::migrate($cliente->schema);
        echo "Schema: " . $cliente->schema . " Criado\n";

//
//        $clienteTe = new \ACL\Model\User();
//        $clienteTe->fill([
//            'name' => $faker->name,
//            'email' => 'userteam@cliente.com',
//            'cpf' => '00539231290',
//            'password' => bcrypt('123456'),
//            'type' => 'Cliente', // Admin = Usuário interno do sistema. | Cliente = Cliente da conecta é criado um schema
//        ]);
//        $clienteTe->schema = $this->createClientDatabase($clienteTe->name);
//
//        $clienteTe->save();
//
//        echo "Cliente do Team  Criado\n";
//
//        //$clienteTe->attachRole($equipeRole);
//
//        echo "Conta user criado!\n";
//        echo "email: cliente@admin.com\n";
//        echo "cpf: 01428452135\n";
//        echo "senha:123456\n";
    }
}
