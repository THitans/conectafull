<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('last_name')->nullable();
            $table->string('cpf')->nullable();
            $table->string('email')->unique()->index();
            $table->string('schema')->unique()->nullable();
            $table->enum('type', ['Admin', 'Cliente']);
            $table->string('password');
            $table->boolean('activated')->default(true);

            $table->enum('status_cad', ['1-Cadastrado', '2-Email Confirmado'])->index()->nullable();

            $table->rememberToken();

            $table->integer('image_id')->nullable()->unsigned();
            $table->foreign('image_id')
                ->references('id')
                ->on('files');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
