<?php

namespace Mailer\Notification;

use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class CadastroMembroEquipeNotification extends Notification
{


    private $userLider;

    /**
     * Create a notification instance.
     *
     * @param  string $token
     * @return void
     */
    public function __construct($userLider)
    {
        $this->userLider = $userLider;
    }

    /**
     * Get the notification's channels.
     *
     * @param  mixed $notifiable
     * @return array|string
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Cadastro como Membro de Equipe')
            ->greeting('Seja bem vindo a equipe ', $notifiable->name)
            ->line('Você está recebendo este e-mail porque o ' . $this->userLider->name . ' o adicionou para participar como membro de sua equipe')
            ->salutation('Saudações, Equipe Conecta');
    }
}
