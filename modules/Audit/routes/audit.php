<?php
Route::group(['prefix' => 'audit', 'middleware' => ['auth:api', 'needsPermission']], function () {
    $this->group(['prefix' => '/log'], function () {
        $this->get('/index', [
            'uses' => 'LogController@index',
            'as' => 'audit.log.index',
            'is' => config('defender.superuser_role')
        ]);
    });
});
